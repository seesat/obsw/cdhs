#!/usr/bin/env bash

# Justus Haverkamp - January 2021

# This script can be used to automatically copy the boot.bin file to the STM32F4 Discovery Board
# Only tested on Xubuntu!

USB=$(lsblk | grep F407 | tr -s ' ' | cut -d ' ' -f 7)

cp boot.bin $USB


