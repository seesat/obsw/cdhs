#ifndef _OBCThread_H_
#define _OBCThread_H_

#include "rodos.h"

#include <map>

class OBCState;

enum class OBC_STATE {
    ERROR,
    POWER_ON,
    RUNNING,
    TMTC_POWERED,
    WAIT_FOR_TIMER,
};

class OBCThread : public StaticThread<> {
  public:
    OBCThread();
    ~OBCThread();

    void init() override;
    void run() override;

    void changeState(const OBC_STATE state);

    template <class T> inline T *initStateClass() { return new (xmalloc(sizeof(T))) T(); }

  private:
    OBCState *currState;
    std::map<const OBC_STATE, OBCState *> enumToClassMap;
};

#endif
