#ifndef _STATE_RUNNING_H_
#define _STATE_RUNNING_H_

#include "OBCState.h"
#include "rodos.h"

#include <string>

class Magnetometer;

class StateRunning : public OBCState {
  public:
    StateRunning();
    virtual ~StateRunning();

    void init() override;
    void execute(OBCThread &obc) override;

  private:
    HAL_GPIO ledGreen;
    Magnetometer *const magnetometer;

    std::string parseCompassHeading2str(const double heading_deg) const;
};

#endif
