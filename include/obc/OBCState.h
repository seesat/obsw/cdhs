#ifndef _OBC_STATE_H_
#define _OBC_STATE_H_

class OBCThread;

class OBCState {
  public:
    virtual ~OBCState();

    virtual void init() {}
    virtual void execute(OBCThread &obc) = 0;
};

#endif
