#ifndef _STATE_WAIT_FOR_TIMER_H_
#define _STATE_WAIT_FOR_TIMER_H_

#include "OBCState.h"

#ifndef WAIT_TIME_IN_SEC
#define WAIT_TIME_IN_SEC 10
#endif

class StateWaitForTimer : public OBCState {
  public:
    StateWaitForTimer();
    virtual ~StateWaitForTimer();

    void execute(OBCThread &obc) override;

  private:
    bool deploySADM();
};

#endif
