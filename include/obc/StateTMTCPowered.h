#ifndef _STATE_TMTC_POWERED_H_
#define _STATE_TMTC_POWERED_H_

#include "OBCState.h"

class StateTMTCPowered : public OBCState {
  public:
    StateTMTCPowered();
    virtual ~StateTMTCPowered();

    void execute(OBCThread &obc) override;

  private:
    void powerOnTMTC();

    bool deployUHF();
};

#endif
