#ifndef _POWER_ON_STATE_H_
#define _POWER_ON_STATE_H_

#include "OBCState.h"

class StatePowerOn : public OBCState {
  public:
    StatePowerOn();
    virtual ~StatePowerOn();

    void execute(OBCThread &obc) override;

  private:
    bool performPOST();

    void bootOBSW();

    bool checkUHFState();
    bool checkSADMState();
};

#endif
