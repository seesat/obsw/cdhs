#ifndef _STATE_ERROR_H_
#define _STATE_ERROR_H_

#include "OBCState.h"

class StateError : public OBCState {
  public:
    StateError();
    virtual ~StateError();

    void execute(OBCThread &obc) override;
};

#endif
