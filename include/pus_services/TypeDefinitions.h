/**
 * @file TypeDefinitions.h
 * @author Pit Hühner (huehner@seesat.eu)
 * @brief PUS Parameter Type Definitions (Preliminary)
 * @version 0.1
 * @date 2023-11-21
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdint.h>

//Preliminary type definitions
typedef float Real32;
typedef double Real64;
typedef uint64_t AbsoluteTime;
typedef uint64_t RelativeTime;
typedef uint8_t* TCPtr;