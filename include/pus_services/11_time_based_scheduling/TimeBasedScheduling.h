/**
 * @file TimeBasedScheduling.h
 * @author Pit Hühner (huehner@seesat.eu)
 * @brief Header of PUS Service 11: Time-based Scheduling
 * @version 0.1
 * @date 2023-11-21
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include "../TypeDefinitions.h"

struct Activity
{
    AbsoluteTime realeaseTime;
    uint8_t TCLength;
    TCPtr TCPacket;
};

struct RequestID
{
    uint8_t sourceID;
    uint16_t APID;
    uint16_t sequenceCount;
};


class ST11_TimeBasedScheduling
{
public:
    ST11_TimeBasedScheduling();
    ~ST11_TimeBasedScheduling();

    //Telecommands
    int SS1_Enable();
    int SS2_Disable();
    int SS3_Reset();
    int SS4_InsertActivities(uint8_t N, Activity* activities);
    int SS5_DeleteByRequestID(uint8_t N, RequestID* identifier);
    int SS6_DeleteByFilter(uint8_t type, AbsoluteTime timeTag1, AbsoluteTime timeTag2);
    int SS7_TimeShiftByRequestID(RelativeTime timeOffset, uint8_t N, RequestID* identifier);
    int SS8_TimeShiftByFilter(RelativeTime timeOffset, uint8_t type, AbsoluteTime timeTag1, AbsoluteTime timeTag2);
    int SS9_DetailReportByRequestID(uint8_t N, RequestID* identifier);
    int SS11_DetailReportByFilter(uint8_t type, AbsoluteTime timeTag1, AbsoluteTime timeTag2);
    int SS12_SummaryReportByRequestID(uint8_t N, RequestID* identifier);
    int SS14_SummaryReportByFilter(uint8_t type, AbsoluteTime timeTag1, AbsoluteTime timeTag2);
    int SS15_TimeShiftAll(RelativeTime timeOffset);
    int SS16_DetailReportAll();
    int SS17_SummaryReportAll();

    //Telemetry
    int SS10_DetailReport();
    int SS13_SummaryReport();
    void SS10_setPtr(void (*ptr)(uint8_t N, Activity* activities));
    void SS13_setPtr(void (*ptr)(uint8_t N, AbsoluteTime* releaseTimes, RequestID* identifier));
};