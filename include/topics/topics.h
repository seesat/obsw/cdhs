
/** Example topics for the tutorials **/

#ifndef __demotopics_h__
#define __demotopics_h__

#include "packet_definition.h"
#include "service20.h"
#include "service5.h"
#include "can_management/can_datatypes.h"



#if defined(__ARM_ARCH)
#define ntohs(x) (x)
#define htons(x) (x)
#define ntohl(x) (x)
#define htonl(x) (x)
#endif

extern Topic<PusTcPacket> incomingTcPacket;
extern Topic<PusTmPacket> outgoingTmPacket;

extern Topic<EventReportData> pusEventBus;

extern Topic<GetParamTcData> getParamTcData;
extern Topic<SetParamTcData> setParamTcData;
extern Topic<ParamValueReportData> paramValueReportData;


extern Topic<T_CanMsg> CanCmd;
extern Topic<T_CanMsg> CanAcq;


#endif
