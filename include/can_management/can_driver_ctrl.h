/**************************************************************************************************
 * @file    can_driver_ctrl.h
 * @author  Christian Ültzhöfer, Philipp Thümler
 * @version V0.0.0
 * @date    04-May-2023
 * @brief   This file contains all function prototypes for the CAN_DRIVER_CTRL class.
 *************************************************************************************************/

#ifndef CAN_DRIVER_CTRL_H_
#define CAN_DRIVER_CTRL_H_

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
#include "can_datatypes.h"

/**************************************************************************************************
 * Public - Class prototype
 *************************************************************************************************/
/**
 * @brief Class for controlling communication with the RODOS HAL_CAN Library
 *
 */
class CanDriverCtrl {
  private:
    /******************************/
    /*      Member variables      */
    /******************************/
    RODOS::HAL_CAN  mCanHandle;                           /*!< HAL_CAN instance for using RODOS CAN driver */

  public:
    /******************************/
    /*      Member functions      */
    /******************************/
    /**
     * @brief Default Constructor for class CAN_DRIVER_CTRL
     *
     */
    CanDriverCtrl();

    /**
     * @brief Default Destructor for class CAN_DRIVER_CTRL
     *
     */
    ~CanDriverCtrl();

    /**
     * @brief Write a CAN message on the bus
     *
     * @param pt_Data Pointer to data array
     * @param u8_Dlc Data length code; size of data [bytes]
     * @param u32_CanId Identifier (matches DeviceId)
     * @param b_IsExtID Frame format (standard frame or extended frame)
     * @param b_Rtr Remote Transmission Request Bit
     * @return true Write successful
     * @return false Error
     */
    bool write(const uint8_t* pt_Data, uint8_t &u8_Dlc, uint32_t &u32_CanId, bool b_Rtr = false);

    /**
     * @brief Read the next CAN message from the RxFifo
     *
     * @param pt_RecBuf Pointer to receive buffer to store data of the message
     * @param u32_CanId Identifier
     * @param b_IsExtId IDE Bit of the Can frame; true if Can frame uses extended identifier (29 Bit); false if Can frame uses standard identifier (11 Bit)
     * @param b_Rtr RTR Bit of the Can frame; true if Can frame is a remote transmission request; false if Can frame is a standard message frame
     * @return int8_t Data length code of received CAN message
     *         -1: Error / Fifo empty
     */
    int8_t read(uint8_t* pt_RecBuf, uint32_t* u32_CanId, bool* b_IsExtId, bool* b_Rtr);
};

#endif /* CAN_DRIVER_CTRL_H_ */
