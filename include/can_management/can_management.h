/**************************************************************************************************
  * @file    can_management.h
  * @author  Christian Ültzhöfer, Philipp Thümler
  * @version V0.0.0
  * @date    03-June-2023
  * @brief   This file contains all function prototypes for the CAN
  *          management library.
  ************************************************************************************************/

#ifndef CAN_MANAGEMENT_H_
#define CAN_MANAGEMENT_H_

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
/* Own Libs */
#include "can_driver_ctrl.h"
#include "can_topic_handler.h"
#include "can_datatypes.h"

/**************************************************************************************************
 * Public - Class prototype
 *************************************************************************************************/
/**
 * @brief Class CAN_MANAGEMENT
 * Bus master for CAN communication with SeeSat subsystems
 * 
 */
class CanManagement {

  private:
    /******************************/
    /*      Member variables      */
    /******************************/
    T_CanProfile                                mCanProfile_t;                                /*!< Can Profile table with slots for bus participants -> shows which message is sent in which slot and determines which devices are controlled with which data rate and at which exclusive times */
    uint32_t                                    u32_mCanProfileSize;                          /*!< Number of entries in CanProfile table; importatant for resetting the current slot counter */
    volatile uint32_t                           u32_mCurrentSlot;                             /*!< Counter for iterating over the schedule and indicate the current slot */
    volatile T_CanStatus                        mCanStatus_t;                                 /*!< Current state of the class */
    volatile T_CanCurrentPhase                  mCanCurrentPhase_t;                           /*!< */
    T_CanMsgSchedule                            mCanMsgSchedule_t;                            /*!< Schedule for Can messages to be sent */
    T_CanMsgSchedule                            mCanMsgScheduleNext_t;                        /*!< Next Can message schedule in case of overflow */

    CanDriverCtrl                               mCanDriverCtrl;                               /*!< Instance of class CAN_DRIVER_CTRL for using the RODOS interface */
    CanTopicHandler                             mCanTopicHandler;                             /*!< Instance of class CAN_TOPIC_HANDLER for thread communication and schedule management */


    /******************************/
    /*      Member functions      */
    /******************************/
    /**
     * @brief Reset the counter for iterating over schedules
     * 
     */
    void resetSlotCounter(void);

    /**
     * @brief Increment the counter for iterating over schedules
     * 
     */
    void incrementSlotCounter(void);

    /**
     * @brief Check if a message is ready in the specific slot of the CanProfile table
     * 
     * @return true If a message is in the slot
     * @return false If no message is in the slot
     */
    bool isMsgInSlot(void) const;

    /**
     * @brief Check if the current slot counter is smaller than or equal to the slot of the given entry id
     * 
     * @param entryId Entry id whose slot to check against the current slot
     * @return true Current slot is smaller than or equal to the slot of the entry id
     * @return false Current slot is higher than the slot of the entry id
     */
    bool isCurrentSlotBeforeEntryIdSlot(const uint32_t &entryId) const;

    /**
     * @brief Check if entry is already in the schedule
     * 
     * @param scheduleEntry Schedule entry for which it is to be checked whether it is in the schedule
     * @param schedule Schedule which is to be checked whether it includes the entry
     * @return true Entry is in schedule
     * @return false Entry is not in schedule
     */
    bool isEntryInSchedule(const T_CanMsgScheduleEntry &scheduleEntry, const T_CanMsgSchedule &schedule) const;

    /**
     * @brief Check if CanProfile entry is valid
     * 
     * @param canProfileEntry CanProfile entry to validate
     * @return true CanProfile entry is valid
     * @return false CanProfile entry is invalid and not specified in CanProfile table
     */
    bool isCanProfileEntryValid(const T_CanProfileEntry &canProfileEntry) const;

    /**
     * @brief Replace current schedule with next schedule and clear next schedule
     * 
     */
    void updateSchedules(void);

    /**
     * @brief Get a CAN message from the schedule and send it.
     * If there is no schedule entry for the slot, do nothing.
     */
    void sendMsg(void);

    /**
     * @brief Read the next received CAN message from the RxFifo and send it to the respective flight software instance via topic
     * 
     */
    void readMsg(void);

    /**
     * @brief Create new schedule entry and add it to the schedule
     * 
     * @param canMsg Message to create schedule entry from to add to the schedule
     * @return true Entry successfully added to the schedule or next schedule
     * @return false Error; no entry added to schedule or next schedule
     */
    bool addToSchedule(const T_CanMsg &canMsg);

    /**
     * @brief Add new entry to the next schedule.
     * Gets invoked only, if there is no slot available in the normal schedule.
     * 
     * @param scheduleEntry Schedule entry to add to the next schedule
     * @return true Entry successfully added to the next schedule
     * @return false Error; no entry added to next schedule
     */
    bool addToNextSchedule(const T_CanMsgScheduleEntry &scheduleEntry);

    /**
     * @brief Error Handler function for class CAN_MANAGEMENT
     * 
     */
    void errorHandler(void);


  public:
    /**
     * @brief Constructor for class CAN_MANAGEMENT
     * 
     * @param CanProfile (constant) CanProfile table
     */
    CanManagement(const T_CanProfile &canProfile_t);

    /**
     * @brief Default Destructor for class CAN_MANAGEMENT
     * 
     */
    ~CanManagement();

    /**
     * @brief Getter function for current status of class CAN_MANAGEMENT
     * 
     * @return T_CanStatus current status
     */
    T_CanStatus getStatus(void) const;

    /**
     * @brief Manger function for CAN interface to be called periodically (twice per time slot)
     * 
     */
    void manager(void);

    /**
     * @brief Manager function for CAN_MANAGEMENT schedule to be called once per time slot.
     * This function will get all messages from the CanMsgRxFifo that were received since the last function call and sort them into the schedule/next schedule.
     * 
     */
    void scheduleManager(void);
};

#endif /* CAN_MANAGEMENT_H_ */
