/**************************************************************************************************
  * @file    can_datatypes.h
  * @author  Christian Ültzhöfer, Philipp Thümler
  * @version V0.0.0
  * @date    03-June-2023
  * @brief   This file contains all datatypes for the CAN management library.
  ************************************************************************************************/

 #ifndef CAN_DATATYPES_H_
 #define CAN_DATATYPES_H_

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
/* C++ Libs */
#include <vector>
#include <map>
#include <cstring>
#include <cstdint>
#include <cstdlib>

/* RODOS Libs */
#include "rodos.h"
#include "hal/hal_can.h"

/**************************************************************************************************
 * MACROS
 *************************************************************************************************/
/* CAN-Bus config */
#define CAN_STD_DATA_LENGTH                 (uint8_t)(8)                                      /*!< Standard number of data bytes in a CAN frame */
#define CAN_STD_BAUDRATE                       1000'000U                                      /*!< Standard CAN baudrate [bytes/sec] used for the driver. Must be between 5 KBit/s and 1 MBit/s. Standards are: 125, 250, 500 KBit/s and 1 MBit/s. */
#define CANIDX                         CAN_IDX::CAN_IDX0                                      /*!< CAN Controller */
#define CAN_RX_PIN                    GPIO_PIN::GPIO_024                                      /*!< Pin mapping for Rx Pin */
#define CAN_TX_PIN                    GPIO_PIN::GPIO_025                                      /*!< Pin mapping for Tx Pin */
#define CAN_STD_EXT_ID                             false                                      /*!< Extended Identifier deactivated */

/* CAN_MANAGEMENT config */
#define CAN_PROFILE_SIZE                             32U
#define CAN_MSG_RX_FIFO_SIZE     (2U * CAN_PROFILE_SIZE)

/* RODOS config */
#define CAN_STD_APPLICATION_ID            (int8_t)(1000)                                      /*!< Default Application Id */
#define CAN_CMD_TOPIC_ID                    (int64_t)(1)                                      /*!< Id for topic CanCmd */
#define CAN_ACQ_TOPIC_ID                    (int64_t)(2)                                      /*!< Id for topic CanAcq */

/**************************************************************************************************
 * Public - typedefs / structs / enums
 *************************************************************************************************/
typedef enum {
  CAN_STATUS_OK,
  CAN_STATUS_WRITE_PENDING,
  CAN_STATUS_READ_PENDING,
  CAN_STATUS_RESPONSE_PENDING,
  CAN_STATUS_AWAIT_RECEIVING,
  CAN_STATUS_UNDEFINED_ERROR,
  CAN_STATUS_WRITE_ERROR,
  CAN_STATUS_READ_ERROR,
  CAN_STATUS_ADD_TO_SCHEDULE_ERROR,
  CAN_STATUS_RESPONSE_ERROR,
  CAN_STATUS_MESSAGE_LOST_ERROR,
  CAN_STATUS_UNKNOWN_MSG_TYPE_ERROR,
  CAN_STATUS_INVALID_MSG
} T_CanStatus;

typedef enum {
  CAN_PHASE_1,
  CAN_PHASE_2
} T_CanCurrentPhase;

typedef enum {
  CAN_COMMAND,
  CAN_ACQUISITION
} T_MsgTransferType;

struct T_CanProfileEntry {
  uint32_t                                              u32_EntryId;                                  /*!< Unique Id for the CanProfile entry */
  uint32_t                                              u32_DeviceId;                                 /*!< Device Id */
  T_MsgTransferType                                     MsgTransferType_t;                            /*!< Command or Acquisition */
  uint8_t                                               u8_Size;                                      /*!< Data length [bytes] */
};                                                                                                    /*!< Entry for the CanProfile table */

struct T_CanMsgScheduleEntry {
  T_CanProfileEntry                                     CanProfileEntry_t;                            /*!< CanProfile entry to be sent */
  uint8_t                                               u8_Data[CAN_STD_DATA_LENGTH];                 /*!< Data payload of the respective CanProfile entry*/
  uint32_t                                              u32_Uuid;                                     /*!< uuid for the respective schedule entry */
};                                                                                                    /*!< Entry for Can message scheduler */

struct T_CanMsg {
  uint32_t                                              u32_Uuid;                                     /*!< uuid */
  uint8_t                                               u8_MsgData[CAN_STD_DATA_LENGTH];              /*!< data */
  uint8_t                                               u8_Size;                                      /*!< Data length [bytes] */
  uint32_t                                              u32_EntryId;                                  /*!< Unique Id for the message type in the CanProfile table */
  uint32_t                                              u32_DeviceId;                                 /*!< Device Id / CAN Identifier */
  T_MsgTransferType                                     MsgTransferType_t;                            /*!< Command or Acquisition */
};                                                                                                    /*!< Generic CanMsg datatype for Commands and Acquisitions to be sent to and received from flight software components via topics */

typedef std::vector<T_CanProfileEntry>                  T_CanProfile;                                 /*!< typedef for CanProfile table */
typedef std::map<uint32_t, T_CanMsgScheduleEntry>       T_CanMsgSchedule;                             /*!< typedef for schedule */
typedef Fifo<T_CanMsg, CAN_MSG_RX_FIFO_SIZE>            T_CanMsgFifo;                                 /*!< Fifo for storing received requests (via topic) for sending CAN messages */

#endif /* CAN_DATATYPES_H_ */
