/**************************************************************************************************
  * @file    can_topic_handler.h
  * @author  Christian Ültzhöfer, Philipp Thümler
  * @version V0.0.0
  * @date    03-June-2023
  * @brief   This file contains all function prototypes for the CAN_TOPIC_HANDLER class.
  *************************************************************************************************/
 #ifndef CAN_TOPIC_HANDLER_H_
 #define CAN_TOPIC_HANDLER_H_

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
/* C++ Libs */
#include <cstring>
#include <algorithm>

/* Own Libs */
#include "can_datatypes.h"
#include "topics.h"
/**************************************************************************************************
 * Public - Shared variables
 *************************************************************************************************/

/**************************************************************************************************
 * Class prototype
 *************************************************************************************************/
/**
 * @brief Class for handling topic communication with flight software
 * 
 */
class CanTopicHandler : public Subscriber {
  public:
    T_CanMsgFifo                                mCanMsgRxFifo;                                /*!< Fifo for received CanMsg-sending-requests from flight software */
    
    /**
     * @brief Default Constructor for class CAN_TOPIC_HANDLER
     * 
     */
    CanTopicHandler();

    /**
     * @brief Default Destructor for class CAN_TOPIC_HANDLER
     * 
     */
    ~CanTopicHandler();

    /**
     * @brief Putter for incoming messages. This function gets invoked, when a new message on the CanCmd topic is published and puts the contents of the message in the CanMsgRxFifo.
     * 
     * @param topicId Topic ID
     * @param len Data length
     * @param data Data
     * @param netMsgInfo 
     * @return uint32_t 
     */
    uint32_t put(const uint32_t topicId, const size_t len, void* data, const NetMsgInfo& netMsgInfo);

    /**
     * @brief Send data payload of received CAN message to client (from flight software) via publisher-subscriber-protocol
     * 
     */
    void responseToClient(const T_CanMsgScheduleEntry* pt_CanMsgScheduleEntry);
};

 #endif /* CAN_TOPIC_HANDLER_H_ */
