/**************************************************************************************************
  * @file    can_profile.cpp
  * @author  Christian Ültzhöfer, Philipp Thümler
  * @version V0.0.0
  * @date    15-June-2023
  * @brief   This file contains the CanProfile table, including all possible message types that
  *          can be sent on the CAN Bus by the CAN_MANAGEMENT class.
  ************************************************************************************************/

#pragma once

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
#include "can_management/can_datatypes.h"

/**************************************************************************************************
 * CanProfile
 *************************************************************************************************/
extern T_CanProfile CanProfile_t;
