#ifndef INC_DATAPOOL_CONFIG_ENTRY
#define INC_DATAPOOL_CONFIG_ENTRY


#include "rodos.h"

typedef enum types {
    E_DP_TYPE_INTEGER,
    E_DP_TYPE_FLOAT
} DpTypes;


typedef struct entry{
    unsigned char id;
    const char* name;
    DpTypes type;
}DatapoolConfigEntry;


#endif
