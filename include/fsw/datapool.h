#ifndef INC_DATAPOOL
#define INC_DATAPOOL

#include <map>

#include "rodos.h"
#include "string_pico.h"
#include "topics.h"
#include "services.h"
#include "PusTmPacket.h"
#include "service20.h"
#include "datapoolConfig.h"

extern Semaphore dataPoolLock;
extern class Datapool datapool;




class DatapoolParameter
{
    const char * name;
    unsigned char id;
    int rawValue;
    float engValue;
    DpTypes type;

public:
    DatapoolParameter ();
    DatapoolParameter (unsigned char id, const char * name, DpTypes type);
    void setValue(int value)
    {
	this->rawValue = value;
	this->engValue = (float) value;
    }
    void setValue(float value)
    {
	if (this->type == E_DP_TYPE_INTEGER)
	{
	    //TODO: Add exception
	    PRINTF("DP: ERROR: Float input to integer parameter\n");
	}
	else
	{
	    this->engValue = value;
	}
    }
    void setId(unsigned char id)
    {
	this->id = id;
    }
    void setName(const char * name)
    {
	this->name = name;
    }
    int getRawValue()
    {
	return this->rawValue;
    }
    float getEngValue()
    {
	return this->engValue;
    }
    const char * getName() {
	return this->name;
    }
    unsigned char getId() {
	return this->id;
    }
    DpTypes getType() {
	return this->type;
    }
    void setType(DpTypes type) {
	this->type = type;
    }
};


class IDatapoolObserver
{
public:
    IDatapoolObserver(){}
    virtual ~IDatapoolObserver(){}
    virtual void updateParameter(DatapoolParameter* parameter) = 0;
};





class Datapool {
    IDatapoolObserver* observer = NULL;
    DatapoolParameter params[NB_PARAMETERS];
public:
    Datapool(){}
    void init();
    ~Datapool(){}
    void registerObserver(IDatapoolObserver* observer) {
	this->observer = observer;
    }

    DatapoolParameter* getParameter(unsigned char id)
    {
	DatapoolParameter* result;
	for (unsigned int i = 0; i < NB_PARAMETERS; i++)
	{	
	    if (params[i].getId() == id)
	    {
		result = &params[i];
		break;
	    }
	}
	return result;
    }
    

    void setValue(unsigned char id, float value) {
	this->getParameter(id)->setValue(value);
	if (this->observer != NULL)
	{
	    this->observer->updateParameter(this->getParameter(id));
	}
    }
    
    void setValue(unsigned char id, int value) {
	this->getParameter(id)->setValue(value);
	if (this->observer != NULL)
	{
	    this->observer->updateParameter(this->getParameter(id));
	}
    }

    int getValue(unsigned char id) {
	return this->getParameter(id)->getRawValue();
    }

    float getEngValue(unsigned char id) {
	return this->getParameter(id)->getEngValue();
    }

    DpTypes getType(unsigned char id)
    {
	return this->getParameter(id)->getType();
    }

};




#endif
