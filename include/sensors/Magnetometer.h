#ifndef _MAGNETOMETER_H_
#define _MAGNETOMETER_H_

#include "rodos.h"
#include <stdint.h>

#include "MagnetometerMeasurement.h"

class Magnetometer {
  public:
    Magnetometer(const uint32_t id);
    virtual ~Magnetometer();

    uint32_t getID() const;

    virtual void init(int mode) = 0;

    virtual bool reset() = 0;

    virtual MagnetometerMeasurement readMeasurement() = 0;

  protected:
    int currMode;

  private:
    const uint32_t id;
};

#endif
