#ifndef _NULL_MAGNETOMETER_H_
#define _NULL_MAGNETOMETER_H_

#include "Magnetometer.h"
#include <stdint.h>
#include <stdlib.h>

class NullMagnetometer : public Magnetometer {
  public:
    NullMagnetometer(const uint8_t id);
    ~NullMagnetometer();

    void init(int mode) override;

    bool reset() override;

    MagnetometerMeasurement readMeasurement() override;
};

#endif
