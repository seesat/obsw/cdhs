#ifndef _MagnetometerMeasurement_H_
#define _MagnetometerMeasurement_H_

#include <type_traits>

struct MagnetometerMeasurement {
    float x;
    float y;
    float z;
    float temperature;

    MagnetometerMeasurement() : MagnetometerMeasurement(0, 0, 0, 0) {}

    MagnetometerMeasurement(const float x, const float y, const float z) : MagnetometerMeasurement(x, y, z, 0) {}

    MagnetometerMeasurement(const float x, const float y, const float z, const float temp) : x(x), y(y), z(z), temperature(temp) {}
};

#endif