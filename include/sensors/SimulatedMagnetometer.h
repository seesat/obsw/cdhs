#ifndef _SIMULATEDMAGNETOMETER_H_
#define _SIMULATEDMAGNETOMETER_H_

#include "Magnetometer.h"

class SimulatedMagnetometer : public Magnetometer {
  public:
    SimulatedMagnetometer(const uint32_t id);

    void init(int mode) override;

    bool reset() override;

    MagnetometerMeasurement readMeasurement() override;

  private:
    uint32_t counter;
};

#endif