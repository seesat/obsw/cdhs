//
// Created by ericj on 14.07.2023.
//

#include <map>
#include <variant>

#include "mms/types/types.h"
#include "mms/globals.h"

#ifndef MY_PROJECT_SETWRITEPROTECTION_H
#define MY_PROJECT_SETWRITEPROTECTION_H

#endif //MY_PROJECT_SETWRITEPROTECTION_H

void EnableWriteProtection (const request& request);
void DisableWriteProtection (const request& request);
