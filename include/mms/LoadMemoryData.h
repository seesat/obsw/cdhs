//
// Created by ericj on 25.05.2023.
//

#include <map>
#include <variant>

#include "mms/types/types.h"
#include "mms/globals.h"

#ifndef CDHS_LOADMEMORYDATA_H
#define CDHS_LOADMEMORYDATA_H

void LoadMemoryData (const request& request) ;

class Memory;


#endif // CDHS_LOADMEMORYDATA_H
