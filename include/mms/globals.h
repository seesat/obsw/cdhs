//
// Created by ericj on 11.07.2023.
//


#include "array"

#include "mms/types/Memory.h"

#ifndef MY_PROJECT_MEMORIES_GLOBAL_H
#define MY_PROJECT_MEMORIES_GLOBAL_H

#endif //MY_PROJECT_MEMORIES_GLOBAL_H

extern std::array<Memory*, 3> memories;
extern bool abort_memory_dump;
