//
// Created by ericj on 11.07.2023.
//

#include <map>
#include <variant>

#include "mms/types/types.h"
#include "mms/globals.h"

#ifndef MY_PROJECT_ABORTMEMORYDUMPS_H
#define MY_PROJECT_ABORTMEMORYDUMPS_H

#endif //MY_PROJECT_ABORTMEMORYDUMPS_H

void AbortMemoryDumps () ;
