//
// Created by ericj on 29.05.2023.
//

#include <string>
#include "types.h"

#ifndef CDHS_MEMORY_H
#define CDHS_MEMORY_H

class Memory {
    public:
        int maximum_object_size;
        int access_alignment_constraint;
        bool is_write_protectable;
        bool write_protected;

        Memory(int maximum_object_size, int access_alignment_constraint, bool is_write_protectable) {
            Memory::maximum_object_size = maximum_object_size;
            Memory::access_alignment_constraint = access_alignment_constraint;
            Memory::is_write_protectable = is_write_protectable;
        }

        void set_write_protection (bool flag) {
            if (is_write_protectable) {
                this->write_protected = flag;
            }
        }

        virtual bool is_known_base_id (std::string* id) {
            return false;
        };
        virtual bool write_is_permitted (memory_identifiers id) {
            return false;
        };
        virtual bool read_is_permitted (memory_identifiers id) {
            return true;
        };
        virtual bool write_is_available (memory_identifiers id) {
            return false;
        };
        virtual bool instruction_is_valid (memory_instruction instruction) {
            return false;
        };

        static char* calculate_address (int* base_id, int offset) {
            return reinterpret_cast<char *>(base_id) + offset;
        };

        virtual void write (std::string* base_id, memory_instruction instruction) {};

        virtual std::string* read (std::string* base_id, memory_instruction instruction) {
            return nullptr;
        };
};

#endif // CDHS_MEMORY_H
