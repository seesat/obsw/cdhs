//
// Created by ericj on 29.05.2023.
//

#include "Memory.h"
// for use in Cube32
//#include "../STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal.h"

#ifndef CDHS_FLASHMEMORY_H
#define CDHS_FLASHMEMORY_H

class FlashMemory : public Memory {
  public:
    using Memory::Memory;

    bool is_known_base_id (std::string * id) override {
        return true;
    };
    bool write_is_permitted (memory_identifiers id) override {
        return true;
    };
    bool write_is_available (memory_identifiers id) override {
        return true;
    };
    bool instruction_is_valid (memory_instruction instruction) override {
        return true;
    };

    void write (std::string* base_id, memory_instruction instruction) override {
        //            uint32_t address = &base_id; // Example flash memory address
//            uint32_t data = instruction.data; // Example data to be written
//
//            HAL_FLASH_Unlock();
//            HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address, data);
//            HAL_FLASH_Lock();
    };

    std::string* read (std::string* base_id, memory_instruction instruction) override {
//            uint32_t address = &base_id; // Example flash memory address
//            uint32_t data = *(__IO uint32_t*)address;
    };

};

#endif // CDHS_FLASHMEMORY_H
