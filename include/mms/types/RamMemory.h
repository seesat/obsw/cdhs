//
// Created by ericj on 29.05.2023.
//

#include <cstring>
#include "Memory.h"

#ifndef CDHS_RAMMEMORY_H
#define CDHS_RAMMEMORY_H

class RamMemory : public Memory {
    public:
    using Memory::Memory;

        bool is_known_base_id (std::string * id) override {
            return true;
        };
        bool write_is_permitted (memory_identifiers id) override {
            return true;
        };
        bool write_is_available (memory_identifiers id) override {
            return true;
        };
        bool instruction_is_valid (memory_instruction instruction) override {
            return true;
        };

        void write (std::string * base_id, memory_instruction instruction) override {
            base_id = base_id + instruction.offset;
            *base_id = instruction.data;
        };

        std::string* read (std::string * base_id, memory_instruction instruction) override {
            base_id = base_id + instruction.offset;
            return base_id;
        };
};

#endif // CDHS_RAMMEMORY_H
