//
// Created by ericj on 29.05.2023.
//

#ifndef CDHS_TYPES_H
#define CDHS_TYPES_H

#include <bitset>
#include <vector>
#include "optional"

//enum values are also used for indexes of memories array
enum memory_identifiers {FLASH, RAM, MEMORY_COUNT};

struct memory_instruction {
    uint32_t offset;
    int data_length;
    std::string data;
    std::bitset<16> checksum;
};

struct request {
    memory_identifiers memory_id;
    std::string* base_id;
    uint32_t section_number; //called "N" in TC[6,1] interface definition (8.6.2.1)
    std::vector<memory_instruction> instructions;
};


#endif // CDHS_TYPES_H
