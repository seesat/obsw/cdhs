//
// Created by ericj on 05.06.2023.
//

#include <exception>
#include <iostream>
#include <string>

#include "types.h"

void notify (const std::exception& exception, const request& request);
