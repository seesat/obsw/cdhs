//
// Created by ericj on 11.07.2023.
//


#include "mms/types/types.h"
#include "mms/globals.h"

#ifndef MY_PROJECT_DUMPMEMORYDATA_H
#define MY_PROJECT_DUMPMEMORYDATA_H

#endif //MY_PROJECT_DUMPMEMORYDATA_H

std::vector<std::string*> DumpMemoryData (const request& request) ;

class Memory;
