6#!/usr/bin/python

from socket import *
from sys import exit
import time
import unittest


class validationTest(unittest.TestCase):

    def startTestRun(self):
        # connect to TC converter
        global tcsock
        global tmsock
        tcsock=socket(AF_INET,SOCK_STREAM)
        tcsock.connect(("127.0.0.1",9991))
        # connect to TM generator & checker
        tmsock=socket(AF_INET,SOCK_STREAM)
        tmsock.connect(("127.0.0.1",9992))

    setattr(unittest.TestResult, 'startTestRun', startTestRun)
        
    def test_expectedValueInteger(self):
        global tcsock, tmsock

        # Set float test value to valid range
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x01,0x00000000;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x04,0x00000001;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x05,f4.0;\n")
        
        # Enable first monitoring
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000001;\n")
        # Wait before setting wrong value
        time.sleep(1)
        # Set wrong value for monitor
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x01,0x00000001;\n")
        time.sleep(1)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000000;\n")

        
        tm = tmsock.recv(4096)
        packet = tm.split(";")[0]
        dataWithTime= packet.split(",")[-1]
        dataField= dataWithTime[22:]
        self.assertEqual(dataField, "AB0100000000")


    def test_upperLimitFloat(self):
        global tcsock, tmsock

        # Set float test value to valid range
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x01,0x00000000;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x04,0x00000001;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x05,f4.0;\n")
        
        # Enable first monitoring
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000001;\n")
        # Wait before setting wrong value
        time.sleep(1)
        # Set wrong value for monitor
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x05,f6.0;\n")
        time.sleep(1)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000000;\n")

        
        tm = tmsock.recv(4096)
        packet = tm.split(";")[0]
        dataWithTime= packet.split(",")[-1]
        dataField= dataWithTime[22:]
        self.assertEqual(dataField, "CB0500000000")


    def test_lowerLimitFloat(self):
        global tcsock, tmsock

        # Set float test value to valid range
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x01,0x00000000;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x04,0x00000001;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x05,f4.0;\n")
        
        # Enable first monitoring
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000001;\n")
        # Wait before setting wrong value
        time.sleep(1)
        # Set wrong value for monitor
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x05,f2.0;\n")
        time.sleep(1)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000000;\n")

        
        tm = tmsock.recv(4096)
        packet = tm.split(";")[0]
        dataWithTime= packet.split(",")[-1]
        dataField= dataWithTime[22:]
        self.assertEqual(dataField, "CC0500000000")

        
    def test_upperLimitInteger(self):
        global tcsock, tmsock

        # Set float test value to valid range
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x01,0x00000000;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x04,0x00000001;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x04,f4.0;\n")
        
        # Enable first monitoring
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000001;\n")
        # Wait before setting wrong value
        time.sleep(1)
        # Set wrong value for monitor
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x05,f6.0;\n")
        time.sleep(1)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000000;\n")

        
        tm = tmsock.recv(4096)
        packet = tm.split(";")[0]
        dataWithTime= packet.split(",")[-1]
        dataField= dataWithTime[22:]
        self.assertEqual(dataField, "CB0400000000")


    def test_lowerLimitInteger(self):
        global tcsock, tmsock

        # Set float test value to valid range
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x01,0x00000000;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x04,0x00000001;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x05,f4.0;\n")
        
        # Enable first monitoring
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000001;\n")
        # Wait before setting wrong value
        time.sleep(1)
        # Set wrong value for monitor
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x04,0x00000000;\n")
        time.sleep(1)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x02,0x00000000;\n")
        
        tm = tmsock.recv(4096)
        packet = tm.split(";")[0]
        dataWithTime= packet.split(",")[-1]
        dataField= dataWithTime[22:]
        self.assertEqual(dataField, "CA0400000000")

        
    def stopTestRun(self):        
        global tcsock
        global tmsock
        #tcsock.send(".exit")
        tcsock.close()
        #tmsock.send(".exit")
        tmsock.close()
        
    setattr(unittest.TestResult, 'startTestRun', startTestRun)
        
if __name__ == '__main__':
    unittest.main()





