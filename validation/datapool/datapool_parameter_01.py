6#!/usr/bin/python

from socket import *
from sys import exit
import time
import unittest


class validationTest(unittest.TestCase):

    def startTestRun(self):
        # connect to TC converter
        global tcsock
        global tmsock
        tcsock=socket(AF_INET,SOCK_STREAM)
        tcsock.connect(("127.0.0.1",9991))
        # connect to TM generator & checker
        tmsock=socket(AF_INET,SOCK_STREAM)
        tmsock.connect(("127.0.0.1",9992))

    setattr(unittest.TestResult, 'startTestRun', startTestRun)
        
    def test_singleParamSetGet(self):
        global tcsock, tmsock
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x00,0x00000001;\n")
        tcsock.send("TC,20,2,1,12,b1001,8,0,0x01,0x00;\n")
        time.sleep(0.5)
        tm = tmsock.recv(4096)
        packet = tm.split(";")[0]
        dataWithTime= packet.split(",")[-1]
        dataField= dataWithTime[22:]
        self.assertEqual(dataField, "010000000001")

    def test_multiParamSetGet(self):
        global tcsock, tmsock
        tcsock.send("TC,20,1,1,12,b1001,22,0,0x03,0x00,0x00000001,0x11,0x00000002,0x22,0x00000003;\n")
        tcsock.send("TC,20,2,1,12,b1001,10,0,0x03,0x00,0x11,0x22;\n")
        time.sleep(0.5)
        tm = tmsock.recv(4096)
        packet = tm.split(";")[0]
        dataWithTime= packet.split(",")[-1]
        dataField= dataWithTime[22:]
        self.assertEqual(dataField, "03000000000111000000022200000003")

        
    def stopTestRun(self):        
        global tcsock
        global tmsock
        #tcsock.send(".exit")
        tcsock.close()
        #tmsock.send(".exit")
        tmsock.close()
        
    setattr(unittest.TestResult, 'startTestRun', startTestRun)
        
if __name__ == '__main__':
    unittest.main()





