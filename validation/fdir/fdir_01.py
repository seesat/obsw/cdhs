#!/usr/bin/python

from socket import *
from sys import exit
import time
import unittest


class validationTest(unittest.TestCase):

    def assertParamReportSingle(self, paramId, paramValue):
        tcString = "TC,20,2,1,12,b1001,8,0,0x01,0x" + paramId + ";\n"
        tcsock.send(tcString)
        tm = tmsock.recv(4096)
        packet = tm.split(";")[0]
        dataWithTime= packet.split(",")[-1]
        dataField= dataWithTime[22:]

        # only one parameter expected
        expected = "01"
        expected = expected + paramId
        expected = expected + paramValue

        self.assertEqual(dataField, expected)
        

    def startTestRun(self):
        global tcsock, tmsock
        # connect to TC converter
        tcsock=socket(AF_INET,SOCK_STREAM)
        tcsock.connect(("127.0.0.1",9991))
        # connect to TM generator & checker
        tmsock=socket(AF_INET,SOCK_STREAM)
        tmsock.connect(("127.0.0.1",9992))

    setattr(unittest.TestResult, 'startTestRun', startTestRun)
        

    def test_robot01_ClearObstacleSuccess(self):
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xA0,f20.0;\n")
        time.sleep(0.5)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xA0,f0.0;\n")
        time.sleep(0.5)
        # Check that mode is FDIR
        self.assertParamReportSingle("A1", "00000002")
        # Check Claw is grabbing and releasing the object
        self.assertParamReportSingle("AC", "00000001")
        self.assertParamReportSingle("AD", "FFFFFFE2")
        time.sleep(2)
        # Check that manipulator is commanded
        self.assertParamReportSingle("A6", "00000001")
        self.assertParamReportSingle("A8", "00000001")
        self.assertParamReportSingle("AC", "00000001")
        self.assertParamReportSingle("AD", "00000000")
        self.assertParamReportSingle("A6", "00000001")
        self.assertParamReportSingle("A7", "FFFFFFBA")
        self.assertParamReportSingle("A8", "00000001")
        self.assertParamReportSingle("A9", "FFFFFFE2")
        time.sleep(3)
        self.assertParamReportSingle("AC", "00000001")
        self.assertParamReportSingle("AD", "FFFFFFE2")
        self.assertParamReportSingle("A6", "00000001")
        self.assertParamReportSingle("A7", "00000046")
        self.assertParamReportSingle("A8", "00000001")
        self.assertParamReportSingle("A9", "FFFFFFE2")

        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xA0,f20.0;\n")
        time.sleep(2)
        # Back to rest position and stop commanding
        self.assertParamReportSingle("A6", "00000000")
        self.assertParamReportSingle("A7", "00000000")
        self.assertParamReportSingle("A8", "00000000")
        self.assertParamReportSingle("A9", "00000000")
        self.assertParamReportSingle("AA", "00000000")
        self.assertParamReportSingle("AB", "00000000")
        self.assertParamReportSingle("AC", "00000000")
        self.assertParamReportSingle("AD", "00000000")

        # Check that mode is NAVIGATING
        self.assertParamReportSingle("A1", "00000001")


    def test_robot02_ClearObstacleFailed(self):
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xA0,f20.0;\n")
        time.sleep(0.5)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xA0,f0.0;\n")
        time.sleep(0.5)
        # Check that mode is FDIR
        self.assertParamReportSingle("A1", "00000002")
        # Check Claw is grabbing and releasing the object
        self.assertParamReportSingle("AC", "00000001")
        self.assertParamReportSingle("AD", "FFFFFFE2")
        time.sleep(2)
        # Check that manipulator is commanded
        self.assertParamReportSingle("A6", "00000001")
        self.assertParamReportSingle("A8", "00000001")
        self.assertParamReportSingle("AC", "00000001")
        self.assertParamReportSingle("AD", "00000000")
        self.assertParamReportSingle("A6", "00000001")
        self.assertParamReportSingle("A7", "FFFFFFBA")
        self.assertParamReportSingle("A8", "00000001")
        self.assertParamReportSingle("A9", "FFFFFFE2")
        time.sleep(3)
        self.assertParamReportSingle("AC", "00000001")
        self.assertParamReportSingle("AD", "FFFFFFE2")
        self.assertParamReportSingle("A6", "00000001")
        self.assertParamReportSingle("A7", "00000046")
        self.assertParamReportSingle("A8", "00000001")
        self.assertParamReportSingle("A9", "FFFFFFE2")
        time.sleep(2)
        # Back to rest position and stop commanding
        self.assertParamReportSingle("A6", "00000000")
        self.assertParamReportSingle("A7", "00000000")
        self.assertParamReportSingle("A8", "00000000")
        self.assertParamReportSingle("A9", "00000000")
        self.assertParamReportSingle("AA", "00000000")
        self.assertParamReportSingle("AB", "00000000")
        self.assertParamReportSingle("AC", "00000000")
        self.assertParamReportSingle("AD", "00000000")

        # Check that mode is still FDIR
        self.assertParamReportSingle("A1", "00000002")

        time.sleep(0.5)
        # Move Flag is set
        self.assertParamReportSingle("A2", "00000001")
        time.sleep(1)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xA0,f20.0;\n")
        time.sleep(10)
        # Move flag is reset and mode is Navigating
        self.assertParamReportSingle("A2", "00000000")
        self.assertParamReportSingle("A1", "00000001")
        # No checks on speed and direction performed as this varies with the used surface


    def test_robot03_ClearObstacleSuccessSaveContext(self):
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xA0,f20.0;\n")
        time.sleep(0.5)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xA0,f0.0;\n")
        time.sleep(0.5)
        # Check that mode is FDIR
        self.assertParamReportSingle("A1", "00000002")

        tcsock.send("TC,150,1,1,12,b1001,4,0;\n")
        time.sleep(0.5)
        tcsock.send("TC,150,3,1,12,b1001,4,0;\n")
        time.sleep(0.5)
        tcsock.send("TC,150,2,1,12,b1001,4,0;\n")
        time.sleep(0.5)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xA0,f20.0;\n")
        time.sleep(12)

        # Check that mode is NAVIGATING
        self.assertParamReportSingle("A1", "00000001")


    def induceBusError(self, errorCountId):
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x" + errorCountId + ",0x00000001;\n")
        time.sleep(0.1)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x" + errorCountId + ",0x00000002;\n")
        time.sleep(0.1)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x" + errorCountId + ",0x00000003;\n")
        time.sleep(0.1)
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x" + errorCountId + ",0x00000004;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x" + errorCountId + ",0x00000005;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x" + errorCountId + ",0x00000006;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x" + errorCountId + ",0x00000007;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0x" + errorCountId + ",0x00000008;\n")

    def test_gnsr01_SingleBusError (self):
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xB7,0x00000001;\n")
        time.sleep(0.5)
        # Check bus retry is issued
        self.assertParamReportSingle("B9", "00000001")
        
    def test_gnsr02_MultipleBusError(self):
        global tcsock, tmsock

        # Check initial state of gnsr
        self.assertParamReportSingle("B0", "00000001")
        self.assertParamReportSingle("B1", "00000001")
        self.assertParamReportSingle("B2", "00000001")
        self.assertParamReportSingle("B3", "00000001")
        self.assertParamReportSingle("B4", "00000001")
        self.assertParamReportSingle("B5", "00000000")
        self.assertParamReportSingle("B6", "00000000")

        self.induceBusError("B7")

        time.sleep(1)
        # Check that bus is switched
        self.assertParamReportSingle("B0", "00000002")
        # check that GNSR config is kept
        self.assertParamReportSingle("B1", "00000001")
        self.assertParamReportSingle("B2", "00000001")
        self.assertParamReportSingle("B3", "00000001")
        self.assertParamReportSingle("B4", "00000001")
        self.assertParamReportSingle("B5", "00000000")
        self.assertParamReportSingle("B6", "00000000")

        # Revert the state
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xB0,0x00000001;\n")
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xB7,0x00000000;\n")
        self.assertParamReportSingle("B0", "00000001")
        self.assertParamReportSingle("B7", "00000000")
        time.sleep(0.2)
        tcsock.send("TC,150,3,1,12,b1001,4,0;\n")
        time.sleep(0.2)


    def test_gnsr03_GnsrFailureBusDetection(self):
        global tcsock, tmsock
        # Check initial state of gnsr
        self.assertParamReportSingle("B1", "00000001")
        self.assertParamReportSingle("B2", "00000001")
        self.assertParamReportSingle("B3", "00000001")
        self.assertParamReportSingle("B4", "00000001")
        self.assertParamReportSingle("B5", "00000000")
        self.assertParamReportSingle("B6", "00000000")

        for i in range(16,50):
            val = hex(i).split("x")[1].upper()
            tcsock.send("TC,20,1,1,12,b1001,17,0,0x02,0xB7,0x000000"+val+",0xB8,0x000000"+val+";\n")
            time.sleep(0.05)

        
        time.sleep(5)
        # check that GNSR was reconfigurated
        self.assertParamReportSingle("B1", "00000000")
        self.assertParamReportSingle("B2", "00000000")
        self.assertParamReportSingle("B3", "00000000")
        self.assertParamReportSingle("B4", "00000001")
        self.assertParamReportSingle("B5", "00000001")
        self.assertParamReportSingle("B6", "00000001")
        
        # Revert the state
        tcsock.send("TC,20,1,1,12,b1001,12,0,0x01,0xB0,0x00000001;\n")
        # Reset Bus error and bus retry counter
        tcsock.send("TC,20,1,1,12,b1001,27,0,0x04,0xB7,0x00000000,0xB8,0x00000000,0xB9,0x00000000,0xBA,0x00000000;\n")
        # Reset GNSR State
        tcsock.send("TC,20,1,1,12,b1001,37,0,0x06,0xB1,0x00000001,0xB2,0x00000001,0xB3,0x00000001,0xB4,0x00000001,0xB5,0x00000000,0xB6,0x00000000;\n")

        time.sleep(0.2)
        tcsock.send("TC,150,3,1,12,b1001,4,0;\n")
        time.sleep(0.2)

        
        
    def stopTestRun(self):        
        global tcsock
        global tmsock
        #tcsock.send(".exit")
        tcsock.close()
        #tmsock.send(".exit")
        tmsock.close()
        
    setattr(unittest.TestResult, 'startTestRun', startTestRun)
        
if __name__ == '__main__':
    unittest.main()





