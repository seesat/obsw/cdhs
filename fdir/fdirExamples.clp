(defrule checkFloat
  ?param <- (datapoolParam (id 5)
			   (value ?value))
  =>
  (retract ?param)
  (setDatapoolParam 5 10.0)
  (printout t "float value is " ?value crlf))

(defrule nomOpPrint
  (operation 1)
  =>
  (printout t "Operational Mode nominal" crlf))

(defrule nomOpDoSomething
  ?nomOp <- (operation 1)
  =>
  (retract ?nomOp)
  (printout t "do something with nominal operation" crlf))

