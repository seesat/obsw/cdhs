(defmodule PERCEPTION (export ?ALL)
  (import MAIN ?ALL))

; Perception Templates for Rover example
(deftemplate PERCEPTION::distance (slot value))
(deftemplate PERCEPTION::gnc_main (slot mode) (slot paramId))
(deftemplate PERCEPTION::obstacle (multislot description))


(deffacts paramsRover
  (distance (value 100))
  (gnc_main (mode NONE) (paramId -1))
  (obstacle (description none))
  )


; Perception Templates for GNSR example
(deftemplate PERCEPTION::busErrors (slot bus1) (slot bus2) (slot lastBus1) (slot lastBus2))
(deftemplate PERCEPTION::fdirMode (slot value))
(deftemplate PERCEPTION::gnsrEct (slot id) (slot health) (slot power) (slot telemetry))
(deftemplate PERCEPTION::busRetry (slot retry))
(deftemplate PERCEPTION::busErrorMode (slot mode) (slot side))
(deftemplate PERCEPTION::gnsrInternalFailure (slot id))

(deffacts paramsGnsr
  (busErrors (bus1 0) (bus2 0) (lastBus1 0) (lastBus2 0))
  (fdirMode (value BUS1))
  (gnsrEct (id 1) (health 1) (power 1) (telemetry 1))
  (gnsrEct (id 2) (health 1) (power 0) (telemetry 0))
  (busRetry (retry 0))
  )

; Common Perception Templates
(deftemplate PERCEPTION::time (slot count))

(defglobal ?*time* = 0)
(deffacts common
  (time (count 0))
 )


(defrule PERCEPTION::getFdirMode_BUS1
  ?param <- (datapoolParam (name PARAM_FDIR_MODE)
			   (value 1))
  ?fdirMode <- (fdirMode (value ?))
  =>
  (retract ?param)
  (modify ?fdirMode (value BUS1)))
(defrule PERCEPTION::getFdirMode_BUS2
  ?param <- (datapoolParam (name PARAM_FDIR_MODE)
			   (value 2))
  ?fdirMode <- (fdirMode (value ?))
  =>
  (retract ?param)
  (modify ?fdirMode (value BUS2)))


(defrule PERCEPTION::getBusRetry
  ?param <- (datapoolParam (name PARAM_BUS_RETRY)
			   (value ?busRetryValue))
  ?busRetry <- (busRetry (retry ?))
  =>
  (retract ?param)
  (modify ?busRetry (retry ?busRetryValue)))


(defrule PERCEPTION::getBus1SingleError
  ?paramBus1ErrCnt <- (datapoolParam (name PARAM_BUS1_ERRCNT)
				     (value ?bus1ErrCnt))
  ?errors <- (busErrors (bus1 ?oldBus1Cnt) (lastBus1 ?last))
  (test (> ?bus1ErrCnt ?oldBus1Cnt))
  (test (= ?oldBus1Cnt ?last))
  =>
  ;(printout t "FDIR : Single Bus error detected " ?bus1ErrCnt crlf)
  (retract ?paramBus1ErrCnt)
  (modify ?errors (bus1 ?bus1ErrCnt) (lastBus1 ?oldBus1Cnt))
  (assert (busErrorMode (mode single) (side BUS1)))
  )
(defrule PERCEPTION::getBus1MultipleError
  ?paramBus1ErrCnt <- (datapoolParam (name PARAM_BUS1_ERRCNT)
				     (value ?bus1ErrCnt))
  ?errors <- (busErrors (bus1 ?oldBus1Cnt) (lastBus1 ?last))
  (test (> ?bus1ErrCnt ?oldBus1Cnt))
  (test (> ?oldBus1Cnt ?last))
  =>
  ;(printout t "FDIR : Multiple Bus error detected " ?bus1ErrCnt crlf)
  (retract ?paramBus1ErrCnt)
  (modify ?errors (bus1 ?bus1ErrCnt) (lastBus1 ?oldBus1Cnt))
  (assert (busErrorMode (mode multiple) (side BUS1)))
  )
(defrule PERCEPTION::getBus1ok
  ?paramBus1ErrCnt <- (datapoolParam (name PARAM_BUS1_ERRCNT)
				     (value ?bus1ErrCnt))
  ?errors <- (busErrors (bus1 ?oldBus1Cnt))
  (test (= ?bus1ErrCnt ?oldBus1Cnt))
  =>
  (retract ?paramBus1ErrCnt)
  (modify ?errors (bus1 ?bus1ErrCnt) (lastBus1 ?oldBus1Cnt))
  (assert (busErrorMode (mode none) (side BUS1)))
  )

(defrule PERCEPTION::getBus2SingleError
  ?paramBus2ErrCnt <- (datapoolParam (name PARAM_BUS2_ERRCNT)
				     (value ?bus2ErrCnt))
  ?errors <- (busErrors (bus2 ?oldBus2Cnt) (lastBus2 ?last))
  (test (> ?bus2ErrCnt ?oldBus2Cnt))
  (test (= ?oldBus2Cnt ?last))
  =>
  (retract ?paramBus2ErrCnt)
  (modify ?errors (bus2 ?bus2ErrCnt) (lastBus2 ?oldBus2Cnt))
  (assert (busErrorMode (mode single) (side BUS2)))
  )
(defrule PERCEPTION::getBus2MultipleError
  ?paramBus2ErrCnt <- (datapoolParam (name PARAM_BUS2_ERRCNT)
				     (value ?bus2ErrCnt))
  ?errors <- (busErrors (bus2 ?oldBus2Cnt) (lastBus2 ?last))
  (test (> ?bus2ErrCnt ?oldBus2Cnt))
  (test (> ?oldBus2Cnt ?last))
  =>
  (retract ?paramBus2ErrCnt)
  (modify ?errors (bus2 ?bus2ErrCnt) (lastBus2 ?oldBus2Cnt))
  (assert (busErrorMode (mode multiple) (side BUS2)))
  )
(defrule PERCEPTION::getBus2ok
  ?paramBus2ErrCnt <- (datapoolParam (name PARAM_BUS2_ERRCNT)
				     (value ?bus2ErrCnt))
  ?errors <- (busErrors (bus2 ?oldBus2Cnt))
  (test (= ?bus2ErrCnt ?oldBus2Cnt))
  =>
  (retract ?paramBus2ErrCnt)
  (modify ?errors (bus2 ?bus2ErrCnt) (lastBus2 ?oldBus2Cnt))
  (assert (busErrorMode (mode none) (side BUS2)))
  )



(defrule PERCEPTION::getGnsrEct
  ?gnsr1Health <- (datapoolParam (name PARAM_GNSR1_HEALTH)
				 (value ?gnsr1HealthValue))
  ?gnsr1Power <- (datapoolParam (name PARAM_GNSR1_PWR)
				 (value ?gnsr1PowerValue))
  ?gnsr1Tm <- (datapoolParam (name PARAM_GNSR1_TM)
				 (value ?gnsr1TmValue))
  ?gnsr2Health <- (datapoolParam (name PARAM_GNSR2_HEALTH)
				 (value ?gnsr2HealthValue))
  ?gnsr2Power <- (datapoolParam (name PARAM_GNSR2_PWR)
				 (value ?gnsr2PowerValue))
  ?gnsr2Tm <- (datapoolParam (name PARAM_GNSR2_TM)
			     (value ?gnsr2TmValue))
  ?gnsr1Ect <- (gnsrEct (id 1))
  ?gnsr2Ect <- (gnsrEct (id 2))
  =>
  (retract ?gnsr1Health)
  (retract ?gnsr1Power)
  (retract ?gnsr1Tm)
  (retract ?gnsr2Health)
  (retract ?gnsr2Power)
  (retract ?gnsr2Tm)
  (modify ?gnsr1Ect (id 1) (health ?gnsr1HealthValue)
	  (power ?gnsr1PowerValue)
	  (telemetry ?gnsr1TmValue))
  (modify ?gnsr2Ect (id 2) (health ?gnsr2HealthValue)
	  (power ?gnsr2PowerValue)
	  (telemetry ?gnsr2TmValue))
  )
  

(defrule PERCEPTION::gsnrInternalFailure
  ?event <- (pusEvent (id 175)
		      (severity 3)
		      )
  =>
  (retract ?event)
  (assert (gnsrInternalFailure (id 1))))



(defrule PERCEPTION::getCycleCount
  ?param <- (datapoolParam (name PARAM_FDIR_CYCLE_COUNT)
			   (value ?cycleCount))
  ?time <- (time (count ?))
  =>
;  (printout t "FDIR : Cycle count " ?cycleCount crlf)
  (retract ?param)
  (modify ?time (count ?cycleCount))
  (bind ?*time* ?cycleCount))

(defrule PERCEPTION::obstaclePresentPercept
  ?param <- (datapoolParam (name PARAM_DISTANCE)
			   (value ?distanceValue&:(<= ?distanceValue 16.0)))
  ?obst <- (obstacle (description $?))
  =>
;  (printout t "FDIR : obstacle detected " ?distanceValue crlf)
  (retract ?param)
  (modify ?obst (description obstacle ahead)))

(defrule PERCEPTION::noObstaclePercept
  ?param <- (datapoolParam (name PARAM_DISTANCE)
			   (value ?distanceValue&:(>= ?distanceValue 17.0)))
  ?obst <- (obstacle (description $?))
  =>
  (retract ?param)
  (modify ?obst (description none)))



(defrule PERCEPTION::gncModePercept_INIT
  ?param <- (datapoolParam (name PARAM_GNC_MODE)
			   (id ?id)
			   (value ?value))
  ?gncMode <- (gnc_main (mode ?mode))
  =>
  (retract ?param)
  (modify ?gncMode (mode INIT) (paramId ?id)))


(defrule PERCEPTION::gncModePercept_NAVIGATING
  ?param <- (datapoolParam (name PARAM_GNC_MODE)
			   (id ?id)
			   (value 1))
  ?gncMode <- (gnc_main (mode ?mode))
=>
(retract ?param)
  (modify ?gncMode (mode NAVIGATING) (paramId ?id)))

(defrule PERCEPTION::gncModePercept_FDIR
  ?param <- (datapoolParam (name PARAM_GNC_MODE)
			   (id ?id)
			   (value 2))
  ?gncMode <- (gnc_main (mode ?mode))
  =>
  (retract ?param)
  (modify ?gncMode (mode FDIR) (paramId ?id)))


(defrule PERCEPTION::genericParam
  (declare (salience -100))
  ?param <- (datapoolParam (name ?name)
			   (value ?value))
  =>
  (retract ?param))


(defrule PERCEPTION::genericEvent
  (declare (salience -100))  
  ?event <- (pusEvent (id ?id)
		      (severity ?severity)
		      )
  =>
  (retract ?event))
