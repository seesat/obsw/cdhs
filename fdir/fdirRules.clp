(defmodule FDIR (export ?ALL)
  (import PERCEPTION ?ALL)
  (import MAIN ?ALL))

(deftemplate FDIR::action (multislot description) (slot until (default -1)))

(defrule FDIR::00_FDIR_INIT
  ?gnsr1Ect <- (gnsrEct (id 1))
  ?gnsr2Ect <- (gnsrEct (id 2))
  (not (initialized))
  =>
  (modify ?gnsr1Ect (id 1) (health 1) (power 1) (telemetry 1))
  (modify ?gnsr2Ect (id 2) (health 1) (power 0) (telemetry 0))
  (assert (initialized))
  ;(printout t "FDIR COMPONENT STARTED" crlf)
)


(defrule FDIR::01_singleBusError
  (fdirMode (value ?mode))
  ?busErrorMode <- (busErrorMode (mode single) (side ?busMode))
  (test (eq ?mode ?busMode))
  =>
  ;(printout t "FDIR : issue retry" crlf)
  (retract ?busErrorMode)
  (assert (action (description retry bus transmission ?mode)))
  )

(defrule FDIR::02_multipleBusError
  (fdirMode (value ?mode))
  ?busErrorMode <- (busErrorMode (mode multiple) (side ?busMode))
  (test (eq ?mode ?busMode))
  (not (action (description bus switchRedundant ongoing)))
  (not (action (description reconfigure ? ?  ongoing)))
  =>
  ;(printout t "FDIR : Multiple error, switch to redundant" crlf)
  (retract ?busErrorMode)
  (assert (action (description bus switchRedundant)))
  )


(defrule FDIR::03_retrySuccess
  ?busErrorMode <- (busErrorMode (mode none) (side BUS1))
  ?action <- (action (description retry bus transmission done))
  =>
  (retract ?busErrorMode)
  (retract ?action))

(defrule FDIR::04_switchRedundantSuccess
  ?bus2ErrorMode <- (busErrorMode (mode none) (side BUS2))
  ?action <- (action (description bus switchRedundant done))
  =>
  ;(printout t "FDIR : Switch redundant was successful" crlf)
  (retract ?bus2ErrorMode)
  (retract ?action)
  )
(defrule FDIR::05_switchRedundantBusFailed
  ?bus2ErrorMode <- (busErrorMode (mode multiple) (side BUS2))
  ?action <- (action (description bus switchRedundant done))
  =>
  ;(printout t "FDIR : Switch redundant failed" crlf)
  (retract ?bus2ErrorMode)
  (retract ?action)
  (assert (gnsrInternalFailure (id 1))))


(defrule FDIR::06_reconfigureGnsrSuccess
  ?bus2ErrorMode <- (busErrorMode (mode none) (side BUS2))
  ?action <- (action (description reconfigure gnsr b done))
  =>
  (retract ?bus2ErrorMode)
  (retract ?action)
  (assert (action (description reconfigure gnsr b ongoing)))
  )

(defrule FDIR::07_gnsrInternalFailureReconf
  ?internalFailure <- (gnsrInternalFailure (id 1))
  =>
  (assert (action (description reconfigure gnsr b)))
  (retract ?internalFailure))

(defrule FDIR::08_reconfigureGnsrAtoBSequence
  ?gnsr1Ect <- (gnsrEct (id 1))
  ?gnsr2Ect <- (gnsrEct (id 2) (health 1) (power 0) (telemetry 0))
  ?action <- (action (description reconfigure gnsr b))
  =>
  ;(printout t "FDIR : Reconfigure GNSR" crlf)
  (modify ?gnsr1Ect (health 0))
  (modify ?action (description reconfigure gnsr b ongoing))
  (assert (action (description gnsr a switchOff)))
  (assert (action (description gnsr b switchOn)))
  )
(defrule FDIR::09_reconfigureGnsrAtoBSequenceEnd
  ?gnsr1Ect <- (gnsrEct (id 1))
  ?gnsr2Ect <- (gnsrEct (id 2) (health 1))
  ?actionGnsrA <- (action (description gnsr a switchOff finished))
  ?actionGnsrB <- (action (description gnsr b switchOn finished))
  ?actionReconf <- (action (description reconfigure gnsr b ongoing))
  =>
  ;(printout t "FDIR : Reconfigure GNSR finished" crlf)
  (retract ?actionGnsrA)
  (retract ?actionGnsrB)
  (assert (action  (description reconfigure gnsr b done))))
  
(defrule FDIR::10_reconfigureGnsrFailDeactivate
  ?bus2ErrorMode <- (busErrorMode (mode multiple) (side BUS2))
  ?action <- (action (description reconfigure gnsr b done))
  =>
  ;(printout t "FDIR : Reconfigure Failed -> Deactivate" crlf)
  (retract ?bus2ErrorMode)
  (retract ?action)
  (assert (action (description gnsr a switchOff)))
  (assert (action (description gnsr b switchOff)))
  )

(defrule FDIR::11_reconfigureGnsrFailDeactivateFinished
  ?actionGnsrA <- (action (description gnsr a switchOff finished))
  ?actionGnsrB <- (action (description gnsr b switchOff finished))
  =>
  (retract ?actionGnsrA)
  (retract ?actionGnsrB)
  )



(defrule FDIR::12_bus1ErrorCntOffLimits
  (busErrors (bus1 ?bus1ErrCnt))
  (fdirMode (value BUS1))
  (test (> ?bus1ErrCnt 200))
  (not (action (description bus switchRedundant ongoing)))
  =>
  ;(printout t "FDIR : BUS 1 error count off limis, switch to redundant bus" crlf)
  (assert (action (description bus switchRedundant))))


(defrule FDIR::13_busErrorModeClear
  (declare (salience -50))
  ?errorMode <- (busErrorMode)
  =>
  (retract ?errorMode))


(defrule FDIR::14_beginGncFindline
  ?gncMode <- (gnc_main (mode INIT))
  (obstacle (description none))
  =>
  ;(printout t "FDIR : beginGncFindline" crlf)
  (modify ?gncMode (mode NAVIGATING)))


(defrule FDIR::15_attemptClearObstacle
  (obstacle (description obstacle ahead))
  ?gncMode <- (gnc_main (mode NAVIGATING))
  =>
  ;(printout t "FDIR : attempt clear obstacle" crlf)
  (modify ?gncMode (mode FDIR))
  (assert (action (description attempt clear)))
  )

(defrule FDIR::16_attemptClearSuccess
  (obstacle (description none))
  ?action <- (action (description attempt clear finished))
  ?gncMode <- (gnc_main (mode FDIR))
  =>
  (retract ?action)
  ;(printout t "FDIR : Clearing obstacle successful" crlf)
  (modify ?gncMode (mode NAVIGATING)))
  

(defrule FDIR::17_evadeObstacle
  (obstacle (description obstacle ahead))
  (not (action (description evade started)))
  ?action <- (action (description attempt clear finished))
  ?gncMode <- (gnc_main (mode FDIR))
  =>
  ;(printout t "FDIR : obstacle evading" crlf)
  (retract ?action)
  (assert (action (description evade left)))
)

(defrule FDIR::18_continueEvade
  (obstacle (description obstacle ahead))
  ?action <- (action (description evade finished))
  =>
  ;(printout t "FDIR : Continue evading" crlf)
  (modify ?action (description evade left))
)



(defrule FDIR::19_evadeObstacleCheck
  (obstacle (description none))
  ?action <- (action (description evade finished))
  ?gncMode <- (gnc_main (mode FDIR))
  =>
  (modify ?gncMode (mode NAVIGATING))
  (retract ?action)
  )



