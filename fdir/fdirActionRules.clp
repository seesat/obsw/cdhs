(defmodule ACTION (import PERCEPTION ?ALL)
  (import FDIR ?ALL))

(defglobal ?*E_PARAMID_DISTANCE* = 160)
(defglobal ?*E_PARAMID_GNC_MODE* = 161)
(defglobal ?*E_PARAMID_CMD_MOVE_FLAG* = 162)
(defglobal ?*E_PARAMID_CMD_MOVE_SPEED* = 163)
(defglobal ?*E_PARAMID_CMD_MOVE_DIR* = 164)
(defglobal ?*E_PARAMID_CMD_MOVE_TURN* = 165)
(defglobal ?*E_PARAMID_CMD_ARM_FLAG* = 166)
(defglobal ?*E_PARAMID_CMD_ARM_ANGLE* = 167)
(defglobal ?*E_PARAMID_CMD_HAND_FLAG* = 168)
(defglobal ?*E_PARAMID_CMD_HAND_ANGLE* = 169)
(defglobal ?*E_PARAMID_CMD_WRIST_FLAG* = 170)
(defglobal ?*E_PARAMID_CMD_WRIST_ANGLE* = 171)
(defglobal ?*E_PARAMID_CMD_CLAW_FLAG* = 172)
(defglobal ?*E_PARAMID_CMD_CLAW_ANGLE* = 173)
(defglobal ?*E_PARAMID_FDIR_MODE* = 176)
(defglobal ?*E_PARAMID_GNSR1_HEALTH* = 177)
(defglobal ?*E_PARAMID_GNSR1_PWR* = 178)
(defglobal ?*E_PARAMID_GNSR1_TM* = 179)
(defglobal ?*E_PARAMID_GNSR2_HEALTH* = 180)
(defglobal ?*E_PARAMID_GNSR2_PWR* = 181)
(defglobal ?*E_PARAMID_GNSR2_TM* = 182)
(defglobal ?*E_PARAMID_BUS1_ERRCNT* = 183)
(defglobal ?*E_PARAMID_BUS2_ERRCNT* = 184)
(defglobal ?*E_PARAMID_BUS1_RETRY* = 185)
(defglobal ?*E_PARAMID_BUS2_RETRY* = 186)
(defglobal ?*E_PARAMID_OBC_RECONF* = 187)
(defglobal ?*E_PARAMID_FDIR_CYCLE_COUNT* = 255)


(defrule ACTION::setGncMode_INIT
  (declare (salience -50))
  (gnc_main (mode INIT) (paramId ?id))
  =>
  (setDatapoolParam ?id 0))
(defrule ACTION::setGncMode_NAVIGATING
  (declare (salience -50))
  (gnc_main (mode NAVIGATING) (paramId ?id))
  =>
  (setDatapoolParam ?id 1))
(defrule ACTION::setGncMode_FDIR
  (declare (salience -50))
  (gnc_main (mode FDIR) (paramId ?id))
  =>
  (setDatapoolParam ?id 2))

(defrule ACTION::setFdirMode_BUS1
  (declare (salience -50))
  (fdirMode (value BUS1))
  =>
  (setDatapoolParam ?*E_PARAMID_FDIR_MODE* 1))

(defrule ACTION::setFdirMode_BUS2
  (declare (salience -50))
  (fdirMode (value BUS2))
  =>
  (setDatapoolParam ?*E_PARAMID_FDIR_MODE* 2))

(defrule ACTION::switchBus1ToBus2_Step1
  ?action <- (action (description bus switchRedundant))
  ?fdirMode <- (fdirMode (value ?))
  =>
  (modify ?fdirMode (value BUS2))
  (setDatapoolParam ?*E_PARAMID_BUS1_RETRY* 0)
  (modify ?action (description bus switchRedundant ongoing)  (until (+ ?*time* 3))))

(defrule ACTION::switchBus1ToBus2_Step2
  ?action <- (action (description bus switchRedundant ongoing)  (until ?until))
  (time (count ?time))
  (test (= ?until ?time))
  =>
  (modify ?action (description bus switchRedundant done)))


(defrule ACTION::setGnsr1Ect
  (gnsrEct (id 1) (health ?gnsr1Health)
			(power ?gnsr1Power)
			(telemetry ?gnsr1Telemetry))
  =>
  ;(printout t "GNSR ECT update" crlf)
  (setDatapoolParam ?*E_PARAMID_GNSR1_HEALTH* ?gnsr1Health)
  (setDatapoolParam ?*E_PARAMID_GNSR1_PWR* ?gnsr1Power)
  (setDatapoolParam ?*E_PARAMID_GNSR1_TM* ?gnsr1Telemetry))
(defrule ACTION::setGnsr2Ect
  (gnsrEct (id 2) (health ?gnsr2Health)
			(power ?gnsr2Power)
			(telemetry ?gnsr2Telemetry))
  =>
  ;(printout t "GNSR ECT update" crlf)
  (setDatapoolParam ?*E_PARAMID_GNSR2_HEALTH* ?gnsr2Health)
  (setDatapoolParam ?*E_PARAMID_GNSR2_PWR* ?gnsr2Power)
  (setDatapoolParam ?*E_PARAMID_GNSR2_TM* ?gnsr2Telemetry))



(defrule ACTION::retryBus1Tx
  ?action <- (action (description retry bus transmission BUS1))
  ?retry <- (busRetry (retry ?nbRetry))
  =>
  (modify ?action (description retry bus transmission done))
  (modify ?retry (retry (+ ?nbRetry 1)))
  (setDatapoolParam ?*E_PARAMID_BUS1_RETRY* (+ ?nbRetry 1)))
(defrule ACTION::retryBus2Tx
  ?action <- (action (description retry bus transmission BUS2))
  ?retry <- (busRetry (retry ?nbRetry))
  =>
  (modify ?action (description retry bus transmission done))
  (modify ?retry (retry (+ ?nbRetry 1)))
  (setDatapoolParam ?*E_PARAMID_BUS2_RETRY* (+ ?nbRetry 1)))


(defrule ACTION::GnsrBSwitchOn_Step1
  ?action <- (action (description gnsr b switchOn))
  ?gnsr2Ect <- (gnsrEct (id 2))
  =>
  (modify ?gnsr2Ect (power 1))
  (modify ?action (description gnsr b switchOn pwr on) (until (+ ?*time* 8))))
(defrule ACTION::GnsrBSwitchOn_Step2
  ?action <- (action (description gnsr b switchOn pwr on) (until ?until))
  (time (count ?time))
  (test (= ?until ?time))
  ?gnsr2Ect <- (gnsrEct (id 2))
  =>
  (modify ?gnsr2Ect (telemetry 1))
  (modify ?action (description gnsr b switchOn finished)))


(defrule ACTION::GnsrASwitchOff_Step1
  ?action <- (action (description gnsr a switchOff))
  ?gnsr1Ect <- (gnsrEct (id 1))
  =>
  (modify ?gnsr1Ect (telemetry 0))
  (modify ?action (description gnsr a switchOff tm off) (until (+ ?*time* 8))))
(defrule ACTION::GnsrASwitchOff_Step2
  ?action <- (action (description gnsr a switchOff tm off)  (until ?until))
  ?gnsr1Ect <- (gnsrEct (id  1))  
  (time (count ?time))
  (test (= ?until ?time))
  =>
  (modify ?gnsr1Ect (power 0))
  (modify ?action (description gnsr a switchOff finished)))


(defrule ACTION::GnsrBSwitchOff_Step1
  ?action <- (action (description gnsr b switchOff))
  ?gnsr2Ect <- (gnsrEct (id 2))
  =>
  (modify ?gnsr2Ect (telemetry 0))
  (modify ?action (description gnsr b switchOff tm off) (until (+ ?*time* 8))))
(defrule ACTION::GnsrBSwitchOff_Step2
  ?action <- (action (description gnsr b switchOff tm off)  (until ?until))
  (time (count ?time))
  (test (= ?until ?time))
  ?gnsr2Ect <- (gnsrEct (id 2))
  =>
  (modify ?gnsr2Ect (power 0))
  (modify ?action (description gnsr b switchOff finished)))



(defrule ACTION::attemptClear_Step1
  ?action <- (action (description attempt clear))
  =>
  ;(printout t "FDIR : Attempt Step 1 (Open Claw)" crlf)
  (modify ?action (description attempt clear open claw) (until (+ ?*time* 4)))
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_ANGLE* -30))

(defrule ACTION::attemptClear_Step2
  ?action <- (action (description attempt clear open claw) (until ?until))
  (time (count ?time))
  (test (<= ?until ?time))
  =>
  ;(printout t "FDIR : Attempt Step 2 (grab position)" crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_ARM_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_ARM_ANGLE* -70)
  (setDatapoolParam ?*E_PARAMID_CMD_HAND_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_HAND_ANGLE* -30)
  (modify ?action (description attempt clear grab position) (until (+ ?*time* 4))))


(defrule ACTION::attemptClear_Step3
  ?action <- (action (description attempt clear grab position) (until ?until))
  (time (count ?time))
  (test (<= ?until ?time))
  =>
  ;(printout t "FDIR : Attempt Step 3 (grab)" crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_ANGLE* 0)
  (modify ?action (description attempt clear grab) (until (+ ?*time* 4))))

(defrule ACTION::attemptClear_Step3a
  ?action <- (action (description attempt clear grab) (until ?until))
  (time (count ?time))
  (test (<= ?until ?time))
  =>
  ;(printout t "FDIR : Attempt Step 3a (rest position)" crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_FLAG* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_ANGLE* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_ARM_FLAG* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_ARM_ANGLE* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_HAND_FLAG* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_HAND_ANGLE* 0)
  (modify ?action (description attempt clear rest) (until (+ ?*time* 4))))


(defrule ACTION::attemptClear_Step4
  ?action <- (action (description attempt clear rest) (until ?until))
  (time (count ?time))
  (test (<= ?until ?time))
  =>
  ;(printout t "FDIR : Attempt _Step 4 (throw position)" crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_ANGLE* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_ARM_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_ARM_ANGLE* 70)
  (setDatapoolParam ?*E_PARAMID_CMD_HAND_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_HAND_ANGLE* -30)
  (modify ?action (description attempt clear throw position) (until (+ ?*time* 4))))


(defrule ACTION::attemptClear_Step5
  ?action <- (action (description attempt clear throw position) (until ?until))
  (time (count ?time))
  (test (<= ?until ?time))
  =>
  ;(printout t "FDIR : Attempt Step 5 (throw)" crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_ANGLE* -30)
  (modify ?action (description attempt clear throw) (until (+ ?*time* 4))))

(defrule ACTION::attemptClear_Step6
  ?action <- (action (description attempt clear throw) (until ?until))
  (time (count ?time))
  (test (<= ?until ?time))
  =>
  ;(printout t "FDIR : Attempt Step 6 (reset arm)" crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_FLAG* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_CLAW_ANGLE* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_ARM_FLAG* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_ARM_ANGLE* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_HAND_FLAG* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_HAND_ANGLE* 0)
  (modify ?action (description attempt clear resetArm) (until (+ ?*time* 4))))

(defrule ACTION::attemptClear_Step7
  ?action <- (action (description attempt clear resetArm) (until ?until))
  (time (count ?time))
  (test (<= ?until ?time))
  =>
  ;(printout t "FDIR : Attempt Step 7 (reset arm done)" crlf)
  (modify ?action (description attempt clear finished) (until 0)))

(defrule ACTION::evadeActionStartLeft
  ?action <- (action (description evade left) (until ?until))
  (time (count ?time))
  (test (> ?time ?until))
  =>
  ;(printout t "FDIR : Evade start " ?*time* crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_DIR* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_TURN* 2)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_SPEED* 80)
  (modify ?action (description evade left started) (until (+ ?*time* 5)))
)



(defrule ACTION::evadeActionLeftToAhead
  ?action <- (action (description evade left started) (until ?until))
  (time (count ?count))
  (obstacle (description none))
  (test (<= ?until ?count))
  =>
  ;(printout t "FDIR : Evade go ahead" crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_DIR* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_SPEED* 80)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_TURN* 0)
  (modify ?action (description evade ahead started) (until (+ ?*time* 6)))
  )



(defrule ACTION::evadeActionAheadToRight
  ?action <- (action (description evade ahead started) (until ?until))
  (time (count ?count))
  (obstacle (description none))
  (test (<= ?until ?count))
  =>
  ;(printout t "FDIR : Evade go right" crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_FLAG* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_DIR* 1)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_SPEED* 80)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_TURN* 1)
  (modify ?action (description evade right started) (until (+ ?*time* 8)))
  )

(defrule ACTION::evadeActionFinish
  ?action <- (action (description evade right started) (until ?until))
  (time (count ?count))
  (test (<= ?until ?count))
  =>
  ;(printout t  "FDIR : Evade action finished " ?until crlf)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_FLAG* 0)
  (setDatapoolParam ?*E_PARAMID_CMD_MOVE_SPEED* 0)
  (modify ?action (description evade finished)) 
)