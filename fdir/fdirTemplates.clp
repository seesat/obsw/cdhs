(defmodule MAIN (export ?ALL))

(deftemplate datapoolParam
  (slot id)
  (slot name)
  (slot value))

(deftemplate pusEvent
  (slot id)
  (slot severity)
  (slot param1)
  (slot param2)
  (slot param3)
  (slot param4)
  (slot param5))

(deftemplate telecommand
  (slot subservice)
  (slot param1)
  (slot param2))