#!/usr/bin/env bash

# Sascha Wanninger - December 2022

# This script can be used to compile source files
# into either a boot.bin file for the discovery board
# or as a binary for linux target named tst
# Argument is either discovery or linux-x86

pushd libs/rodos # Change to Rodos Repository

source setenvs.sh # Set Environment Variables

popd



CompileFiles=''

for f in $(find src/fsw/ -name "*.cpp")
do
	CompileFiles="${CompileFiles} ${f}"
done

for f in $(find libs/puslib -name "*.cpp")
do
	CompileFiles="${CompileFiles} ${f}"
done

echo "Compiled Files:${CompileFiles}"

export INCLUDES="$INCLUDES -Iinclude/fsw/ -Ilibs/rodos/api/  -Iinclude/topics -Ilibs/puslib/include"


rodos-executable.sh $1 $CompileFiles # Compile all source files for the discovery board together with Rodos

if [ "$1" = "discovery" ]; then
    echo "Create boot.bin file."
    arm-none-eabi-objcopy -O binary tst boot.bin # Convert elf file to bin file
    rm tst # Remove elf file
fi

