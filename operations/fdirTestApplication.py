import PySimpleGUI as sg
import time
import socket
import threading
UDP_IP = "127.0.0.1"
UDP_PORT_TM_DATA = 12000
UDP_PORT_TC = 12001
socket_tm_data = socket.socket(socket.AF_INET,
                     socket.SOCK_DGRAM)
socket_tc = socket.socket(socket.AF_INET,
                     socket.SOCK_DGRAM)
socket_tc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
socket_tc.bind((UDP_IP, UDP_PORT_TC))

datalock = threading.Lock()
command = ""
single_error = "0"
permanent_error = "0"
gnsr_failure = "0"
gnsr_internal_failure = "0"
nb_packets_received = 0


def thread_get_data(lock):
    while 1:
        global command
        global nb_packets_received
        global single_error, permanent_error, gnsr_failure
        data, addr = socket_tc.recvfrom(1024) # buffer size is 1024 bytes
        lock.acquire()
        command = data.decode("UTF-8")
        nb_packets_received = nb_packets_received + 1
        lock.release()
        print("received message: %s" % command)
        window.write_event_value("UpdateValues", "some values in here")

        lock.acquire()
        socket_tm_data.sendto(bytes(gnsr_internal_failure+single_error+permanent_error+gnsr_failure, "UTF-8"), (UDP_IP, UDP_PORT_TM_DATA))
        single_error = "0"
        lock.release()
  

sg.theme('Dark')
datapool_column = [
    [sg.Text("FDIR Mode", key='-TEXT1-')],
    [sg.In("N/A", enable_events=False,  key='-FDIR_MODE-')],

    [sg.Text("GNSR 1 Health/Power/TM", key='-TEXT2-')],
    [sg.In("N/A", enable_events=False,  key='-GNSR1-')],

    [sg.Text("GNSR 2 Health/Power/TM", key='-TEXT3-')],
    [sg.In("N/A", enable_events=False,  key='-GNSR2-')],

    [sg.Text("BUS 1 Error Count", key='-TEXT4-')],
    [sg.In("N/A", enable_events=False,  key='-BUS1_ERRCNT-')],

    [sg.Text("BUS 2 Error Count", key='-TEXT5-')],
    [sg.In("N/A", enable_events=False,  key='-BUS2_ERRCNT-')],
    
    [sg.Text("BUS Retry", key='-TEXT6-')],
    [sg.In("N/A", enable_events=False,  key='-BUS_RETRY-')],

    [sg.Text("OBC Reconfiguration", key='-TEXT7-')],
    [sg.In("N/A", enable_events=False,  key='-OBC_RECONF-')],
    

]

error_column = [
    [sg.Text("Single Bus Error Flag")],
    [sg.In("N/A", enable_events=False,  key='-S_BUS_ERROR-')],
    [sg.Button("Single Bus 1 Error")],
    [sg.Text("Permanent Bus Error Flag")],
    [sg.In("N/A", enable_events=False,  key='-P_BUS_ERROR-')],
    [sg.Button("Permanent Bus 1 Error")],
    [sg.Text("GNSR Failure Flag")],
    [sg.In("N/A", enable_events=False,  key='-P_GNSR_FAILURE-')],
    [sg.Button("Permanent GNSR A Failure")],
    [sg.Text("GNSR Internal Error Flag")],
    [sg.In("N/A", enable_events=False,  key='-P_GNSR_INTERNAL_FAILURE-')],
    [sg.Button("GNSR A sporadic internal Error")],
    [sg.Button("EXIT")]
]


image_column = [
    [sg.Text("Received Packets", key='-TEXT8-')],
    [sg.In("N/A", enable_events=False,  key='-PKT_REC-')],

    [sg.Text(size=(40, 1), key="-TOUT-")],
    [sg.Image("FunctionalAvionics.gif", key="-IMAGE-")],
]

layout = [
    [
        sg.Column(datapool_column),
        sg.VSeperator(),
        sg.Column(image_column),
        sg.VSeperator(),
        sg.Column(error_column),
    ]
]
    

# Create the window
window = sg.Window("FDIR Test Application", layout)
#window["-IMAGE-"].update(filename="FunctionalAvionics.jpg")
dataThread = threading.Thread(target=thread_get_data, args=(datalock,))
dataThread.start()

# Create an event loop
while True:
    event, values = window.read()
    # End program if user closes window or
    # presses the OK button
    if event == "UpdateValues":
        datalock.acquire()
        command_values = command.split(",")
        datalock.release()
        window['-PKT_REC-'].update(value=str(nb_packets_received))
        window['-FDIR_MODE-'].update(value=command_values[0])
        window['-GNSR1-'].update(value=command_values[1] + " / " + command_values[2] + " / " + command_values[3])
        window['-GNSR2-'].update(value=command_values[4] + " / " + command_values[5] + " / " + command_values[6])
        window['-BUS1_ERRCNT-'].update(value=command_values[7])
        window['-BUS2_ERRCNT-'].update(value=command_values[8])
        window['-BUS_RETRY-'].update(value=command_values[9])
        window['-OBC_RECONF-'].update(value=command_values[10])
        window['-S_BUS_ERROR-'].update(value=single_error)
        window['-P_BUS_ERROR-'].update(value=permanent_error)
        window['-P_GNSR_FAILURE-'].update(value=gnsr_failure)
        window['-P_GNSR_INTERNAL_FAILURE-'].update(value=gnsr_internal_failure)

    if event == "Single Bus 1 Error":
        datalock.acquire()
        single_error = "1"
        datalock.release()
        window['-S_BUS_ERROR-'].update(value=single_error)

    if event == "Permanent Bus 1 Error":
        datalock.acquire()
        if permanent_error == "0":
            permanent_error = "1"
        else:
            permanent_error = "0"
        datalock.release()
        window['-P_BUS_ERROR-'].update(value=permanent_error)

    if event == "Permanent GNSR A Failure":
        datalock.acquire()
        if gnsr_failure == "0":
            gnsr_failure = "1"
        else:
            gnsr_failure = "0"
        datalock.release()
        window['-P_GNSR_FAILURE-'].update(value=gnsr_failure)

    if event == "GNSR A sporadic internal Error":
        datalock.acquire()
        if gnsr_internal_failure == "0":
            gnsr_internal_failure = "1"
        else:
            gnsr_internal_failure = "0"
        datalock.release()
        window['-P_GNSR_INTERNAL_FAILURE-'].update(value=gnsr_internal_failure)

        
    if event == "EXIT" or event == sg.WIN_CLOSED:
        break

socket_tc.close()
socket_tm_data.close()
window.close()
