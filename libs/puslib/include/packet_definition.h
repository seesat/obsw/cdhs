#ifndef _INC_PACKET_DEFINITION_H
#define _INC_PACKET_DEFINITION_H


#ifdef  __cplusplus
extern "C" {
#endif

    typedef struct __attribute__((__packed__)) tcSecondaryHeader {
	unsigned int pusVersion : 4;
	unsigned int ackFlags : 4;
	unsigned char serviceType;
	unsigned char serviceSubtype;
	unsigned short sourceId;
    } TcSecondaryHeader; 

    typedef struct __attribute__((__packed__)) tmSecondaryHeader {
	unsigned int pusVersion : 4;
	unsigned int timeReference : 4;
	unsigned char serviceType;
	unsigned char serviceSubtype;
	unsigned short messageTypeCounter;
	unsigned short destinationId;
	char time[7];
    } TmSecondaryHeader; 
    
    typedef struct __attribute__((__packed__)) packetHeader {
	unsigned int packetVersion : 3;
	unsigned int packetType : 1;
	unsigned int secondaryHeaderFlag : 1;
	unsigned int apid : 11;
	unsigned int sequenceFlags : 2;
	unsigned int sequenceCount : 14;
	unsigned short packetDataLength;
    } PacketHeader;

    typedef struct __attribute__((__packed__)) tcPacket {
	PacketHeader header;
	TcSecondaryHeader secondaryHeader;
	unsigned char data[980];
    } PusTcPacket;

    typedef struct __attribute__((__packed__)) tmPacket {
	PacketHeader header;
	TmSecondaryHeader secondaryHeader;
	unsigned char data[980];
    } PusTmPacket;
    
    
#ifdef  __cplusplus
}

#endif

#endif
