#include "rodos.h"
#include "packet_definition.h"

void PusTmPacket_create(PusTmPacket* tmPacket, unsigned char serviceType, unsigned char serviceSubtype,
			unsigned short length, unsigned short destinationId, void* data, unsigned int dataLength);
