#ifndef _INC_SERVICE20_H
#define _INC_SERVICE20_H

#ifdef  __cplusplus
extern "C" {
#endif

    typedef union {
	float fValue;
	int iValue;
    } ParamValue;
    
    typedef struct __attribute__((__packed__)) paramValuePair
    {
	unsigned char paramId;
	ParamValue value;
    }ParamValuePair;
    
    typedef struct __attribute__((__packed__)) setParamTcTopic
    {
	unsigned char n1;
	ParamValuePair paramValue[50];
    } SetParamTcData;

    typedef struct __attribute__((__packed__)) getParamTcTopic
    {
	unsigned char n1;
        unsigned char paramId[50];
    } GetParamTcData;

    typedef struct __attribute__((__packed__)) paramValueReportTopic
    {
	unsigned char n1;
	ParamValuePair paramValue[50];
    } ParamValueReportData;
    


#ifdef  __cplusplus
}
#endif

#define TYPE_UINT 0
#define TYPE_FLOAT 1

#endif
