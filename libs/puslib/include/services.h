#ifndef _INC_SERVICES_H
#define _INC_SERVICES_H


#ifdef  __cplusplus
extern "C" {
#endif



typedef enum {
    S20_ONBOARD_PARAM = 20,
    S5_EVENT_SERVICE = 5,
    S150_FDIR_SERVICE = 150,
} PusServices;


#ifdef  __cplusplus
}
#define PUS_S20_ONBOARD_PARAM	S20_ONBOARD_PARAM
#define PUS_S5_EVENT_SERVICE	S5_EVENT_SERVICE
#define PUS_S150_FDIR_SERVICE   S150_FDIR_SERVICE

#endif

#endif
