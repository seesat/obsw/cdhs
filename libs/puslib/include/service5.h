#ifndef _INC_SERVICE5_H
#define _INC_SERVICE5_H

#ifdef  __cplusplus
extern "C" {
#endif

    typedef struct __attribute__((__packed__)) eventReportTopic
    {
	unsigned char severity;
	unsigned char eventId;
	unsigned char eventInfo[5];
    }EventReportData;


    void EventReportData_create(EventReportData* eventReport, unsigned char severity, unsigned char eventId,
			    unsigned char param1, unsigned char param2,
			    unsigned char param3, unsigned char param4,
			    unsigned char param5);
#ifdef  __cplusplus
}
#endif

#endif
