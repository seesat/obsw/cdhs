#include "PusTmPacket.h"



void PusTmPacket_create(PusTmPacket* tmPacket, unsigned char serviceType, unsigned char serviceSubtype,
			unsigned short length, unsigned short destinationId, void* data, unsigned int dataLength)
{
    tmPacket->header.packetVersion = 1;
    tmPacket->header.packetType = 0;
    tmPacket->header.secondaryHeaderFlag = 1;
    tmPacket->header.apid = 0;
    tmPacket->header.sequenceFlags = 3;
    tmPacket->header.sequenceCount = 0;
    tmPacket->header.packetDataLength = (length);

    tmPacket->secondaryHeader.pusVersion = 2;
    tmPacket->secondaryHeader.timeReference = 0;
    tmPacket->secondaryHeader.serviceType = serviceType;
    tmPacket->secondaryHeader.serviceSubtype = serviceSubtype;
    tmPacket->secondaryHeader.messageTypeCounter = 0;
    tmPacket->secondaryHeader.destinationId = destinationId;

    memcpy(tmPacket->data, data, dataLength);
}
