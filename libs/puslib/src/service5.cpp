#include "service5.h"

void EventReportData_create(EventReportData* eventReport, unsigned char severity,
			    unsigned char eventId, unsigned char param1,
			    unsigned char param2, unsigned char param3,
			    unsigned char param4, unsigned char param5)
{
    eventReport->severity = severity;
    eventReport->eventId = eventId;
    eventReport->eventInfo[0] = param1;
    eventReport->eventInfo[1] = param2;
    eventReport->eventInfo[2] = param3;
    eventReport->eventInfo[3] = param4;
    eventReport->eventInfo[4] = param5;
}
