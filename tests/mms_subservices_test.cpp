#include <gtest/gtest.h>
#include <list>
#include "mms/CheckMemory.h"
#include "mms/SetWriteProtection.h"
#include "mms/LoadMemoryData.h"
#include "mms/DumpMemoryData.h"
#include "mms/AbortMemoryDumps.h"
#include "mms/globals.h"


TEST(LoadMemoryDataTest, ExecuteOneInstructionRam) {

    std::string mock_memory[64];
    // clear memory area
    for (std::string & i : mock_memory) {
        i = "";
    }
    memory_identifiers memory_identifier = RAM;
    memory_instruction instruction1 = {
            0,
            8,
            "A"
    };
    std::vector<memory_instruction> instructions = {instruction1};

    request request = {
            memory_identifier,
            mock_memory,
            0,
            instructions
    };

    LoadMemoryData(request);

    // Print the contents of the mock_memory array
    for (int i = 0; i < 1; i++) {
        std::cout << "Value at index " << i << ": " << mock_memory[i] << std::endl;
    }

    EXPECT_EQ(mock_memory[0], "A");
}

TEST(LoadMemoryDataTest, ExecuteOneInstructionFlash) {

    std::string mock_memory[64];
    // clear memory area
    for (std::string & i : mock_memory) {
        i = "";
    }

    //memory_identifiers memory_identifier = FLASH;
    //For unit tests on a system that does not support the STM32 library, the FLASH cannot be tested
    memory_identifiers memory_identifier = RAM;
    memory_instruction instruction1 = {
            0,
            8,
            "B"
    };
    std::vector<memory_instruction> instructions = {instruction1};

    request request = {
            memory_identifier,
            mock_memory,
            0,
            instructions
    };

    LoadMemoryData(request);

    // Print the contents of the mock_memory array
    for (int i = 0; i < 1; i++) {
        std::cout << "Value at index " << i << ": " << mock_memory[i] << std::endl;
    }

    EXPECT_EQ(mock_memory[0], "B");
}

TEST(LoadMemoryDataTest, ExecuteSeveralInstructionsRam) {

    std::string mock_memory[64];
    //int *ptr_to_memory_item = &mock_memory[1];
    //uint32_t offset = ptr_to_memory_item - mock_memory;
    // clear memory area
    for (std::string & i : mock_memory) {
        i = "";
        //std::cout << "Value at index " << i << ": " << mock_memory[i] << std::endl;
    }

    memory_identifiers memory_identifier = RAM;
    memory_instruction instruction1 = {
            0,
            8,
            "C"
    };
    memory_instruction instruction2 = {
            1,
            8,
            "D"
    };
    memory_instruction instruction3 = {
            2,
            8,
            "E"
    };
    std::vector<memory_instruction> instructions = {instruction1, instruction2, instruction3};

    request request = {
            memory_identifier,
            mock_memory,
            0,
            instructions
    };

    LoadMemoryData(request);

    // Print the contents of the mock_memory array
    for (int i = 0; i < 3; i++) {
        std::cout << "Value at index " << i << ": " << mock_memory[i] << std::endl;
    }

    EXPECT_EQ(mock_memory[0], "C");
    EXPECT_EQ(mock_memory[1], "D");
    EXPECT_EQ(mock_memory[2], "E");
}

// Revise LoadMemory comment about invalid memory IDs
TEST(LoadMemoryDataTest, DISABLED_HandleInvalidRequest) {

    std::string mock_memory[64];
    // clear memory area
    for (std::string & i : mock_memory) {
        i = "";
    }

    //complies with type memory_identifiers but is incorrect since not available in memories array
    memory_identifiers memory_identifier = MEMORY_COUNT;
    memory_instruction instruction1 = {
            0,
            8,
            "A"
    };
    std::vector<memory_instruction> instructions = {instruction1};

    request request = {
            memory_identifier,
            mock_memory,
            0,
            instructions
    };

    testing::internal::CaptureStdout();
    LoadMemoryData(request);
    std::string output = testing::internal::GetCapturedStdout();

    std::cout << output << std::endl;

    std::ostringstream get_the_address;
    get_the_address << mock_memory;
    std::string address = get_the_address.str();
    std::string expected_output = "During the start of the following request: {memory_id: 2, base_id: " + address + ", section_number: 0} The following exception occurred: St16invalid_argument: The memory identifier is unknown.";
    EXPECT_EQ(expected_output, output);
}

TEST(DumpMemoryDataTest, ExecuteInstruction) {

    std::string mock_memory[64];
    // add data to memory
    for (std::string & i : mock_memory) {
        i = "ABC";
    }

    memory_identifiers memory_identifier = RAM;
    memory_instruction instruction1 = {
            0,
            8
    };
    std::vector<memory_instruction> instructions = {instruction1};

    request request = {
            memory_identifier,
            mock_memory,
            0,
            instructions
    };

    std::vector<std::string*> extracted_data = DumpMemoryData(request);

    std::cout << "Extracted data from memory: " << extracted_data[0];
    EXPECT_EQ(*extracted_data[0], "ABC");
}

TEST(AbortMemoryDumpsTest, SetAbortDumpsFlag) {

    AbortMemoryDumps();
    EXPECT_EQ(abort_memory_dump, true);
}

TEST(SetWriteProtectionTest, SetWriteProtectionFlag) {

    memory_identifiers memory_identifier = FLASH;
    memory_instruction instruction1 = {
            0,
            0
    };
    std::vector<memory_instruction> instructions = {instruction1};

    request request = {
            memory_identifier,
            nullptr,
            0,
            instructions
    };


    EnableWriteProtection(request);
    Memory* memory = memories[memory_identifier];
    EXPECT_EQ(memory->write_protected, true);

    DisableWriteProtection(request);
    EXPECT_EQ(memories[memory_identifier]->write_protected, false);
}

TEST(CheckMemoryTest, CheckMemoryCorrectly) {
    memory_identifiers memory_identifier = FLASH;
    memory_instruction instruction1 = {
            0,
            0
    };
    std::vector<memory_instruction> instructions = {instruction1};

    request request = {
            memory_identifier,
            nullptr,
            0,
            instructions
    };
    CheckMemory(request);
}
