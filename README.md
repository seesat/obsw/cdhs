# Welcome to SeeSat CDHS

This Wiki contents many useful (and unuseful) information about the SeeSat Central Data Handling System (CDHS).

The CDHS is based on the [RODOS](https://gitlab.com/seesat/rodos) embedded real-time operating system. We maintain a local fork for adapting/bugfixing without interfering with the original upstream repository (https://gitlab.com/rodos/rodos).

Happy coding!

For more information please have a look at the [Wiki](https://gitlab.com/seesat/OBC/-/wikis/home).


# RODOS Implementation for the OBC


We strongly recommend using the [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) to flash the stm_obc.bin file to the board.


# Important when cloning the repository

The RODOS and googletest repositories are added as a git submodules. When cloning this repository to a computer the following two commands have to be executed in order to get the submodules cloned as well:

1. `git submodule init`
2. `git submodule update`

After that this repository should be ready to be worked with locally.

## Target platform: Linux

1. `mkdir build && cd build` 
2. `cmake -DCMAKE_TOOLCHAIN_FILE=libs/rodos/cmake/port/linux-x86.cmake -DCMAKE_BUILD_TYPE:STRING=Debug -G "Unix Makefiles" ..` 
3. `cmake --build . --config Debug`

You can call the application from the terminal to execute on Linux:

`./bin/fsw`

## Host and target platform: Raspberry Pi
1. `mkdir build && cd build`
2. `cmake -DCMAKE_TOOLCHAIN_FILE=../libs/rodos/cmake/port/posix.cmake -DEXECUTABLE=ON ..`
3. `make all`

You can call the application from the terminal to execute on Raspbian

./bin/fsw

## Target platform: STM32F4-Discovery


1. `mkdir build && cd build`
2. `cmake -DCMAKE_TOOLCHAIN_FILE=../libs/rodos/cmake/port/discovery.cmake -DCMAKE_BUILD_TYPE:STRING=MinSizeRel -G "Unix Makefiles" ..`
3. `cmake --build . --config MinSizeRel` 

Please note the `-DCMAKE_TOOLCHAIN_FILE` flag. This is very important and depends on the target platform. If you want to build and test on Linux use the tool chain file just like in the example. Otherwise, if you want to build for the STM32f4 Discovery board, you should use `-DCMAKE_TOOLCHAIN_FILE": "./libs/rodos/cmake/port/discovery.cmake`. This will also generate the file *build/bin/stm_obc.bin*. 


## NOT SUPPORTED ANYMORE: Building plain folder with **.cpp** source files

To get started, place all **.cpp** source files into the src folder and run the **make_sources.sh** script with eiter linux-x86 or discovery as target

`$ ./make_sources.sh linux-x86`

`$ ./make_sources.sh discovery`

All files in the src directory will then be passed to the compiler and built into a boot.bin file which can be executed on the discovery board or in a linux application named tst.
Note: Rodos does not need a main file. All files and applications will run alongside eachother automatically.
