#!/usr/bin/python

from socket import *
from sys import exit
import time

# connect to TC converter
tcsock=socket(AF_INET,SOCK_STREAM)
tcsock.connect(("127.0.0.1",9991))
# connect to TM generator & checker
tmsock=socket(AF_INET,SOCK_STREAM)
tmsock.connect(("127.0.0.1",9992))



### check availablility of DM
#tcsock.send("TC,17,1,2,12,b1001,0,1;")
#tmsock.send("TM,1,1,*,*,b0000,*,*;")
#tmsock.send("TM,17,2,*,*,b0000,*,0;")
#tmsock.send("TM,1,7,*,*,b0000,*,*;")

### command DM into state INIIALIZED
#tcsock.send("TC,8,1,2,12,b1001,0,1,0x03,0x006e,0x01;")
#tmsock.send("TM,1,1,*,*,b0000,*,*;")
#tmsock.send("TM,1,7,*,*,b0000,*,*;")
### repeat command DM into state INIIALIZED -> invalid
#tcsock.send("TC,8,1,2,12,b0000,0,1,0x03,0x006e,0x01;")
#tmsock.send("TM,1,8,*,*,b0000,*,*;")

### get the initial parameter monitoring list -> empty
tcsock.send("TC,12,8,2,12,b0000,0,1;")
#tcsock.send("TC,8,1,2,12,b1001,0,1,0x03,0x020e;")
tmsock.send("TM,12,9,*,*,b0000,*,*,0x000000;")

### get the initial out-of-limits -> none
tcsock.send("TC,12,10,2,12,b0000,0,1;");
tmsock.send("TM,12,11,*,*,b0000,0x01,*,0x00;")

tcsock.send("TC,12,5,2,12,b0000,0,1,0x02,0x01,0x03000502,0x00000000,0x0001,0x00,0x00,0x01,0x3333333333333333,0x1111,0x4444444444444444,0x2222,0x00,0x00000000,0x00000000,0x0000,0x02,0x03000602,0x00000000,0x0001,0x00,0x00,0x01,0xcccccccccccccccc,0xaaaa,0xdddddddddddddddd,0xbbbb,0x00,0x00000000,0x00000000,0x0000;");

tcsock.send("TC,12,8,2,12,b0000,0,1;")
tmsock.send("TM,12,9,*,*,b0000,0x6a,*,*;")


#tcsock.send("TC,140,1,2,12,b1001,0,1,0x01,0x01004d04,f2.5;")
#tmsock.send("TM,1,1,*,*,b0000,*,*;")
#tmsock.send("TM,1,7,*,*,b0000,*,*;")

#tcsock.send("TC,140,1,2,12,b1001,0,1,0x01,0x01004c04,f2.6;")
#tmsock.send("TM,1,1,*,*,b0000,*,*;")
#tmsock.send("TM,1,7,*,*,b0000,*,*;")

#tcsock.send("TC,140,2,2,12,b1001,0,1,0x01,0x01004d04;")
#tmsock.send("TM,1,1,*,*,b0000,*,*;")
#tmsock.send("TM,*,*,*,*,b0000,*,*,0x01,0x01004d04,f2.5;")
#tmsock.send("TM,1,7,*,*,b0000,*,*;")

#tcsock.send("TC,140,2,2,12,b1001,0,1,0x01,0x01004c04;")
#tmsock.send("TM,1,1,*,*,b0000,*,*;")
#tmsock.send("TM,*,*,*,*,b0000,*,*,0x01,0x01004c04,f2.6;")
#tmsock.send("TM,1,7,*,*,b0000,*,*;")



time.sleep(2)
print tmsock.recvfrom(4096)[0]
tcsock.send(".exit")
tcsock.close()

tmsock.send(".exit")
#t=raw_input()
tmsock.close()

exit(0) # Successful exit

