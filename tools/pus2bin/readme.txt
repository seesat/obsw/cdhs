CMD2PUS Converter - A short introduction

Usage: 
----------------------------------------------------------------------------------
cmd2pus.exe  [-h] [-tc TC_PORT] [-tm TM_PORT] [-ps SVF_PORT]
                            [-pr REC_PORT] [-V] [-nc]
                            host



USAGE
----------------------------------------------------------------------------------

positional arguments:
  host                  set the host, where OBSW is running

optional arguments:
  -h, --help            show this help message and exit
  -tc TC_PORT, --telecommand TC_PORT
                        Set a port for Telecommand strings [Default: 9991]
  -tm TM_PORT, --telemetry TM_PORT
                        Set a port for Telemetry strings [Default: 9992]
  -ps SVF_PORT, --svf_port SVF_PORT
                        Set a port where OBSW is listening for binary packets
                        [Default: 9000]
  -pr REC_PORT, --recv_port REC_PORT
                        Set a port where Converter is listening for binary
                        packets [Default: 9999]
  -V, --version         show program's version number and exit
  -nc, --nocheck        If this flag is set, TM will be just returned back




Command Format:
----------------------------------------------------------------------------------
Commands passed to the Tool shall have the following format (separator shall be a comma):

for Telecommands:
Type	|	Service Type	| Service Subtype	|	PRID	|	PCAT	|	ACK	|	Len	|	SrcID	 | Payload | Optional checksum;

for Telemetry:
Type	|	Service Type	| Service Subtype	|	PRID	|	PCAT	|	SSC	|	Len	|	DestID	 | CoarseTime | FineTime | Synch | Payload ;


eg for a cetain command:

TC,140,1,2,12,b1001,0,1,<PAYLOAD>[,CHKSUM0xNNNN];

TC,2,4,2,12,b0000,0,1,0x0001,f2.5;

NOTE: When Len == 0. it is filled in automagically
NOTE: When CHKSUM0xNNNN is specified at the end, NNNN is interpreted as 2-byte
      hex number and used as checksum field of the packet. If unspecified, it
      is calculated automagically.



or for TM

TM,140,1,2,3,13,0,1,0x0000028d,0x75c28f,0x00,<PAYLOAD>;




Checking and mismatches:
----------------------------------------------------------------------------------
The following fields of a PUS TM packet aren't checked:
SequenceFlags,CoarseTime,FineTime,Synch

In case of a mismatch in a certain field, an error code is appended to the command.
"1:Version Information mismatch"
"2:Packet Type mismatch"
"3:Datafield Header Flag mismatch"
"4:PRID mismatch"
"5:PCAT mismatch"
"6:SSC mismatch"
"7:Length mismatch"
"8:CCSDS Flag mismatch"
"9:PUS Version mismatch"
"10:Service Type mismatch"
"11:Service Subtype mismatch"
"12:Source ID / Destination ID mismatch"

The datafield are checked field by field and a list of wrong fields is passed:
"13=1: Datafield mismatch" (mismatch in field 1)

The errorcode will be return appended to the packet itself:
TC,140,1,2,12,b0101,0,1,PAYLOAD,e2,e5,e13=1, (for Packet type,PCAT and datafield 1 mismatch)
