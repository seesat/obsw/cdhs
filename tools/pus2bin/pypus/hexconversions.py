'''
Created on 29.08.2013

@author: Sascha
'''

from ctypes import pointer,cast,c_float,c_int,POINTER
# This Function only decodes hexadecimal values not complete PUS packages
def decode_hex2float(s):
    #print "DEBUG_HEXFLOAT: ",s
    s=s.encode("hex")
    #print "DEBUG_HEXFLOAT: ",s
    i=int(s,16)
    #print "DEBUG_HEXFLOAT: ",i
    cp=pointer(c_int(i))
    fp=cast(cp,POINTER(c_float))
    #print "DEBUG_HEXFLOAT: ",fp.contents.value
    return fp.contents.value


# This Function only encodes float values to hex values
def encode_float2hex(s):
    #print "DEEEE:",s
    i=float(s)
    #print "DEEEE:",i
    cp=pointer(c_float(i))
    fp=cast(cp,POINTER(c_int))
    hex_string=hex(fp.contents.value)[2:]
    PUS_string=""
    if len(hex_string) is 1:
        PUS_string=PUS_string+"0"
    for nibble in range(len(hex_string)):
        PUS_string=PUS_string+hex_string[nibble]
        if (((nibble%2) ==1) and nibble is not len(hex_string)-1):
            PUS_string=PUS_string+""
    #print "DEEEE:",PUS_string
    return PUS_string.decode_raw("hex")




def decode_hex2int(s):
    return int(s.encode("hex"),16)