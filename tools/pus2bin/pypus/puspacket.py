'''
Created on 28.08.2013

@author: Sascha
Version 1.0
'''



from __future__ import print_function
from ctypes import CDLL,sizeof,c_void_p,c_float,c_int,pointer,POINTER,cast
import os
import binascii
import sys
from hexconversions import decode_hex2int, decode_hex2float
global count
count=0


class packet_header(object):
    """
    PUS Header class. This defines the fields and the decoding function for binary format.
    """
    def  __init__(self):
        """
        Contructor of the Packet header, which initializes all fields and all values except the Sequence Flags with 0.
        Sequence flags are preset to 3 because this is used for stand alone packets.
        """
        # 2bytes Packet ID
        self.version=0          # 3bit    Header Version (fixed = 0) 
        self.packet_type=0      # 1bit    Packet Type (0 = TM, 1 = TC)
        self.dfh=0              # 1bit    DFH Flag (1: secondary header present)
        #   11bit APID
        self.prid=0             # 7bit
        self.pcat=0             # 4bit
        # 2bytes Pkt Seq Control
        self.sequence_flags=3   # 2bit    Must be set to 0b11 (stand-alone TC packet)
        self.sequence_count=0   # 14bit   0...0x3fff
        # 2bytes Pkt Length
        self.length=0           # 16bit
        #                        ======
        # Sum                     48bit (6bytes)
        
    def decode_raw(self, raw_package):
        """
        Decodes a binary format and stores the values in the attributes.
        @param raw_package: The packet in binary format.
        """
        # Just take the actual header
        
        raw_package = raw_package[:6]
        vers_type_dfh_prid1 = raw_package[:1]
        prid2_pcat = raw_package[1:2]
        sequence = raw_package[2:4]
        lengthbin = raw_package[4:6]
    # Extracting Version Type and DFH Flag
        self.version = decode_hex2int(vers_type_dfh_prid1) >> 5
    ##print(self.version, file = sys.stderr)
        self.packet_type = ((decode_hex2int(vers_type_dfh_prid1) << 3) & 0b11111111) >> 7
    ##print(self.packet_type, file = sys.stderr)
        self.dfh = ((decode_hex2int(vers_type_dfh_prid1) << 4) & 0b11111111) >> 7
    ##print(self.dfh, file = sys.stderr)
    # Extracting PRID and PCAT
        self.prid = ((decode_hex2int(vers_type_dfh_prid1) << 5) & 0b11111111) >> 1
        self.prid = self.prid | (decode_hex2int(prid2_pcat) >> 4)
    ##print(self.prid, file = sys.stderr)
        self.pcat = ((decode_hex2int(prid2_pcat) << 4) & 0b11111111) >> 4
    ##print(self.pcat, file = sys.stderr)
    # Sequence Flags
        self.sequence_flags = ((decode_hex2int(sequence[0]) & 0b11000000) >> 6)
        #print(bin(self.sequence_flags), file = sys.stderr)
    # Sequence Count
        self.sequence_count = (decode_hex2int(sequence[0]) & 0b00111111)
        self.sequence_count = (self.sequence_count << 8) + decode_hex2int(sequence[1])
        #print(self.sequence_count, file = sys.stderr)
    # Packet length
        self.length = decode_hex2int(lengthbin)
        #print(self.length, file = sys.stderr)
    
    
class abstract_data_field_header(object):
    """
    Abstract Class for header definition and methods. Implements all fields that are shared by TC and TM definition.
    """
    def __init__(self):
        """
        Constructor which initializes all fields with 0 except the pus_version which is 1 for all packets.
        """
        self.CCSDS=0
        self.pus_version=1
        self.ack=0
        self.servicetype=0
        self.servicesubtype=0
        self.src_destID=0
        
        
    def getack(self):
        """
        Get Method for the flags.
        Hidden by property declaration.
        @return: Returns a binary string representation
        """
        if self.ack==0:
            ret="b0000"
        elif self.ack==1:
            ret="b0001"
        elif self.ack==2:
            ret="b0010"
        elif self.ack==4:
            ret="b0100"
        elif self.ack==8:
            ret="b1000"
        else:
            raise ValueError("Wrong Flag Value set! (Must be 0,1,2,4,8)")
        return ret
    
    def setack(self,value):
        """
        Set method for ACK Flags. Can be used with string and integer values.
        Hidden by property declaration.
        @param value: A binary string format value or a integer(0,1,2,4,8)
        """
        if type(value)=="<type 'int'>":
            self.ack=value
        elif type(value)=="<type 'str'>":
            if value=="b0000":
                self.ack=0
            elif value=="b0001":
                self.ack=1
            elif value=="b0010":
                self.ack=2
            elif value=="b0100":
                self.ack=4
            elif value=="b1000":
                self.ack=8
            else:
                raise ValueError("Wrong Flag Value set! (Must be 0,1,2,4,8)")
        else:
            raise ValueError("Wrong Flag Value set! (Must be 0,1,2,4,8)")

    tc_ack_flags=property(getack,setack)
    
    def decode_raw(self,raw_packet):
        """
        Decodes the packet header fields from a binary packet.
        @param raw_packet: The binary format packet
        """
        ccsds_pusv_ack=raw_packet[6]
        sourceid=raw_packet[9]
        
        # Getting the flags
        self.CCSDS=((decode_hex2int(ccsds_pusv_ack))&0b11111111)>>7
        ##print(dfh.CCSDS, file = sys.stderr)
        self.pus_type=((decode_hex2int(ccsds_pusv_ack) <<1)&0b11111111)>>5
        ##print(bin(dfh.pus_version), file = sys.stderr)
        self.ack=((decode_hex2int(ccsds_pusv_ack)<<4)&0b11111111)>>4
        ##print(bin(dfh.ack), file = sys.stderr)
    
        # ServiceType and ServiceSubType
        self.servicetype=decode_hex2int(raw_packet[7])
        ##print(dfh.servicetype, file = sys.stderr)
        self.servicesubtype=decode_hex2int(raw_packet[8])
        ##print(dfh.servicesubtype, file = sys.stderr)
        self.src_destID=decode_hex2int(sourceid)


class tc_data_field_header(abstract_data_field_header):
    def __init__(self):
        abstract_data_field_header.__init__(self)
        self.packet_type=1

        
    def decode_raw(self,raw_packet):
        abstract_data_field_header.decode_raw(self,raw_packet)
        

class tm_data_field_header(abstract_data_field_header):
    def __init__(self):
        abstract_data_field_header.__init__(self)
        self.packet_type=0
        self.coarse_time=""
        self.fine_time=""
        self.synch=""
        
    def decode_raw(self,raw_packet):
        abstract_data_field_header.decode_raw(self,raw_packet)
        raw_packet=raw_packet[10:]
        
        self.coarse_time="0x"+binascii.hexlify(raw_packet[:4])
        self.fine_time="0x"+binascii.hexlify(raw_packet[4:7])
        self.synch="0x"+binascii.hexlify(raw_packet[7])


class puspacket(object):
    '''
    Class representing a PUS package.
    '''
    def __init__(self,pheader,data_field_header):
        '''
        Constructor adds the packet header, the datafield_array header and the datafield_array as well as the CRC
        '''
        self.packet_header=packet_header()
        self.packet_header=pheader
        self.datafield_header=abstract_data_field_header()
        self.datafield_header=data_field_header
        self.datafield_array=[]
        self.datafield=""
        self.checksum=None
    
    
    def _popn(self,stream,n):
        
        ret=stream[:n]
        stream=stream[n:]
        return stream,ret
    
    
    def getdatafield(self, raw_packet,expected_packet):

        if self.packet_header.packet_type==0:
            #assert expected_packet[0]=="TM", "Expected and actual Package are of unequal Type <0x"+raw_packet.encode("hex")+">."
            ret=""
            try:
                # determine the amount of nibbles acc. hex notation for later prepending of '0's
                num_nibbles=len(raw_packet[18:-2].encode("hex"))
                raw_packet=(bin(int(raw_packet[18:-2].encode("hex"),16))[2:])
            except ValueError:
                return ret  
                
        elif self.packet_header.packet_type==1:
            #assert expected_packet[0]=="TC", "Expected and actual Package are of unequal Type <0x"+raw_packet.encode("hex")+">."
            ret=""
            try:
                # determine the amount of nibbles acc. hex notation for later prepending of '0's
                num_nibbles=len(raw_packet[10:-2].encode("hex"))
                raw_packet=(bin(int(raw_packet[10:-2].encode("hex"),16))[2:])
            except ValueError:
                return ret  

        # now prepend the required amount of '0's in front of binary packet string
        for i in range(num_nibbles*4 - len(raw_packet)):
            raw_packet="0"+raw_packet
        
        # for any existing packet data bytes
        for element in expected_packet[8:]:
            if element[0]=="b":
                if element[1:]=="":
                    print("Wrong Data field format", file = sys.stderr)
                    break;
                ret=ret+"b"+self._popn(raw_packet, len(element[1:]))+","
                pass
            
            elif element[0:2]=="0x" or element[0:2]=="0X":
                #print(element, file = sys.stderr)
                #print(raw_packet, file = sys.stderr)
                if element[1:]=="":
                    print("Wrong Data field format", file = sys.stderr)
                    break;
                ret=ret+"0x"+self._converthextostring(raw_packet[:len(element[2:])*4]).upper()+","
                raw_packet=raw_packet[len(element[2:])*4:]
                pass

            elif element[0]=="f":
                ret=ret+"f"+str(decode_hex2float(self._converthex(raw_packet[:32])))+","
                pass
            else:
                print("Wrong Data field format", file = sys.stderr)
                break;
        if len(raw_packet)>0:
            #print("This is the REST OF THE RAW PACKET "+str(len(raw_packet)), file = sys.stderr)
            #print(raw_packet, file = sys.stderr)
            ret=ret+"0x"+self._converthextostring(raw_packet).upper()+","
            
        self.datafield=ret
        return ret
    
    def _build_datafield(self,payload):
        """
        
        """
        self.datafield=self.__csvtobin(payload)
        pass
    
    def _convertbin(self,f):
        # change wildcard to zeros
        f=f.replace("*","0")
        
        i=float(f)
        cp=pointer(c_float(i))
        fp=cast(cp,POINTER(c_int))
        ret=bin(fp.contents.value)[2:]
        while len(ret) < 32:
            ret="0"+ret
        return ret
    
    def _converthextostring(self,hexa):
        buff=self._converthex(hexa)
        string=buff.encode("hex")
        #print(repr(string), file = sys.stderr)
        return string
    
    def _converthex(self,string):
        ret=""
        liste=list(string)

        for i in range(len(liste)/8):

            ret=ret+chr(int("".join(liste[i*8:(i+1)*8]),2))
        return ret
    
    def _converthexstring(self,string):
        # Replace wildcards with zeros for decoding
        string=string.replace("*","0")
        
        ret=(bin(int(string,16))[2:])
        while len(ret) < (len(string)*4):
            ret="0"+ret
        return ret

    def __csvtobin(self,liste=None):
        #if inputstring==None: inputstring="f2.5,hABBE,b011,b00011"
        field=""
        #liste=inputstring.split(",")
    
        for element in liste:
            if element=="": pass
            if element[0]=="b":
                field=field+element[1:]
                pass
            elif element[0:2]=="0x" or element[0:2]=="0X":
                field=field+self._converthexstring(element[2:]).zfill(len(element[2:])*4)
                pass
            elif element[0]=="f":
                field=field+self._convertbin(element[1:])
                pass
            else:
                element="0x**"
                field=field+self._converthexstring(element[2:]).zfill(len(element[2:])*4)
                return field

        #field=self._converthex(field)
        if len(field)>0:
            binfield=field
            field=hex(int(field,2))[2:]
            try:
                field=field[:field.rindex("L")]
            except:
                pass
    
            while (len(field)*4)< len(binfield):
                #print("0 added "+str(len(field)), file = sys.stderr)
                field="0"+field
                #print(field, file = sys.stderr)
            field=binascii.unhexlify(field)

        return field

    

    def decode_pus_header(self,raw_package):
        self.packet_header.decode_raw(raw_package)
        return self
    
    def generate_message(self):
        message=""

        # Constructing Packet Header
        
        # 2Byte Packet ID
        #  1st byte
        #     3bit Version + 1bit Type + 1bit DFH + 3bit MSb PRID
        message = message + chr( ((self.packet_header.version & 0b111)<<5) |
                                 ((self.packet_header.packet_type & 0b1)<<4) | 
                                 ((self.packet_header.dfh & 0b1)<<3) |
                                 (((self.packet_header.prid & 0b1111111)>>4) & 0b111) )
        #  2nd byte
        #     4bit LSb PRID + 4bit PCAT
        message = message + chr( ((self.packet_header.prid & 0b1111)<<4) | 
                                 (self.packet_header.pcat & 0b1111) )
        
        # 2byte Pkt Seq Control
        #  2bit SeqFlags + 14bit MSb SSC
        add = ( ((self.packet_header.sequence_flags & 0b11)<<14) |
                self.packet_header.sequence_count & 0b11111111111111)
        #  1st byte
        #     8bit MSb PktSeqCtrl
        message = message + chr(((add & 0b1111111100000000)>>8))
        #     8bit LSb PktSeqCtrl
        message = message + chr((add & 0b11111111))
        
        # 2byte Pkt Seq Control
        message = message + chr(((int(self.packet_header.length) & 0b1111111100000000) >>8))
        message = message + chr((int(self.packet_header.length) & 0b11111111))
        #print(repr(message), file = sys.stderr)

        # 1Byte Datafield Header
        #   1bit CCSDS HdrFlage + 3bit TC PUS Version + 4bit AckFlags
        message = message + chr( ((self.datafield_header.CCSDS & 0b1) << 7) | 
                                 ((self.datafield_header.pus_version & 0b111) << 4) | 
                                 (self.datafield_header.ack & 0b1111) )
        # 1byte PUS Service Type
        message = message + chr(self.datafield_header.servicetype & 0b11111111)
        # 1byte PUS Service Sub-Type
        message = message + chr(self.datafield_header.servicesubtype & 0b11111111)
        # 1byte Source ID
        message = message + chr(((int(self.datafield_header.src_destID) & 0b1111111100000000) >>8))
        message = message + chr((int(self.datafield_header.src_destID) & 0b11111111))
        
        ##TODO: Adding TM specific generation
        if (self.packet_header.packet_type==0):
            message=message+binascii.unhexlify(self.datafield_header.coarse_time[2:])
            message=message+binascii.unhexlify(self.datafield_header.fine_time[2:])
            message=message+binascii.unhexlify(self.datafield_header.synch[2:])
        
        #print(repr(message), file = sys.stderr)
        # Adding Datafield
        message=message+self.datafield
        #print(repr(message), file = sys.stderr)
        # Calculating CRC
        if self.checksum == None:
            crc=msgCrc16(message,len(message))
            message=message+chr((crc >>8)& 0b11111111 )
            message=message+chr((crc & 0b11111111))
        else:
            message=message+self.checksum
        #print(repr(message), file = sys.stderr)
        return message





class csv_bin_parser(object):
    """
    This class parses a PUS Packet from CSV to Binary and vice versa.
    """
    def __init__(self):
        """
        The constructor initializes three fields - one for the packet structure and one for the binary and one for CSV.
        """
        pheader = packet_header()
        data_field_header = abstract_data_field_header()
        self.packet=puspacket(pheader,data_field_header)
        self.binary=""
        self.csv_string=""
        
    def reset(self):
        pheader = packet_header()
        data_field_header = abstract_data_field_header()
        self.packet=puspacket(pheader,data_field_header)
        self.binary=""
        self.csv_string=""


    def parse_raw(self,raw_packet, expected_packet):
        self._check_crc(raw_packet)
        self.packet.decode_pus_header(raw_packet)
        if self.packet.packet_header.packet_type==0:
            self.packet.datafield_header=tm_data_field_header()
        elif self.packet.packet_header.packet_type==1:
            self.packet.datafield_header=tc_data_field_header()
        else:
            raise ValueError("Wrong PUS Type")
        self.packet.datafield_header.decode_raw(raw_packet)
        #print(expected_packet, file = sys.stderr)
        try:
            self.packet.getdatafield(raw_packet,expected_packet[:expected_packet.rindex(";")].split(","))
        except ValueError:
            self.packet.getdatafield(raw_packet,expected_packet.split(","))
        
        
    def generate_string(self):

        self.csv_string=""
        if self.packet.packet_header.packet_type==0:
            self.csv_string=self.csv_string+("TM,")
        elif self.packet.packet_header.packet_type==1:
            self.csv_string=self.csv_string+("TC,")
        
        self.csv_string=self.csv_string+(str(self.packet.datafield_header.servicetype)+",")
        self.csv_string=self.csv_string+(str(self.packet.datafield_header.servicesubtype)+",")
        self.csv_string=self.csv_string+(str(self.packet.packet_header.prid)+",")
        self.csv_string=self.csv_string+(str(self.packet.packet_header.pcat)+",")
        
        # Binary representation of Flags
        # For TM these are only fillers and can be omitted
        if self.packet.packet_header.packet_type==1:
            self.csv_string=self.csv_string+"b"+(bin(self.packet.datafield_header.ack)[2:].zfill(4)+",")
        
        else: #self.packet.packet_header.packet_type==0:    
            # Now add sequence information to the string
            #self.csv_string=self.csv_string+"b"+(bin(self.packet.packet_header.sequence_flags)[2:].zfill(2))+","
            # Sequence Count
            self.csv_string=self.csv_string+(str(self.packet.packet_header.sequence_count))+","
        
        # Add Length
        self.csv_string=self.csv_string+(str(self.packet.packet_header.length)+",")
        
        # Add Source/Destination ID
        self.csv_string=self.csv_string+(str(self.packet.datafield_header.src_destID)+",")
        
        # Add timing information to string
        if self.packet.packet_header.packet_type==0:  
            self.csv_string=self.csv_string+self.packet.datafield_header.coarse_time+","
            self.csv_string=self.csv_string+self.packet.datafield_header.fine_time+","
            self.csv_string=self.csv_string+self.packet.datafield_header.synch+","
        
        # Add datafield
        self.csv_string=self.csv_string+(self.packet.datafield)
#         self.csv_string=self.csv_string+()
#         self.csv_string=self.csv_string+()
        
        
    
        


    


    def parse_csv(self,string):
        """
        This parses a CSV file and stores it in the attribute packet.
        A CSV PUS Packet looks like this:
        TC,140,1,2,12,b0001,h1234,...;
        Sets fields to None if any invalid element is found for example the wildcard character. Such a packet will throw errors
        if generated.
        """
        try:
            string=string[:string.index(";")]
        except:
            pass
        parameter=string.split(",")
        #print(parameter, file = sys.stderr)
        # Now get the kind of packet TC or TM
        if parameter[0].upper()=='TC':
            self.packet.datafield_header=tc_data_field_header()
        elif parameter[0].upper()=="TM":
            self.packet.datafield_header=tm_data_field_header()
        ## Packet Header Data
        # Obvious Data parsed to structure
        try:
            self.packet.packet_header.prid=int(parameter[3])
        except:
            self.packet.packet_header.prid=None
        try:
            self.packet.packet_header.pcat=int(parameter[4])
        except:
            self.packet.packet_header.pcat=None
        
        self.packet.packet_header.dfh=1
        # Setting fixed values
        self.packet.packet_header.sequence_flag=3
        
        self.packet.packet_header.version=0
        self.packet.packet_header.packet_type=1
        
        
        
        ## Datafield Header Data
        try:
            self.packet.datafield_header.servicetype=int(parameter[1])
        except:
            self.packet.datafield_header.servicetype=None
        try:
            self.packet.datafield_header.servicesubtype=int(parameter[2])
        except:
            self.packet.datafield_header.servicesubtype=None

        
        if parameter[0].upper()=='TC':
            global count
            self.packet.packet_header.sequence_count = count
            count = (count+1) & 0x3fff   # increment consider 14bit size limit)

            # Parse Ack Flags
            if parameter[5][0]!='b' and parameter[5][0]!='B':
                raise ValueError("Wrong Ack Flags (must be e.g. 'b0100')") 
            else:
                self.packet.datafield_header.ack=int(parameter[5][1:],2)
            try:
                self.packet.packet_header.length=int(parameter[6])
            except:
                self.packet.packet_header.length=None
                
            try:
                self.packet.datafield_header.src_destID=int(parameter[7])
            except:
                self.packet.datafield_header.src_destID=None

            if parameter[-1].startswith('CHKSUM0x'):
                payload=parameter[8:-1]
                self.packet.checksum = binascii.unhexlify(parameter[-1][(parameter[-1].index("0x") + 2):])
            else:
                payload=parameter[8:]
                self.packet.checksum = None

            self.packet._build_datafield(payload)
            #print(repr(self.packet.datafield), file = sys.stderr)
            ## Copying the payload into the datafield_array
# # #             bitcounter=0
# # #             payload=parameter[8:]
# # #             buff=None
# # #             for field in payload:
# # #                 # Only take the value
# # #                 fieldtype=field[0]
# # #                 field=field[1:]
# # #                 #  Binary fields
# # #                 if (fieldtype=='b' and bitcounter%8==0):
# # #                        
# # #                     buff=None
# # #                     dfl=list(self.packet.datafield)
# # #                     dfl.append(chr(0))
# # #                     dfl[bitcounter/8]=chr((int(field,2)<<(8-(bitcounter%8)))&0b11111111)
# # #                     bitcounter=bitcounter+len(field)
# # #                        
# # #                 elif (fieldtype=='b' and bitcounter%8!=0):
# # #                        
# # #                     buff=None
# # #                     dfl=list(self.packet.datafield)
# # #                     dfl[bitcounter/8]=chr(int(dfl[bitcounter/8].encode("hex"),16) | ((int(field,2)<<(8-(bitcounter%8)-len(field)))&0b11111111))
# # #                     self.packet.datafield="".join(dfl)
# # #                     bitcounter=bitcounter+len(field)
# # #                     pass
# # #                        
# # #                        
# # #                        
# # #                 # Hexadecimal fields
# # #                 elif (fieldtype=='h') and(bitcounter%8==0):
# # #                        
# # #                     payloadfield=""
# # #                     #print(field, file = sys.stderr)
# # #                     for el1,el2 in zip(field[0::2],field[1::2]):
# # #                         #print(el1+el2, file = sys.stderr)
# # #                         payloadfield_buffer=chr(int(el1+el2,16))
# # #                         payloadfield=payloadfield+payloadfield_buffer
# # #                     self.packet.datafield=self.packet.datafield+payloadfield
# # #                     bitcounter=bitcounter+len(field)*4
# # #                    
# # #                 elif (fieldtype=='h') and(bitcounter%8!=0):
# # #                        
# # #                     payloadfield=""
# # #                     #print(field, file = sys.stderr)
# # #                     for el1,el2 in zip(field[0::2],field[1::2]):
# # #                         #print(el1+el2, file = sys.stderr)
# # #                         payloadfield_buffer=chr(int(el1+el2,16))
# # #                         payloadfield=payloadfield+payloadfield_buffer
# # #                        
# # #                     #print(list(payloadfield), file = sys.stderr)
# # #                     #self.packet.datafield=self.packet.datafield+payloadfield
# # #                     dfl=list(self.packet.datafield)
# # #                     for i in range(len(field)/2):
# # #                         dfl.append(chr(0))
# # #                         if (i==0and buff==None): buff=dfl[bitcounter/8]
# # #                         dfl[bitcounter/8+i]=chr((int(buff.encode("hex"),16) |  (((int(list(payloadfield)[i].encode("hex"),16)) >> ((bitcounter%8)))  ))& 0b11111111)
# # #                         buff=chr(((int(list(payloadfield)[i].encode("hex"),16)) << (8-(bitcounter%8)))& 0b11111111)
# # #                         #print((buff), file = sys.stderr)
# # #                     dfl.append(chr(0))
# # #                     dfl[bitcounter/8+(len(field)/2)]=chr((int(buff.encode("hex"),16) |  (((int(list(payloadfield)[i].encode("hex"),16)) >> ((bitcounter%8)))  ))& 0b11111111)
# # #                     self.packet.datafield="".join(dfl) 
# # #                        
# # #                     bitcounter=bitcounter+len(field)*4
# # #                         
# # #                 # Floating point fields (IEEE Standard)
# # #                 elif ((fieldtype=='f')and(bitcounter%8==0)):
# # #                        
# # #                     payloadfield=encode_float2hex(field)
# # #                     #print(repr(payloadfield), file = sys.stderr)
# # #                     self.packet.datafield=self.packet.datafield+payloadfield
# # #                     bitcounter=bitcounter+32
# # #                    
# # #                 elif ((fieldtype=='f')and(bitcounter%8!=0)):
# # #                        
# # #                     payloadfield=encode_float2hex(field)
# # #                     #print(list(payloadfield), file = sys.stderr)
# # #                     #self.packet.datafield=self.packet.datafield+payloadfield
# # #                     dfl=list(self.packet.datafield)
# # #                     for i in range(4):
# # #                         dfl.append(chr(0))
# # #                         if (i==0and buff==None): buff=dfl[bitcounter/8]
# # #                         dfl[bitcounter/8+i]=chr((int(buff.encode("hex"),16) |  (((int(list(payloadfield)[i].encode("hex"),16)) >> ((bitcounter%8)))  ))& 0b11111111)
# # #                         buff=chr(((int(list(payloadfield)[i].encode("hex"),16)) << (8-(bitcounter%8)))& 0b11111111)
# # #                         #print((buff), file = sys.stderr)
# # #                     dfl.append(chr(0))
# # #                     dfl[bitcounter/8+4]=chr((int(buff.encode("hex"),16))& 0b11111111)
# # #                     self.packet.datafield="".join(dfl) 
# # #                     bitcounter=bitcounter+32
# # #                        
# # #                            
# # #    
# # #                        
# # #                 else:
# # #                     raise ValueError("Wrong Payload format "+str(fieldtype)+" for "+str(field))
# # #                    
# # #    
              
              
            #self.binary=self.packet.generate_message()
                
            
        elif parameter[0].upper()=='TM':
            
            
            ## TM specific parsing
            self.packet.packet_header.packet_type=0
            
            try:
                self.packet.packet_header.length=int(parameter[6])
            except:
                self.packet.packet_header.length=None
                
            try:
                self.packet.datafield_header.src_destID=int(parameter[7])
            except:
                self.packet.datafield_header.src_destID=None
                
            try:
                self.packet.packet_header.sequence_count=int(parameter[5])
            except:
                self.packet.packet_header.sequence_count=None
            
            
            payload=parameter[8:]
             
            self.packet._build_datafield(payload)
            
            pass
        else:
            print("Packet Type "+parameter[0].upper(), file = sys.stderr)
            raise ValueError("Wrong Packet Type")

        # a '0' for packet length indicates to calculate it ourselves
        if self.packet.packet_header.length==0:
            length=0.0
            for element in payload:
                if element=="": break
                if element[0]=="b":
                    length=length+(len(element[1:])*(1.0/8))
                elif (element[0:2]=="0x") or (element[0:2]=="0X"):
                    length=length+len(element[2:])*0.5
                elif element[0]=="f":
                    length=length+4
            #print("Length ="+str(length), file = sys.stderr)
            self.packet.packet_header.length = int(length+1+4)

        return
    
    
    

    

    def _check_crc(self,raw_packet):
        """
        Checks the CRC Field with a Shared Library included by ctypes. Works on windows and linux.
        """
        #crc=msgCrc16(raw_packet[:-2],len(raw_packet)-2) & 0b1111111111111111

        #assert (int(raw_packet[-2:].encode("hex"),16) == crc), "ERROR: CRC mismatch <0x" + raw_packet.encode("hex") + ">, expected " + hex(crc)
        
        
def msgCrc16(data, length):
    crc=0xFFFF
    for i in range(length):
        for j in range(8):
            if ((( int(data[i].encode("hex"),16) <<j) & 0x80) ^ ((crc & 0x8000) >>8)):
                crc = ((crc << 1) ^ 0x1021) & 0xFFFF
            else:
                crc = (crc << 1) & 0xFFFF 
    
    return crc
            
