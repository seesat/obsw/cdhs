#!/usr/bin/python
# encoding: utf-8
'''
main -- Converts PUS commands in string format to binary format.
'''

from __future__ import print_function
import errno
import sys
import os
import socket
import threading
import Queue
import pypus
import signal
from copy import copy
import time
import traceback
import datetime



######################Options#################################
qsize=2048
global buffersize
buffersize=4096
global exitFlag
exitFlag=0
global udprec_socket
global tc_socket
global tm_socket
global tm_returnq
global return_flag
return_flag=False

helplist=["-h", "-H", "-help","--help","-Help","--Help"]

##############################################################
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

__all__ = []
__version__ = 0.1
__date__ = '2013-11-14'
__updated__ = '2013-11-14'

DEBUG = 0
TESTRUN = 0
PROFILE = 0



def signal_handler(signal,frame):
    global exitFlag
    exitFlag=1
    
    print("Exiting... Please close all sockets!", file = sys.stderr)
    time.sleep(1)
    
    global udprec_socket
    global tc_socket
    global tm_socket
    
    try:
        print("UDP Receiver close ... ", file = sys.stderr, end="")
        udprec_socket.close()
        print("done", file = sys.stderr)
        
        tc_socket.destroy()
        tm_socket.destroy()

    except socket.error, e:
        print("Error %d: %s" % (e.args[0], e.args[1]), file = sys.stderr)
    sys.exit(0)


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg



def main(argv=None): # IGNORE:C0111
    '''Command line options.'''
    signal.signal(signal.SIGINT, signal_handler)
    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Sascha Wanninger on %s.
  Copyright 2016 Airbus DS GmbH. All rights reserved.
  
  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0
  
  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    usage_examples = '''Example usage:
    ./%s -c -nc -ps 9001 -pr 9000 10.0.5.3 2>/dev/null # Ctrl+D at the end of use
    ''' % (program_name)
        
    IP=""   # if empty string, localhost IP 127.0.0.1 will be used in further initialisations
    IP_REMOTE = "127.0.0.1"
    PORT_TC=9991
    PORT_TM=9992
    PORT_REMOTE=9000
    PORT_LISTEN=9999
    PORT_MIRROR=0
    mirror=False
    verbose=False

    global NO_CHECK
    global CONSOLE_MODE

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, epilog=usage_examples, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument(dest="host", default=IP_REMOTE, type=str, help="set the host, where OBSW is running")
        parser.add_argument("-c", "--console", dest="consoleio", action="store_true",
                            help="Use console I/O instead of TCP interface (invalidates -tc/-tm)")
        parser.add_argument("-tc", "--telecommand", dest="tc_port", default=PORT_TC, type=int,
                            help="Set a port for Telecommand strings [Default: %s]" % PORT_TC)
        parser.add_argument("-tm", "--telemetry", dest="tm_port", default=PORT_TM, type=int,
                            help="Set a port for Telemetry strings [Default: %s]" % PORT_TM)
        parser.add_argument("-ps", "--svf_port", dest="svf_port", default=PORT_REMOTE, type=int,
                            help="Set a port where OBSW is listening for binary packets [Default: %s]" % PORT_REMOTE)
        parser.add_argument("-pr", "--recv_port", dest="rec_port", default=PORT_LISTEN, type=int,
                            help="Set a port where Converter is listening for binary packets [Default: %s]" % PORT_LISTEN)
        parser.add_argument('--version', action='version', version=program_version_message)
        parser.add_argument("-v", "--verbose", dest="verbose", action="store_true")
        parser.add_argument("-nc", "--nocheck", dest="nocheck", action="store_true",
                            help="If this flag is set, TM will be just returned back")
        parser.add_argument("-dr", "--dumpraw", dest="dumpraw", action="store_true",
                            help="Dump RAW TC/TM with into current directory (naming using timestamps).")
        parser.add_argument("--start-ssc", dest="initialSSC", default=0, type=int,
                            help="Staring Source Sequence Counter for TC generation")
        
        group = parser.add_argument_group('Port Mirroring (to be implemented!)')

        group.add_argument("--mirror", dest="mirror", type=int,
                            help="Use the mirror port to redirect everything received on -pr port")
        group.add_argument("--filter", dest="filter", default=None, type=str,
                            help="A packet filter string preventing matching packets to be mirrored. " +
                                "(NOT YET IMPLEMENTED!!! "
                                "Requires harmonisation with NBSR; ref. tests/validation_tests/Common/nbstreamreader.py)")
        # Process arguments
        args = parser.parse_args()

        if not args.host==None:
            IP_REMOTE=args.host
        if not args.tc_port==None:
            PORT_TC=int(args.tc_port)
        if not args.tm_port==None:
            PORT_TM=int(args.tm_port)
        if not args.svf_port==None:
            PORT_REMOTE=int(args.svf_port)
        if not args.rec_port==None:
            PORT_LISTEN=int(args.rec_port)
        verbose=args.verbose
        if not args.mirror==None:
            PORT_MIRROR=int(args.mirror)
            mirror=True
        if not args.filter==None:
            #               ----------------------------- Service Type
            #               |  -------------------------- Service SubType
            #               |  | ------------------------ Source PRID
            #               |  | | ---------------------- PCAT
            #               |  | | | -------------------- SSC
            #               |  | | | | ------------------ ??
            #               |  | | | | | ---------------- Destination PRID
            #               |  | | | | | | -------------- Time Coarse
            #               |  | | | | | | | ------------ Time Fine
            #               |  | | | | | | | | ---------- Quality
            #               |  | | | | | | | | | -------- Payload Data
            #               |  | | | | | | | | | |
            filterTM = "TM,140,3,*,*,*,*,*,*,*,*,*"
            
        NO_CHECK=args.nocheck
        CONSOLE_MODE=args.consoleio
        DUMPRAW=args.dumpraw
        
        print("---- CMD 2 PUS Converter ----\n", file = sys.stderr)
        print("Settings:", file = sys.stderr)
        print("IP="+str(IP_REMOTE)+" PORT[TC]="+str(PORT_TC)+" PORT[TM]="+str(PORT_TM), file = sys.stderr)
        if NO_CHECK:
            print("TM Checking is deactivated", file = sys.stderr)
        if verbose:
            print("Verbose output enabled", file = sys.stderr)
        
        pypus.count = 0 # initialize to default (starting at 0)
        if args.initialSSC != 0:
            pypus.count = args.initialSSC
            print("Starting SSC is %d (0x%04x)" % (int(args.initialSSC), int(args.initialSSC)), file = sys.stderr)

        print("\n--> Exit with Ctrl-C <--\n", file = sys.stderr)
            
        ## All the queues
        TCbinq=Queue.Queue(qsize)
        TMcsvq=Queue.Queue(qsize)
        TMbinq=Queue.Queue(qsize)
        
        global tm_returnq
        
        tm_returnq=Queue.Queue(qsize)
    
        
        ## Open the UDP Socket to the SVF
        
        
        
        ## Start the TC Thread
        TCacquire=TCacquireThread(TCbinq, CONSOLE_MODE, IP, PORT_TC, TMbinq, verbose=verbose)
        TCacquire.setDaemon(True)
        TCacquire.start()

        ## Start the TM Thread
        TMacquire=TMacquireThread(TMcsvq, CONSOLE_MODE, IP, PORT_TM, TMbinq, verbose=verbose)
        TMacquire.setDaemon(True)
        TMacquire.start()
        
        ## Start the UDP Connection
        UDPserver=UDPserverThread(TCbinq, IP_REMOTE, PORT_REMOTE, dumpraw=DUMPRAW, verbose=verbose)
        UDPserver.setDaemon(True)
        UDPserver.start()
        
        UDPrec=UDPrecieverThread(TMbinq, IP, PORT_LISTEN ,dumpraw=DUMPRAW, verbose=verbose
                                 ,mirror=mirror,mirrorport=PORT_MIRROR)    ## from PUS Router
        UDPrec.setDaemon(True)
        ##UDPrec=UDPrecieverThread(TMbinq,IP,9000)    ## from IOH
        UDPrec.start()
                
        
        ## Start the comparator
        compare=ComparatorThread(TMbinq,TMcsvq)
        compare.setDaemon(True)
        compare.start()
        
        while not exitFlag:
            time.sleep(1)
        return
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        if DEBUG or TESTRUN:
            raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2
    
    
"""
######################################### THREAD CLASSES ###################################################
"""
    
class UDPrecieverThread(threading.Thread):
    def __init__(self,tmq,ip,port, group=None, target=None, name=None, 
        args=(), kwargs=None, verbose=False, dumpraw=False, mirror=False, mirrorport=0):
        
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs, verbose=None)
        
        self.ip=ip
        self.port=port
        
        ## Save the queues
        self.tmqueue=tmq

        self.dumpraw=dumpraw
        self.verbose=verbose

        self.mirror = mirror
        self.mirrorport = mirrorport

        ## Open a socket
        global udprec_socket
        
        try:
            udprec_socket=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
            ##udprec_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            udprec_socket.bind((ip,port))
        except socket.error, e:
            print("Error %d: %s" % (e.args[0], e.args[1]), file = sys.stderr)
            sys.exit (1)    
            
        print("Open UDP receiver " + ("127.0.0.1" if ip == "" else ip) + ":" + str(port) + " ... ", file = sys.stderr, end="")
        
        if self.mirror == True and self.mirrorport != 0:
            print("mirroring to port " + str(self.mirrorport) + " ... ", file = sys.stderr, end="")
            
        print("\n", file = sys.stderr)
        
        udprec_socket.settimeout(0.1)

    def join(self, timeout=None):
        """ Stop the thread and wait for it to end. """
        self._stopevent.set( )
        threading.Thread.join(self, timeout)
       
    def run(self):
        global exitFlag
        while not exitFlag:
            global buffersize
            try:
                data,addr=udprec_socket.recvfrom(buffersize)
                del addr
                
                if not self.tmqueue.full(): 
                    self.tmqueue.put(data)

                    time.sleep(0) # yield
                    
                    if self.verbose==True:
                        print("UDPReceiver: put ", file = sys.stderr)
                        print (" "+" ".join("{:02x}".format(ord(c)) for c in data), file = sys.stderr)
                        
                    if self.dumpraw:
                        open(
                            "RAW-" + datetime.datetime.now().strftime('%Y-%m-%d_%X_%f') + ".tm",
                            "w"
                        ).write(data)
                else:
                    print("UDPReceiver: TM Queue Full !!! ", file = sys.stderr)
                    if self.verbose==True:
                        print (" "+" ".join("{:02x}".format(ord(c)) for c in data), file = sys.stderr)
                    
                if self.mirror == True and self.mirrorport != 0:
                    if self.verbose==True:
                        print("UDPReceiver: Mirroring to port " + str(self.mirrorport), file = sys.stderr)
                        
                    try:
                        udpmirror_socket=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
                        udpmirror_socket.sendto(data,(self.ip,self.mirrorport))
                        
                        time.sleep(0) # yield
                    except socket.error, e:
                        print("Error %d: %s" % (e.args[0], e.args[1]), file = sys.stderr)
            except:
                pass
        return
    
class UDPserverThread(threading.Thread):
    
    def __init__(self, tcq, ip, port, group=None, target=None, name=None, args=(), kwargs=None, verbose=False, dumpraw=False):
        
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs, verbose=None)
        
        self.ip=ip
        self.port=port
        
        ## Save the queues
        self.tcqueue=tcq

        self.dumpraw=dumpraw
        self.verbose=verbose

        print("UDP endpoint is " + ("127.0.0.1" if ip == "" else ip) + ":" + str(port), file = sys.stderr)

    def join(self, timeout=None):
        """ Stop the thread and wait for it to end. """
        self._stopevent.set( )
        threading.Thread.join(self, timeout)
        
    def run(self):
        global exitFlag
        
        while not exitFlag:
            try:
                element=self.tcqueue.get(True, 1)
                
                if self.verbose==True:
                    print("DEBUGUDPserver:", file = sys.stderr),
                    for c in list (element):
                        print(' '+c.encode("hex"), file = sys.stderr, end="")
                    print("")
                    
                if self.dumpraw:
                    open(
                        "RAW-" + datetime.datetime.now().strftime('%Y-%m-%d_%X_%f') + ".tc",
                        "w"
                    ).write(element)

                # we use the socket created in UDPrecieverThread::__init__
                udprec_socket.sendto(element,(self.ip,self.port))

                time.sleep(0.005) # yield
            except:
                pass
        return


class ComparatorThread(threading.Thread):
    
    def __init__(self, tmbin,tmcsv,group=None, target=None, name=None, args=(), kwargs=None, verbose=False):
        
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs, verbose=None)
        
        self.parser=pypus.csv_bin_parser()
        self.parser2=pypus.csv_bin_parser()
        self.binqueue=tmbin
        self.csvqueue=tmcsv
        
        self.verbose=verbose

    def join(self, timeout=None):
        """ Stop the thread and wait for it to end. """
        self._stopevent.set( )
        threading.Thread.join(self, timeout)
        
    def run(self):
        ## always get the two latest items and parse them in binary format and do a compare
        element1=""
        element2=""
        element_buff=""
        global exitFlag
        global return_flag
        global NO_CHECK

        error13=False
        
        while not exitFlag:
            #print("DebugCompare "+element1+" "+element2, file = sys.stderr)
            
            if not self.csvqueue.empty() and element1=="" and return_flag==False and NO_CHECK==False:
                print("get csv", file = sys.stderr)
#                 try:
#                     self.parser.parse_csv(self.csvqueue.get()[:-1])
#                 except:
#                     pass
                element1=self.csvqueue.get()
                #print(element1, file = sys.stderr)
                element_buff=copy(element1)
                # 
                try:
                    self.parser.parse_csv(element1)
                    #self.parser.binary=self.parser.packet.generate_message()
                    try:
                        self.parser.packet.datafield=(element_buff[:element_buff.rindex(";")].split(",")[8:])
                    except ValueError:
                        self.parser.packet.datafield=(element_buff.split(",")[8:])
                    element1=copy(self.parser.packet)
                except Exception as e:
                    print("ComparatorThread: Error in Parser Engine", file = sys.stderr)
                    print(e, file = sys.stderr)
                    traceback.print_exc()
                    element1=""
                    element_buff=""
                    self.parser.reset()

                
            #print(element1.datafield_header.servicetype, file = sys.stderr)
            element2=self.binqueue.get(True, None)
            if (element1!="" or return_flag==True or NO_CHECK==True):
                #print("Got bin %d bytes" % len(element2), file = sys.stderr)
                
                """
                If the global Flag is true, all packets are compared to a general packet.
                This produces an output with no errors.
                """
                if return_flag==True or NO_CHECK==True:
                    
                    error13=not(NO_CHECK)
                    
                    element1="TM,*,*,*,*,*,*,*;"
                    element_buff="TM,*,*,*,*,*,*,*;"
                    
                    try:
                        self.parser.parse_csv(element1)
                        #self.parser.binary=self.parser.packet.generate_message()
                        try:
                            self.parser.packet.datafield=(element_buff[:element_buff.rindex(";")].split(",")[8:])
                        except ValueError:
                            self.parser.packet.datafield=(element_buff.split(",")[8:])
                        element1=copy(self.parser.packet)
                    except Exception as e:
                        print("ComparatorThread: Error in Parser Engine", file = sys.stderr)
                        print(e, file = sys.stderr)
                        traceback.print_exc()
                        element1=""
                        element_buff=""
                        self.parser.reset()
                
                
            if element1!="" and element2!="" and element_buff!="":
                # Assert TM without the Time information and sequence flags
                #assert element1==element2, "Expected Packet and actual Packet are unequal"
                ret=""
                try:
                    self.parser2.parse_raw(element2, element_buff)
                    element2=copy(self.parser2.packet)
                    
                    #print(element2.datafield, file = sys.stderr)
                    #print("Asserting", file = sys.stderr)
                    #print(element1, file = sys.stderr)
                    #print(element2, file = sys.stderr)
                    
                    msg=""
                    
                    if NO_CHECK==True:
                        element1.packet_header.packet_type = element2.packet_header.packet_type
                    
                    # If an empty string is passed, the corresponding packet is ignored
                    ret=assertpackets(element1,element2)
                    
                    self.parser2.generate_string()
                    
                    msg=self.parser2.csv_string
                    msg=msg[:-1]+";"
                    
                    for el in ret:
                        msg=msg+"e"+el[:el.index(":")]+","
                    
                    if error13:
                        msg=msg+"10013"+","
                        error13=False
                    
                    if self.verbose==True:
                        print("MessageDEBUG:"+msg , file = sys.stderr)
                    
                    global tm_returnq
                    
                    tm_returnq.put(msg)
                    
                except Exception as e:
                    #raise AssertionError("Could not Parse with given packet")
                    print("Could not Parse with given Packet", file = sys.stderr)
                    print(e, file = sys.stderr)
                    traceback.print_exc()
                    msg="ParserError"
                    

                

                ## Clear the elements
                element1=""
                element2=""
                element_buff=""
                ret=""
                msg=""
                self.parser.reset()
                self.parser2.reset()
        

def __checkmismatch(para1,para2):
    if para1==None:
        return False
    elif para2==None:
        return False
    try:
        if round(float(para1[1:]),5)==round(float(para2[1:]),5):
            return False
    except:
        try:
            if para1.upper()==para2.upper():
                return False
        except:
            if para1==para2:
                return False
    return True


def assertpackets(element1,element2):
    """
    Checks if the packages are equal. Raises no error. It returns the Wrong Field instead.
    @param element1: The first packet
    @param element2: The second packet
    
    @return: The wrong fields
    """
    ret=[]
    ## Asserting Packet Header information
    # TODO: wildcards
    # Only use Packet Objects. Strings will be ignored
    if str(type(element1))=="<type 'str'>" or str(type(element2))=="<type 'str'>":
        return ret
    
    
    if __checkmismatch(element1.packet_header.version,element2.packet_header.version):
        ret.append("10001:Version Information mismatch")
    if __checkmismatch(element1.packet_header.packet_type,element2.packet_header.packet_type):
        ret.append("10002:Packet Type mismatch")
    if __checkmismatch(element1.packet_header.dfh,element2.packet_header.dfh):
        ret.append("10003:Datafield Header Flag mismatch")
    if __checkmismatch(element1.packet_header.prid,element2.packet_header.prid):
        ret.append("10004:PRID mismatch")
    if __checkmismatch(element1.packet_header.pcat,element2.packet_header.pcat):
        ret.append("10005:PCAT mismatch")
    if __checkmismatch(element1.packet_header.sequence_count,element2.packet_header.sequence_count): 
        ret.append("10006:SSC mismatch")
    if __checkmismatch(element1.packet_header.length,element2.packet_header.length): 
        ret.append("10007:Length mismatch")
    
    if __checkmismatch(element1.datafield_header.CCSDS,element2.datafield_header.CCSDS):
        ret.append("10008:CCSDS Flag mismatch")
    if __checkmismatch(element1.datafield_header.pus_version,element2.datafield_header.pus_version):
        ret.append("10009:PUS Version mismatch")
    #if element1.datafield_header.ack,element2.datafield_header.ack: ret.append("ACK Flags mismatch")
    if __checkmismatch(element1.datafield_header.servicetype,element2.datafield_header.servicetype):
        ret.append("10010:Service Type mismatch")
    if __checkmismatch(element1.datafield_header.servicesubtype,element2.datafield_header.servicesubtype):
        ret.append("10011:Service Subtype mismatch")
    if __checkmismatch(element1.datafield_header.src_destID,element2.datafield_header.src_destID):
        ret.append("10012:Source ID / Destination ID mismatch")
    
    # FIXME: Wildcards dont work
    element2.datafield=element2.datafield.split(",")
#     for el in element2.datafield:
#         element2.datafield[element2.datafield.index(el)]=el.replace(",","")
    #element2.datafield="".join(element2.datafield)
    #print(element1.datafield, file = sys.stderr)
    #print(element2.datafield, file = sys.stderr)
    for field1,field2 in zip(element1.datafield,element2.datafield):
        if __checkmismatch(field1,field2):
            print("  " + field1 + " != " + field2 + " ?", file = sys.stderr)
            if not("*" in field1) and not("*" in field2):
                ret.append("10013="+str(element1.datafield.index(field1))+": Datafield mismatch")
            else:
                return ret
    # if element1.datafield.upper()!=element2.datafield.upper(): ret.append("13: Datafield mismatch")
    return ret

class ClientChannel:
	pass

class TCPClientChannel(ClientChannel):
	def __init__(self, bindparams):
		try:
			self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 0)
			self.sock.bind(bindparams)
			self.sock.setblocking(0)
			self.sock.listen(1)
			self.sock.settimeout(0.1)
		except socket.error, e:
			print("Error %d: %s" % (e.args[0], e.args[1]), file = sys.stderr)
			time.sleep(1)
			raise
	
	def accept(self):
		self.conn, self.addr = self.sock.accept()
		#self.conn.settimeout(0.1)

	def read(self, _buffersize):
		return self.conn.recv(_buffersize)
	def write(self, msg):
		return self.conn.send(msg)

	def close(self):
		self.conn.close()

	def destroy(self):
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		print("Message acquisition shutdown... ", file = sys.stderr, end="")
		self.sock.shutdown(socket.SHUT_RDWR)
		print("done, ", file = sys.stderr, end="")
		print("close ... ", file = sys.stderr, end="")
		self.sock.close()
		print("done", file = sys.stderr)

class FDClientChannel(ClientChannel):
	def __init__(self, fd, isInput):
		self.fd = fd
		self.isInput = isInput
	
	def accept(self):
		self.conn = None
		self.addr = None
		pass

	def read(self, _buffersize):
		if self.isInput:
			return self.fd.readline(_buffersize)
		else:
			while self.fd != None:
				time.sleep(1)
			return ""

	def write(self, msg):
		if self.isInput:
			return
		else:
			self.fd.write(msg)
			# The flush is important! Otherwise the process may get into a
			# busy-waiting loop, when the output is piped...
			self.fd.flush()

	def close(self):
		if self.fd != None:
			print("Closing fileid %d..." % (self.fd.fileno()), file = sys.stderr)
			self.fd.close()
			self.fd = None

	def destroy(self):
		self.close()
		
class TCacquireThread(threading.Thread):
    """
    Thread that endlessly reads CSV formatted PUS TC strings in the form
        TC,17,1,1,12,b1001,0,255,0x0000
    either from TCP socket connection (consolemode=False; ip:port '-tc') or from STDIN (consolemode=True).
    Several TC strings are allowed but are expected to be terminated by a final semi-colon (';') character.
    Any space character (' ') is removed.
    Also special 'command' strings, preceeded by a dot character ('.') are allowed:
        '.EXIT':    close active socket connection
    
    @param consolemode: indicates if TC string is read from TCP socket (False) or STDIN (True)
    @param queue: queue for each converted binary TC
    @param tmqueue: binary queue of TMs
    @param ip: ip address string for accepting connections;
                only local addresses are allowed; empty string is translated to "127.0.0.1"
                this makes only sense if multiple network adapters (virtual or physical) are available
    @param port: port number to listen on specified IP address 
    """

    def __init__(self, queue, consolemode, ip, port, tmqueue, verbose=False):
        """ constructor, setting initial variables """

        self._stopevent = threading.Event( )
        self._sleepperiod = 1.0
        self.consolemode = consolemode
        
        threading.Thread.__init__(self)
        
        self.queue=queue
        self.parser=pypus.csv_bin_parser()
        
        # Read-only access
        self.tmqueue=tmqueue
        
        self.verbose=verbose
        
        global tc_socket

        while 1:
            try:
                if consolemode == False:
                    tc_socket = TCPClientChannel((ip,port))
                else:
                    tc_socket = FDClientChannel(sys.stdin, True)
                break
            except socket.error, e:
                if e[0] == errno.EADDRINUSE:
                    print("Retry ... ", file = sys.stderr),
                    pass
                else:
                    print("Error %d: %s" % (e.args[0], e.args[1]), file = sys.stderr)
                    sys.exit(1)
        print("Open TC acquisition " + ("127.0.0.1" if ip == "" else ip) + ":" + str(port) + " ... ", file = sys.stderr)

    def join(self, timeout=None):
        """ Stop the thread and wait for it to end. """
        self._stopevent.set( )
        threading.Thread.join(self, timeout)

    def run(self):
        self.acquirebinJob()
        return

    def acquirebinJob(self):
        global exitFlag
        tc_conn = type(socket)
        global return_flag
        global NO_CHECK

        while not exitFlag:
            try:
                tc_socket.accept()
                print("Client with TC connected: ", tc_socket.addr, file = sys.stderr)
                print("", file = sys.stderr)

                while 1:
                    try:
                        data = None
                        data=tc_socket.read(buffersize)
                        
                        # socket buffering is on, so we have to read until we get the line terminator '\n'
                        if len(data) > 0:
	                        while data[-1:] != '\n':
	                        	data+=tc_socket.read(buffersize)
	                    
                        if self.verbose==True:
                            print("Length received data: "+str(len(data)), file = sys.stderr)
                            
                        if len(data) == 0:
                            print("TC Connection closed!", file = sys.stderr)
                            tc_socket.close()
                            break
                        
                        data=data.replace('\n','')
                        
                    except Exception as ex:
                        if os.name=="nt":
                            pass
                        else:
                            print("TC Connection Error ", file = sys.stderr)
                            print(type(ex), file = sys.stderr)
                            print(ex.args, file = sys.stderr)
                            print(ex, file = sys.stderr)
                            tc_socket.close()
                            if self.consolemode:
                                os.kill(os.getpid(), signal.SIGINT)
                            break
                    
                    # Check if single data is valid
                    if data and data.replace(" ","")!="" and data!=None:
                        for dat in data.split(";"): 
                            #print(dat, file = sys.stderr)
                            
                            # Check if each command is valid
                            if dat and dat.replace(" ","")!="" and dat!=None:
                                
                                if dat.upper()==".EXIT":
                                    print("DebugTC: Client shutdown", file = sys.stderr)
                                    tc_socket.close()
                                    break
                                
                                try:
                                    if (NO_CHECK==False):
                                        if self.verbose==True:
                                            print("Returning TMs from Queue", file = sys.stderr)
                                        while not self.tmqueue.empty():
                                            return_flag=True
                                        return_flag=False
                                    
                                    if self.verbose==True:
                                        print("DEBUGTC: put\n"+dat, file = sys.stderr)
                                    
                                    self.parser.parse_csv(dat)
                                    self.parser.binary=self.parser.packet.generate_message()
                                    self.queue.put(self.parser.binary)
                                    
                                    time.sleep(0) # yield
                                    
                                except Exception as ex:
                                    print("Error in TC Parser Engine", file = sys.stderr)
                                    traceback.print_exc()

                                                                
            except Exception as ex:
                #print("TC acquiring crashed")
                #print(type(ex), file = sys.stderr)
                #print(ex.args, file = sys.stderr)
                #print(ex, file = sys.stderr)
                pass
            
        try:
            tc_socket.close()
        except:
            pass
        
        return

class TMreturnThread(threading.Thread):
    
    def __init__(self, tm_conn, verbose=False):
        
        threading.Thread.__init__(self)

        self.stopRequest = threading.Event()
        
        self.conn=tm_conn
        
        self.verbose=verbose

    def join(self, timeout=None):
        """ Stop the thread and wait for it to end. """
        self._stopevent.set( )
        threading.Thread.join(self, timeout)
    
    def run(self):
        #datetime.datetime.now().strftime('%Y-%m-%d_%X_%f')
        start_time = datetime.datetime.now() 
        
        while not self.stopRequest.isSet():
            try:
                msg=tm_returnq.get(True, 1)
            
                if self.verbose==True:
                    print("Sending response", file = sys.stderr)
                
                stop_time = datetime.datetime.now()
                
                if self.verbose==True:
                    print("Message "+msg, file = sys.stderr)
                
                time_elapsed = stop_time - start_time 
                print('Time betweenTMs (hh:mm:ss.ms) {}'.format(time_elapsed), file = sys.stderr)
                self.conn.write(msg[:-1]+";\n")
                
                start_time = stop_time
                                    
                time.sleep(0) # yield
            except:
                pass
        return

class TMacquireThread(threading.Thread):
    """
    Thread that endlessly reads CSV formatted PUS TM strings in the form
        TM,1,7,1,1,*,17,255,*,*,0x00,0x181CC000
    either from TCP socket connection (consolemode=False; ip:port '-tm') or from STDIN (consolemode=True).
    Several TM strings are allowed but are expected to be terminated by a final semi-colon (';') character.
    Any space character (' ') is removed.
    Also special 'command' strings, preceeded by a dot character ('.') are allowed:
        '.EXIT':    close active socket connection
        '.PURGE'    clearing all data from tmbin queue until it's empty w/o 'comparing' the data
        '.FLUSH'    clearing all data from tmbin queue until it's empty but comparing to default wildcarded TM
    
    @param consolemode: indicates if TC string is read from TCP socket (False) or STDIN (True)
    @param queue: queue for each converted binary TM
    @param tmbin: binary queue of TMs
    @param ip: ip address string for accepting connections;
                only local addresses are allowed; empty string is translated to "127.0.0.1"
                this makes only sense if multiple network adapters (virtual or physical) are available
    @param port: port number to listen on specified IP address ('-tm')
    """

    def __init__(self, queue, consolemode, ip, port, tmbin, verbose=False):
        
        threading.Thread.__init__(self)
        
        self.queue=queue

        global tm_socket
        
        self.tmbin=tmbin
        
        self.verbose=verbose
        
        while 1:
            try:
                if consolemode == False:
                    tm_socket = TCPClientChannel((ip,port))
                else:
                    tm_socket = FDClientChannel(sys.stdout, False)

                break
            except socket.error, e:
                if e[0] == errno.EADDRINUSE:
                    print("Retry ... ", file = sys.stderr),
                    pass
                else:
                    print("Error %d: %s" % (e.args[0], e.args[1]), file = sys.stderr)
                    sys.exit(1)

        print("Open TM acquisition " + ("127.0.0.1" if ip == "" else ip) +
              ":" + str(port) + " ... ", file = sys.stderr)

    def join(self, timeout=None):
        """ Stop the thread and wait for it to end. """
        self._stopevent.set( )
        threading.Thread.join(self, timeout)
            
    def run(self):
        self.acquireJob()
        return

    def acquireJob(self):
        global exitFlag
        tm_conn = type(socket)

        global return_flag
        global tm_socket
        
        while not exitFlag:
            try:
                tm_socket.accept()
                
                print("Client for TM connected: ", tm_socket.addr, file = sys.stderr)
                print("", file = sys.stderr)
                
                tmreturn=TMreturnThread(tm_socket, verbose=self.verbose)
                tmreturn.setDaemon(True)
                tmreturn.start()
                
                while 1:
                        
                    try:
                        data = None
                        data=tm_socket.read(buffersize)
                        
                        if len(data) == 0:
                            print("TM Connection closed!", file = sys.stderr)
                            tmreturn.stopRequest.set()
                            tm_socket.close()
                            break
                        
                        if self.verbose==True:
                            print("Received "+str(repr(data)), file = sys.stderr)
                        
                        data=data.replace('\n','')
                        
                    except Exception as ex:
                        if os.name=="nt":
                            pass
                        else:
                            print("TM Connection Error ", file = sys.stderr)
                            print(type(ex), file = sys.stderr)
                            print(ex.args, file = sys.stderr)
                            print(ex, file = sys.stderr)
                            tmreturn.stopRequest.set()
                            tm_socket.close()
                            break
                    
                    if data and data.replace(" ","")!="" and data!=None:
                        for dat in data.split(";"):
                            if dat and dat.replace(" ","")!="" and dat!=None:
                                if dat.upper()==".EXIT":
                                    print("DebugTM: Client shutdown", file = sys.stderr)
                                    tmreturn.stopRequest.set()
                                    tm_socket.close()
                                    break
                                
                                elif dat.upper()==".PURGE":
                                    print("Purging TM Queue", file = sys.stderr)
                                    while not self.tmbin.empty():
                                        buf=self.tmbin.get()
                                        del buf
                                        
                                elif dat.upper()==".FLUSH":
                                    while not self.tmbin.empty():
                                        return_flag=True
                                    return_flag=False
                                    
                                elif ("\n" in dat) or ("\r" in dat):
                                    pass
                                else:
                                    if self.verbose==True:
                                        print("TM: PUT DATA", file = sys.stderr)
                                        
                                    self.queue.put(dat)
                                    
                                    time.sleep(0) # yield
            except:
                #print("TM acquiring crashed")
                #print_exc()            
                pass
        
        return



    

"""
############################################################################################################
"""


    

if __name__ == "__main__":
    if DEBUG:
        sys.argv.append("-h")
        sys.argv.append("-v")
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'main_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())
