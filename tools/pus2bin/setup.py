'''
Created on 06.09.2013

@author: Sascha
'''
from py2exe.build_exe import py2exe 
from distutils.core import setup
import sys
from glob import glob
sys.path.append("C:\\Program Files\\Microsoft Visual Studio 9.0\\VC\\redist\\x86\\Microsoft.VC90.CRT")
data_files = [("Microsoft.VC90.CRT", glob(r'C:\Program Files\Microsoft Visual Studio 9.0\VC\redist\x86\Microsoft.VC90.CRT\*.*'))]

setup( 
     name = "CMD2PUS Converter", 
     version = "0.9", 
     author = "Sascha Wanninger", 
     author_email = "sascha.wanninger@astrium.eads.net", 
     py_modules = ["cmd2pus_converter"],
     packages=["pypus"],
     data_files=data_files+[("pypus",["pypus\\CRC.so","pypus\\CRC32.dll","pypus\\CRC64.dll"])],
     console=["cmd2pus_converter.py"],
     #data_files=data_files
     )

