'''
Created on 29.08.2013

@author: Sascha
'''

# -*- coding: utf-8 -*-
import unittest


from pypus.puspacket import csv_bin_parser
from socket import socket, AF_INET,SOCK_DGRAM

class Test(unittest.TestCase):
    """
    Testcase written for checking the pypus API written for generating messages for the Packet Utilization Standard.
    """
    
    def setUp(self):
        """
        This is the overwritten setUp function for initializing some data.
        Loads a Testfile from the Demo Folder with a Binary 140,1 Packet to Datamanagement (PRID 2, PCAT 12) and
        adds several CSV-Strings for testing the pypus-API
        """
        test_input_file=open("PUS_140_1__PRID_2__OBC_DM__mach_7.bin",'rb')
        self.test_input=test_input_file.read()
        test_input_file.close()
        
        self.csvtest_string="TC,140,1,2,12,b0010,4,3,0xAAFe,f2.34;"
        self.csvtest_string_binfield="TC,140,1,2,12,b0010,4,3,0xAAFe,b0110,f2.34,b0110;"
        self.csvtest_string_false1="TC,140,1,2,12,0010,4,3,0xAAFe,f2.34;"
        self.csvtest_string_false2="TC,140,1,2,12,b0010,4,3,0xAAFe,2.34;"
        self.csvtest_string_exp="TC,140,1,2,12,b0010,4,3,0xaa,0xaaaaaaaa,0xaaaaaaaa;"


    def tearDown(self):
        pass


    def testrawtc140_1_prid_pcat(self):
        """
        Checks Prid and PCAT for the Demo Packet
        """
        print "Raw Parsing Test"
        parser=csv_bin_parser()
        parser.parse_raw(self.test_input,self.csvtest_string_exp)
        parser.generate_string()
        print repr(parser.csv_string)
        self.assertEqual(parser.packet.packet_header.prid, 2, None)
        self.assertEqual(parser.packet.packet_header.pcat, 12, None)
        print "Ok"

    def testcsv_tc140_1(self):
        """
        Parses a CSV String and checks for PRID, PCAT and ACK-Flags.
        """
        print "CSV Parsing Test"
        parser=csv_bin_parser()
        parser.parse_csv(self.csvtest_string)
        self.assertEqual(parser.packet.packet_header.prid, 2, None)
        self.assertEqual(parser.packet.packet_header.pcat, 12, None)
        self.assertEqual(parser.packet.datafield_header.ack, 2, None)
        print repr(parser.binary)
        print "Ok"

        
    def testcsv_tc140_1_binfield(self):
        """
        Parses another CSV-String to check the conversion of odd-length bit fields.
        """
        print "Binary element test"
        
        # Initializing Parser instance
        parser=csv_bin_parser()
        # Parse the CSV
        parser.parse_csv(self.csvtest_string_binfield)
        
        # Make Assertions
        self.assertEqual(parser.packet.packet_header.prid, 2, None)
        self.assertEqual(parser.packet.packet_header.pcat, 12, None)
        self.assertEqual(parser.packet.datafield_header.ack, 2, None)
        print repr(parser.binary)
        print "Ok"

        
    def test_csv_bin_csv_loop(self):
        """
        A Loop test using a CSV input which is parsed to binary format and back to CSV and can be manually compared by output.
        """
        print "CSV-Binary-CSV Loop Test"
        
        ## Need to Parser instances because field will be overwritten if used twice
        # Init Parser Instance 1
        parser=csv_bin_parser()
        # Parse the CSV
        parser.parse_csv(self.csvtest_string)
        # Init Parser Instance 2
        parser2=csv_bin_parser()
        # Parse the generated binary
        parser2.parse_raw(parser.binary,self.csvtest_string)
        parser2.generate_string()
        # Show the results
        print repr(parser.binary)
        print self.csvtest_string
        print parser2.csv_string
        
        # Show each field with the corresponding field from the input
        for i in range(len(parser2.csv_string.split(","))):
            if parser2.csv_string.split(",")[i]=="": break
            try:
                print parser2.csv_string.split(",")[i]+" -> "+self.csvtest_string.split(",")[i]
            except:
                print "Wrong field "+str(i)
        
        self.assertEqual(parser.packet.packet_header.pcat, parser2.packet.packet_header.pcat, None)
        self.assertEqual(parser.packet.packet_header.prid, parser2.packet.packet_header.prid, None)
        print "Ok"
    
    def test_bin_csv_bin_loop(self):
        """
        Loop test with a binary as input.
        """
        
        print "Binary-CSV-Binary"
        # Init Parser Instance 1
        parser=csv_bin_parser()
        # Parse the binary data
        parser.parse_raw(self.test_input,self.csvtest_string_exp)
        parser.generate_string()
        # Init Parser Instance 2
        parser2=csv_bin_parser()
        # Parse the resulting string
        parser2.parse_csv(parser.csv_string)
        
        # Show the binary data
        print repr(self.test_input)
        print repr(parser2.binary)
        
        # Make assertions for pcat and prid
        self.assertEqual(parser.packet.packet_header.pcat, parser2.packet.packet_header.pcat, None)
        self.assertEqual(parser.packet.packet_header.prid, parser2.packet.packet_header.prid, None)
        print "Ok"
        
        
    def testcsv_tc_false(self):
        """
        Tests the raising of Errors by using wrong fields.
        """
        print "Error Test"
        parser=csv_bin_parser()
        self.assertRaises(ValueError, parser.parse_csv,self.csvtest_string_false1)
        self.assertRaises(ValueError, parser.parse_csv,self.csvtest_string_false2)
        print "Ok"
    

        
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testtc_140_1']
    unittest.main()