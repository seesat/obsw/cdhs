#include <rodos.h>
#include "MLX90393.h"

class MyPublisher : public StaticThread<> {
public:
	MyPublisher() : StaticThread("MX_Publisher") {}
	
	void init() {
		//Select I2C 1
		HAL_I2C i2c1 = new HAL_I2C(I2C_IDX1);
		
		//Create new MLX Sensor Object
		MLX90393 mlx = new MLX90393(&i2c1);
		
		//Initialize MLX sensor
		//I2C is initialized internally as well
		uint8_t init_error = mlx->init();
	}
	
	void run() {
		mlx->txyz_result.x = 0;
		
        mlx->setHallConf(0xC);

        mlx->setResolution(0,0,0);

        mlx->setGainSel(7);
            
        mlx->startBurstAll();
		
		//Read I2C Value 2 times per Second
        TIME_LOOP(0.5*SECONDS, 0.5*SECONDS){
            //Read data
            mlx->readMeasurement(); 
			
			//Write I2C Value to Middleware Topic
			i2c1.publish(mlx->txyz_result.x);
        };
	}
} mx_publisher;