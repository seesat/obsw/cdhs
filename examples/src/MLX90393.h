#include "rodos.h"
#include "hal/hal_i2c.h"
#include "hal/hal_gpio.h"
#define MLX_ADDRESS (0x0c<<1)
class MLX90393
{
public:


  enum { STATUS_OK = 0, STATUS_ERROR = 0xff } return_status_t;
  enum { Z_FLAG = 0x8, Y_FLAG = 0x4, X_FLAG = 0x2, T_FLAG = 0x1 } axis_flag_t;
  enum { I2C_BASE_ADDR = 0x0c };
  enum { GAIN_SEL_REG = 0x0, GAIN_SEL_MASK = 0x0070, GAIN_SEL_SHIFT = 4 };
  enum { HALLCONF_REG = 0x0, HALLCONF_MASK = 0x000f, HALLCONF_SHIFT = 0 };
  enum { BURST_SEL_REG = 0x1, BURST_SEL_MASK = 0x03c0, BURST_SEL_SHIFT = 6};
  enum { TRIG_INT_SEL_REG = 0x1, TRIG_INT_SEL_MASK = 0x8000, TRIG_INT_SEL_SHIFT = 15 };
  enum { EXT_TRIG_REG = 0x1, EXT_TRIG_MASK = 0x0800, EXT_TRIG_SHIFT = 11 };
  enum { OSR_REG = 0x2, OSR_MASK = 0x0003, OSR_SHIFT = 0 };
  enum { OSR2_REG = 0x2, OSR2_MASK = 0x1800, OSR2_SHIFT = 11 };
  enum { DIG_FLT_REG = 0x2, DIG_FLT_MASK = 0x001c, DIG_FLT_SHIFT = 2 };
  enum { RES_XYZ_REG = 0x2, RES_XYZ_MASK = 0x07e0, RES_XYZ_SHIFT = 5 };
  enum { TCMP_EN_REG = 0x1, TCMP_EN_MASK = 0x0400, TCMP_EN_SHIFT = 10 };
  enum { X_OFFSET_REG = 4, Y_OFFSET_REG = 5, Z_OFFSET_REG = 6 };
  enum { WOXY_THRESHOLD_REG = 7, WOZ_THRESHOLD_REG = 8, WOT_THRESHOLD_REG = 9 };
  enum { BURST_MODE_BIT = 0x80, WAKE_ON_CHANGE_BIT = 0x40,
         POLLING_MODE_BIT = 0x20, ERROR_BIT = 0x10, EEC_BIT = 0x08,
         RESET_BIT = 0x04, D1_BIT = 0x02, D0_BIT = 0x01 };

  enum {
    CMD_NOP = 0x00,
    CMD_EXIT = 0x80,
    CMD_START_BURST = 0x10,
    CMD_WAKE_ON_CHANGE = 0x20,
    CMD_START_MEASUREMENT = 0x30,
    CMD_READ_MEASUREMENT = 0x40,
    CMD_READ_REGISTER = 0x50,
    CMD_WRITE_REGISTER = 0x60,
    CMD_MEMORY_RECALL = 0xd0,
    CMD_MEMORY_STORE = 0xe0,
    CMD_RESET = 0xf0,
    CMD_READ_MEASUREMENT_ALL = 0x4F
  };

  struct txyz
  {
    float t;
    float x;
    float y;
    float z;
  };
  struct txyzRaw
  {
    uint16_t t;
    uint16_t x;
    uint16_t y;
    uint16_t z;
  };
  MLX90393(HAL_I2C* i2c);
  ~MLX90393();
  uint8_t init();
  HAL_I2C* i2c;

  //device commands
  uint8_t startBurstAll();
  bool isOK(uint8_t status);
  uint8_t startSingleMeasurementAll();
  uint8_t sendCommand(uint8_t cmd);
  uint8_t readMeasurement();
  uint8_t readRegister(uint8_t address, uint16_t data);
  uint8_t reset();
  uint8_t writeRegister(uint8_t address, uint16_t data);
  uint8_t convertRaw();
  uint8_t checkStatus(uint8_t status);


  //return B in uT, temp in C
  uint8_t readData();

  //functions to set and get current configuration of the sensor
  uint8_t setGainSel(uint8_t gain_sel);
  uint8_t getGainSel();
  uint8_t setHallConf(uint8_t hallconf);
  uint8_t getHallConf();
  uint8_t setDigitalFiltering(uint8_t dig_flt);
  uint8_t getDigitalFiltering();
  uint8_t setResolution(uint8_t res_x, uint8_t res_y, uint8_t res_z);
  uint8_t getResolution();
  uint8_t setTemperatureCompensation(uint8_t enable);
  uint8_t getTemperatureCompensation();
  uint8_t setOffsets(uint16_t x, uint16_t y, uint16_t z);
  uint8_t setThreshold(uint16_t xy_thresh, uint16_t z_thresh, uint16_t t_thresh);

 

  float gain_multipliers[8];
  float base_xy_sens_hc0;
  float base_z_sens_hc0;
  float base_xy_sens_hc0xc;
  float base_z_sens_hc0xc;
  uint8_t gain_sel;
  uint8_t hallconf;
  uint8_t res_x;
  uint8_t res_y;
  uint8_t res_z;
  uint8_t dig_flt;
  uint8_t tcmp_enable;
  uint16_t offset_x;
  uint16_t offset_y;
  uint16_t offset_z;
  uint16_t xy_thresh;
  uint16_t z_thresh;
  uint16_t t_thresh;



  txyzRaw txyz_result;
  txyz txyz;
   private:
};


  


  

