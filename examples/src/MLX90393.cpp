#include "rodos.h"
#include "hal/hal_i2c.h"
#include "hal/hal_gpio.h"
#include "MLX90393.h"
#include <string.h>





MLX90393:: MLX90393(HAL_I2C* i2c)
{
	this->i2c=i2c;
    //gain steps derived from datasheet section 15.1.4 tables
    this->gain_multipliers[0] = 5.f;
    this->gain_multipliers[1] = 4.f;
    this->gain_multipliers[2] = 3.f;
    this->gain_multipliers[3] = 2.5f;
    this->gain_multipliers[4] = 2.f;
    this->gain_multipliers[5] = 1.66666667f;
    this->gain_multipliers[6] = 1.33333333f;
    this->gain_multipliers[7] = 1.f;

    // from datasheet
    // for hallconf = 0
    this->base_xy_sens_hc0 = 0.196f;
    this->base_z_sens_hc0 = 0.316f;
    // for hallconf = 0xc
    this->base_xy_sens_hc0xc = 0.150f;
    this->base_z_sens_hc0xc = 0.242f;
}

MLX90393:: ~MLX90393()
{


}


uint8_t MLX90393:: init()
{
	i2c->init();
	
    uint8_t status1 = setGainSel(4);
    /*uint8_t status2 = setResolution();
    uint8_t status3 = setDigitalFiltering();
    uint8_t status4 = setTemperatureCompensation();*/

    return status1; //| status2 | status3 | status4;
}

uint8_t MLX90393:: sendCommand(uint8_t cmd)
{
	uint8_t data;
	i2c->writeRead(MLX_ADDRESS,&cmd, 1,&data,1);
	return data;
}

uint8_t MLX90393:: reset()
{
	uint8_t status = sendCommand(CMD_RESET);
	//give the device time to reset
	//delay(10);

	return status;
}

bool MLX90393:: isOK(uint8_t status)
{
	if((status & ERROR_BIT)==0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

uint8_t MLX90393 :: checkStatus(uint8_t status)
{
	
}

uint8_t MLX90393:: startSingleMeasurementAll()
{
	uint8_t cmd = CMD_START_MEASUREMENT|0xf;
	return sendCommand(cmd);
}

uint8_t MLX90393:: startBurstAll()
{
	uint8_t cmd = CMD_START_BURST | 0xf;
	uint8_t status = this->sendCommand(cmd);
	return status;
}

uint8_t MLX90393:: readMeasurement( )
{
	char test[] = "Error Not set\r\n";
	uint8_t cmd = CMD_READ_MEASUREMENT_ALL;
	uint8_t buffer[9];
	uint8_t cout = 9;

	uint8_t ret = i2c->writeRead(MLX_ADDRESS, &cmd, 1, buffer,9);
	PRINTF("Retval %u \n\r",ret);

	for(int i = 0; i<9;i++)
	{
		PRINTF("Buffer %u is %b\n\r",i,buffer[i]);
	};
	uint8_t i = 1;
	//set the result for t
	this->txyz_result.t = ((uint16_t)(buffer[i]<<8) | buffer[i+1]);
	
	i+=2;
	//set the result for x
	this->txyz_result.x = ((uint16_t)(buffer[i]<<8) | buffer[i+1]);
	i+=2;
	//set the result for y
	this->txyz_result.y = ((uint16_t)(buffer[i]<<8) | buffer[i+1]);
	i+=2;
	//set the result for z
	this->txyz_result.z = ((uint16_t)(buffer[i]<<8) | buffer[i+1]);
	i+=2;
	//return status byte
	return buffer[0];
}

uint8_t MLX90393::readRegister(uint8_t address, uint16_t data)
{
	uint8_t sbuffer[2];
	sbuffer[0] = CMD_READ_REGISTER;
	sbuffer[1] = (address & 0x3f)<<2;

	uint8_t rbuffer[3];
	i2c-> writeRead(MLX_ADDRESS, sbuffer, 2, rbuffer,3);

	
	data = (uint16_t)(rbuffer[1]<<8) | rbuffer[2];

	return rbuffer[0];
}

uint8_t MLX90393:: writeRegister(uint8_t address, uint16_t data)
{
	uint8_t sbuffer[4];
	uint8_t status;
	sbuffer[0] = CMD_WRITE_REGISTER;
	sbuffer[1] = (data&0xff00)>>8;
	sbuffer[2] = (uint8_t)(data&0x00ff);
	sbuffer[3] = address << 2;

	i2c-> writeRead(MLX_ADDRESS,sbuffer,4,&status,1);
	
	return status;
}

uint8_t MLX90393:: convertRaw()
{
	
    


	
}

uint8_t MLX90393:: readData()
{
    uint8_t status1 = this->startSingleMeasurementAll();
    //short Delay for Approximation
    for(int i = 0; i<5000;i++){};

    
    uint8_t status2 = this->readMeasurement();
    convertRaw();
    return status1;
}

uint8_t MLX90393:: setGainSel(uint8_t gain_sel)
{
    uint16_t old_val;
    uint8_t status1 = this->readRegister(GAIN_SEL_REG, old_val);
    if (!isOK(status1))
    {
    return status1;
    }
    uint8_t status2 = this->writeRegister(GAIN_SEL_REG, (old_val & ~GAIN_SEL_MASK) |
                                  	((uint16_t(gain_sel) << GAIN_SEL_SHIFT) & GAIN_SEL_MASK));
    return checkStatus(status1) | checkStatus(status2);
}

uint8_t MLX90393:: getGainSel()
{
    uint16_t reg_val;
    uint8_t status = this->readRegister(GAIN_SEL_REG, reg_val);
 	this->gain_sel = (reg_val & GAIN_SEL_MASK) >> GAIN_SEL_SHIFT;
    return status;
}

uint8_t MLX90393 :: setHallConf(uint8_t hallconfig)
{
    uint16_t old_val;
    uint8_t status1 = this->readRegister(HALLCONF_REG, old_val);
    if (!isOK(status1))
    {
    return status1;
    }
	//store the hallconf in the object
 	this->hallconf = hallconfig;
    uint8_t status2 = this->writeRegister(HALLCONF_REG, (old_val & ~HALLCONF_MASK) |
                                  	((uint16_t(hallconfig) << HALLCONF_SHIFT) & HALLCONF_MASK));
    return status2;
}

uint8_t MLX90393:: getHallConf()
{
    uint16_t reg_val;
    uint8_t status = this->readRegister(HALLCONF_REG, reg_val);
 	this->hallconf = (reg_val & HALLCONF_MASK) >> HALLCONF_SHIFT;
    return status;
}

uint8_t MLX90393 :: setDigitalFiltering(uint8_t dig_filt)
{
	uint16_t old_val;
  	uint8_t status1 = this->readRegister(DIG_FLT_REG, old_val);
  	if(!isOK(status1))
  	{
		return status1;
 	}
 	this->dig_flt = dig_filt;
  	uint8_t status2 = this->writeRegister(DIG_FLT_REG, (old_val & ~DIG_FLT_MASK) |
                                  	((uint16_t(dig_filt) << DIG_FLT_SHIFT) & DIG_FLT_MASK));
  	return status2;

}

uint8_t MLX90393 :: getDigitalFiltering()
{
	uint16_t reg_val;
  	uint8_t status = this->readRegister(DIG_FLT_REG, reg_val);
 	this->dig_flt = (reg_val & DIG_FLT_MASK) >> DIG_FLT_SHIFT;
  	return status;

}

uint8_t MLX90393 :: setResolution(uint8_t res_x, uint8_t res_y, uint8_t res_z)
{
 	this->res_x = res_x;
 	this->res_y = res_y;
 	this->res_z = res_z;
	uint16_t res_xyz = ((res_z & 0x3)<<4) | ((res_y & 0x3)<<2) | (res_x & 0x3);
  	uint16_t old_val;
  	uint8_t status1 = this->readRegister(RES_XYZ_REG, old_val);
	if(!isOK(status1))
  	{
		return status1;
 	}
  	uint8_t status2 = this->writeRegister(RES_XYZ_REG,
                                  (old_val & ~RES_XYZ_MASK) |
                                  (res_xyz << RES_XYZ_SHIFT) & RES_XYZ_MASK);
  	return status2;

}

uint8_t MLX90393 :: getResolution()
{
	uint16_t reg_val;
  	uint8_t status = this->readRegister(RES_XYZ_REG, reg_val);
  	uint8_t res_xyz = (reg_val & RES_XYZ_MASK) >> RES_XYZ_SHIFT;
 	this->res_x = (res_xyz >> 0) & 0x3;
 	this->res_y = (res_xyz >> 2) & 0x3;
 	this->res_z = (res_xyz >> 4) & 0x3;
  	return status;

}

uint8_t MLX90393 :: setTemperatureCompensation(uint8_t enable)
{
	uint8_t tcmp_enable = 0;
	if(enable == 1)
	{
	 this->tcmp_enable = 1;
	}
	uint16_t old_val;
	uint8_t status1 = readRegister(TCMP_EN_REG, old_val);
	if(!isOK(status1))
  	{
		return status1;
 	}
	uint8_t status2 = writeRegister(TCMP_EN_REG, (old_val & ~TCMP_EN_MASK)|
									(((uint16_t)tcmp_enable << TCMP_EN_SHIFT) & TCMP_EN_MASK));

	return status2;
}

uint8_t MLX90393 :: getTemperatureCompensation()
{
	uint16_t reg_val;
	uint8_t status = readRegister(TCMP_EN_REG, reg_val);
 	this->tcmp_enable = (reg_val & TCMP_EN_MASK) >> TCMP_EN_SHIFT;
	return status;

}

uint8_t MLX90393 :: setOffsets(uint16_t x, uint16_t y, uint16_t z)
{
 this->offset_x = x;
 this->offset_y = y;
 this->offset_z = z;
	uint8_t status1 = writeRegister(X_OFFSET_REG, x);
	uint8_t status2 = writeRegister(Y_OFFSET_REG, y);
	uint8_t status3 = writeRegister(Z_OFFSET_REG, z);

	return status1;
}

uint8_t MLX90393 :: setThreshold(uint16_t xy_thresh, uint16_t z_thresh, uint16_t t_thresh)
{
	this->xy_thresh =  xy_thresh;
	this->z_thresh = z_thresh;
	this->t_thresh = t_thresh;
	uint8_t status1 = writeRegister(WOXY_THRESHOLD_REG, xy_thresh);
	uint8_t status2 = writeRegister(WOZ_THRESHOLD_REG, z_thresh);
	uint8_t status3 = writeRegister(WOT_THRESHOLD_REG, t_thresh);

	return status1;   
}