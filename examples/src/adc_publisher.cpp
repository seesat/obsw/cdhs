#include <rodos.h>

class MyPublisher : public StaticThread<> {
public:
	MyPublisher() : StaticThread("ADC_Publisher") {}
	
	void init() {
		//Select ADC 1
		HAL_ADC adc1 = new HAL_ADC(ADC_IDX1);

		//Enable Channel 5 (Channel 5 on ADC1 is Pin PA5, see /rodos/api/hal/hal_adc.h)
		adc1.init(ADC_CH_005);

		//Set Resolution to 12 Bit == 4096 values
		adc1.config(ADC_PARAMETER_RESOLUTION, 12);
	}
	
	void run() {
		uint16_t adc_value = 0;

		//Read ADC Value 2 times per Second
		TIME_LOOP(0.5*SECONDS, 0.5*SECONDS){
			//Returns Value <0 if Error occured, see /rodos/api/hal/hal_adc.h
			adc_value = adc1.read(ADC_CH_005);

			//Write ADC Value to Middleware Topic
			adc1.publish(adc_value);
		}
	}
} adc_publisher;