#include "obc/StateError.h"

#include "obc/OBCThread.h"

StateError::StateError() {
}

StateError::~StateError() {
}

void StateError::execute(OBCThread &obc) {
    PRINTF("[Error]: There went something wrong :(.\n");
    obc.suspendCallerUntil(END_OF_TIME);
}
