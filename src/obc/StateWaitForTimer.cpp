#include "obc/StateWaitForTimer.h"

#include "obc/OBCThread.h"

StateWaitForTimer::StateWaitForTimer() {
}

StateWaitForTimer::~StateWaitForTimer() {
}

void StateWaitForTimer::execute(OBCThread &obc) {
    PRINTF("[Timer]: Started timer for %d seconds.\n", WAIT_TIME_IN_SEC);
    obc.suspendCallerUntil(NOW() + WAIT_TIME_IN_SEC * SECONDS);
    PRINTF("[Timer]: Timer ended.\n");

    bool success = deploySADM();
    if (success) {
        PRINTF("[Timer]: SADM deployed successfully\n");
        obc.changeState(OBC_STATE::TMTC_POWERED);
    } else {
        PRINTF("[Timer]: SADM not deployed successfully\n");
        PRINTF("(TODO) [Timer]: Report error and change state\n");
        obc.changeState(OBC_STATE::ERROR);
    }
}

bool StateWaitForTimer::deploySADM() {
    PRINTF("[Timer]: Deploing SADM\n");
    return true;
}
