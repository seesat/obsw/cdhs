#include "obc/StatePowerOn.h"

#include "obc/OBCThread.h"

StatePowerOn::StatePowerOn() {
}

StatePowerOn::~StatePowerOn() {
}

void StatePowerOn::execute(OBCThread &obc) {
    bool success = performPOST();
    if (!success) {
        PRINTF("[PowerOn]: Error in performing POST.\n");
        obc.changeState(OBC_STATE::ERROR);
        return;
    }

    bootOBSW();

    success = checkUHFState();
    if (success) {
        PRINTF("[PowerOn]: UHF is deployed.\n");
        obc.changeState(OBC_STATE::RUNNING);
        return;
    } else {
        PRINTF("[PowerOn]: UHF is not deployed\n");
        success = checkSADMState();
        if (success) {
            PRINTF("[PowerOn]: SADM is deployed.\n");
            obc.changeState(OBC_STATE::TMTC_POWERED);
            return;
        } else {
            PRINTF("[PowerOn]: SAMD is stowed.\n");
            obc.changeState(OBC_STATE::WAIT_FOR_TIMER);
            return;
        }
    }
}

bool StatePowerOn::performPOST() {
    PRINTF("[PowerOn]: Performing POST\n");
    return true;
}

void StatePowerOn::bootOBSW() {
    PRINTF("[PowerOn]: Booting OBSW\n");
}

bool StatePowerOn::checkUHFState() {
    PRINTF("[PowerOn]: Checking UHF state\n");
    return false;
}

bool StatePowerOn::checkSADMState() {
    PRINTF("[PowerOn]: Checking SADM state\n");
    return true;
}
