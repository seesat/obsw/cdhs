#include "obc/StateRunning.h"

#include "obc/OBCThread.h"

#include "sensors/Magnetometer.h"
#include "sensors/NullMagnetometer.h"
#include "sensors/SimulatedMagnetometer.h"

#include <math.h>

#define M_PI 3.14159265358979323846 /* pi */

HAL_GPIO ledOrange(GPIO_061);

Magnetometer *initMagnetometer() {
#ifndef MAGNETOMETER
#error Define a magenetometer
#endif
#if MAGNETOMETER == 0
    return new (xmalloc(sizeof(NullMagnetometer))) NullMagnetometer(2);
#elif MAGNETOMETER == 1
    return new (xmalloc(sizeof(NullMagnetometer))) SimulatedMagnetometer(2);
#elif MAGNETOMETER == 2
    PRINTF("MLX90393 magnetometer\n");
#error is not yet implemented
#else
#error No magnetometer defined
#endif
}

StateRunning::StateRunning() : ledGreen(HAL_GPIO(GPIO_060)), magnetometer(initMagnetometer()) {
}

StateRunning::~StateRunning() {
}

void StateRunning::init() {
    ledGreen.init(true);
    magnetometer->init(0);

    ledOrange.init(true);
}

void StateRunning::execute(OBCThread &obc) {
    PRINTF("[Running]: OBCThread is now running.\n");

    [[maybe_unused]]uint32_t on = 0;

    [[maybe_unused]]int32_t year, month, day, hour, min;
    [[maybe_unused]]double sec;

    for (obc.setPeriodicBeat(NOW(), 200 * MILLISECONDS); obc.suspendUntilNextBeat(), true;) {
        // ledGreen.setPins(on);
        // ledOrange.setPins(1 - on);
        // on = 1 - on;

        // TimeModel::localTime2Calendar(NOW(), year, month, day, hour, min, sec);

        // MagnetometerMeasurement measurement = magnetometer->readMeasurement();

        // double heading_deg = static_cast<double>(atan2(measurement.y, measurement.x)) * (180 / M_PI);
        // std::string heading_text = parseCompassHeading2str(heading_deg);
        // PRINTF("[%02d:%02d] Magnetometer: { x:%3.2fuT, y:%3.2fuT, z:%3.2fuT, temp:%3.2f } Heading: %4.3f°; %s\n", (int)min, (int)sec,
        //        static_cast<double>(measurement.x), static_cast<double>(measurement.y), static_cast<double>(measurement.z),
        //        static_cast<double>(measurement.temperature), heading_deg, heading_text.c_str());
    }

    obc.suspendCallerUntil(END_OF_TIME);
}

// std::string StateRunning::parseCompassHeading2str(const double heading_deg) const {
//     if (heading_deg > 157.5) {
//         return "S";
//     } else if (heading_deg > 112.5) {
//         return "SE";
//     } else if (heading_deg > 67.5) {
//         return "E";
//     } else if (heading_deg > 22.5) {
//         return "NE";
//     } else if (heading_deg > -22.5) {
//         return "N";
//     } else if (heading_deg > -67.5) {
//         return "NW";
//     } else if (heading_deg > -112.5) {
//         return "W";
//     } else if (heading_deg > -157.5) {
//         return "SW";
//     } else {
//         return "S";
//     }
// }
