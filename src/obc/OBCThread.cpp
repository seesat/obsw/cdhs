#include "obc/OBCThread.h"

#include "obc/OBCState.h"
#include "obc/StateError.h"
#include "obc/StatePowerOn.h"
#include "obc/StateRunning.h"
#include "obc/StateTMTCPowered.h"
#include "obc/StateWaitForTimer.h"

#include <map>
#include <memory>

OBCThread obc;

OBCThread::OBCThread() : StaticThread("OBC-Thread", 200) {
    // enumToClassMap = std::map<const OBC_STATE, OBCState *>{
    //     {OBC_STATE::ERROR, initStateClass<StateError>()},
    //     {OBC_STATE::POWER_ON, initStateClass<StatePowerOn>()},
    //     {OBC_STATE::RUNNING, initStateClass<StateRunning>()},
    //     {OBC_STATE::TMTC_POWERED, initStateClass<StateTMTCPowered>()},
    //     {OBC_STATE::WAIT_FOR_TIMER, initStateClass<StateWaitForTimer>()},
    // };

    // currState = enumToClassMap.at(OBC_STATE::POWER_ON);
}

OBCThread::~OBCThread() {
}

void OBCThread::changeState([[maybe_unused]]const OBC_STATE state) {
    //    currState = enumToClassMap.at(state);
}

void OBCThread::init() {
    //    for (auto pair : enumToClassMap) {
    //        pair.second->init();
    //    }
}

void OBCThread::run() {
    while (currState != NULL) {
        //      currState->execute(obc);
    }

    PRINTF("[OBC]: Waiting for end of time :(\n");
    suspendCallerUntil(END_OF_TIME);
}
