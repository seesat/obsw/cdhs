#include "obc/StateTMTCPowered.h"

#include "obc/OBCThread.h"

StateTMTCPowered::StateTMTCPowered() {
}

StateTMTCPowered::~StateTMTCPowered() {
}

void StateTMTCPowered::execute(OBCThread &obc) {
    powerOnTMTC();

    if (deployUHF()) {
        PRINTF("[TMTC Powered]: Deployment successful\n");
        obc.changeState(OBC_STATE::RUNNING);
    } else {
        PRINTF("[TMTC Powered]: Not able to deploy UHF\n");
        obc.changeState(OBC_STATE::ERROR);
    }
}

void StateTMTCPowered::powerOnTMTC() {
    PRINTF("[TMTC Powered]: Powering on TMTC\n");
}

bool StateTMTCPowered::deployUHF() {
    PRINTF("[TMTC Powered]: Deploing UHF\n");
    return true;
}
