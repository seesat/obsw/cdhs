//
// Created by ericj on 14.07.2023.
//

#include "mms/types/Notification.h"

void notify (const std::exception& exception, const request& request) {
    std::cout << "During the start of the following request: {memory_id: ";
    std::cout << request.memory_id;
    std::cout << ", base_id: ";
    std::cout << request.base_id;
    std::cout << ", section_number: ";
    std::cout << request.section_number;
    std::cout << "} The following exception occurred: ";
    std::cout << typeid(exception).name();
    std::cout << ": ";
    std::cout << exception.what();
}
