//
// Created by ericj on 25.05.2023.
//

#include <list>
#include <stdexcept>

#include "mms/LoadMemoryData.h"
#include "mms/types/Notification.h"


void LoadMemoryData (const request& request) {
    Memory* memory;

    try {
	//TODO: This check does not work. to be revised
        memory = memories.at(request.memory_id);
    }
    catch (const std::out_of_range& e) {
        notify(e, request);
        return;
    }

    if (memory == nullptr) {
        notify(std::invalid_argument("The memory identifier is unknown." ), request);
        return;
    }


    if (!memory->is_known_base_id(request.base_id)) {
        notify(std::invalid_argument( "The memory object that the base identifier referred to is unknown." ), request);
        return;
    }

    //check write operation is permitted and memory area is not write protected
    if (!memory->write_is_permitted(request.memory_id)) {
        notify(std::invalid_argument( "The memory or the memory object is write protected." ), request);
        return;
    }

    //check write operation is permitted and memory area is not write protected
    if (memory->maximum_object_size < sizeof(request.instructions)) {
        notify(std::invalid_argument( "The maximum memory size of the memory object was exceeded." ), request);
        return;
    }

    //verify request instructions
    for (const memory_instruction& instruction : request.instructions) {
        //validate offset value is not too big
        if (instruction.offset > memory->maximum_object_size) {
            notify(std::invalid_argument("Offset value exceeds maximum object size."), request);
            return;
        }

        //validate data is not too big
        if (instruction.data.size() > memory->maximum_object_size) {
            notify(std::invalid_argument("Data is too large and exceeds maximum object size."), request);
            return;
        }

        //validate data length aligns with memory access alignment constraint
        if (memory->access_alignment_constraint % instruction.data_length != 0) {
            notify(std::invalid_argument("Data is not a multiple of the memory access alignment constraint."), request);
            return;
        }

        //verify data values with checksum
        //if (!data_is_correct(write_address, instruction.checksum)) {
          //  throw exception("Corrupt instruction data.");
        //};
    }

    //write data
    for (memory_instruction instruction : request.instructions) {
        memory->write(request.base_id, instruction);
    }
}
