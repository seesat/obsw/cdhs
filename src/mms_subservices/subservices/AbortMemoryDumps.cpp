//
// Created by ericj on 11.07.2023.
//

#include "mms/AbortMemoryDumps.h"

// application data field is omitted as defined in PUS, therefore, no function argument is required
void AbortMemoryDumps () {
    abort_memory_dump = true;
}
