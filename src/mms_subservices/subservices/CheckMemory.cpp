//
// Created by ericj on 15.07.2023.
//

#include <stdexcept>
#include "mms/CheckMemory.h"
#include "mms/types/Notification.h"

void CheckMemory(const request& request) {
    Memory* memory;

    try {
        memory = memories.at(request.memory_id);
    }
    catch (const std::out_of_range& e) {
        notify(e, request);
        return;
    }

    if (memory == nullptr) {
        notify(std::invalid_argument("The memory identifier is unknown." ), request);
        return;
    }
}
