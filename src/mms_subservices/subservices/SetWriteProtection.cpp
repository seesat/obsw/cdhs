//
// Created by ericj on 14.07.2023.
//


#include <stdexcept>
#include "mms/SetWriteProtection.h"
#include "mms/types/Notification.h"

void EnableWriteProtection (const request& request) {
    Memory* memory;

    try {
        memory = memories.at(request.memory_id);
    }
    catch (const std::out_of_range& e) {
        notify(e, request);
        return;
    }

    if (memory == nullptr) {
        notify(std::invalid_argument("The memory identifier is unknown." ), request);
        return;
    }

    if (!memory->is_write_protectable) {
        notify(std::invalid_argument("The memory that was referred to by the request is not write protectable."), request);
        return;
    }

    memory->set_write_protection(true);
}

void DisableWriteProtection (const request& request) {
    Memory* memory;

    try {
        memory = memories.at(request.memory_id);
    }
    catch (const std::out_of_range& e) {
        notify(e, request);
        return;
    }

    if (memory == nullptr) {
        notify(std::invalid_argument("The memory identifier is unknown." ), request);
        return;
    }

    if (!memory->is_write_protectable) {
        notify(std::invalid_argument("The memory that was referred to by the request is not write protectable."), request);
        return;
    }

    memory->set_write_protection(false);
}
