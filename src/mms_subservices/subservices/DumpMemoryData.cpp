//
// Created by ericj on 11.07.2023.
//

#include <stdexcept>
#include "mms/DumpMemoryData.h"
#include "mms/types/Notification.h"

std::vector<std::string*> DumpMemoryData (const request& request) {
    Memory* memory;

    try {
        memory = memories.at(request.memory_id);
    }
    catch (const std::out_of_range& e) {
        notify(e, request);
        return std::vector<std::string*> {};
    }

    if (memory == nullptr) {
        notify(std::invalid_argument("The memory identifier is unknown." ), request);
        return std::vector<std::string*> {};
    }

    if (!memory->is_known_base_id(request.base_id)) {
        notify(std::invalid_argument( "The memory object that the base identifier referred to is unknown." ), request);
        return std::vector<std::string*> {};
    }

    //check read operation is permitted
    if (!memory->read_is_permitted(request.memory_id)) {
        notify(std::invalid_argument( "The memory or the memory object is not available for a read operation" ), request);
        return std::vector<std::string*> {};
    }

    //The maximum packet size of the CCSDS space packet protocol is 65542 bytes.
    const memory_instruction last_instruction = request.instructions[request.instructions.size() - 1];
    if (last_instruction.offset + last_instruction.data_length > 65542) {
        notify(std::invalid_argument( "The memory data requested to dump exceeds the maximum packet size of the CCSDS space packet protocol." ), request);
        return std::vector<std::string*> {};
    }

    std::vector<std::string*> data_buffer;

    //verify request instructions
    for (const memory_instruction& instruction : request.instructions) {
        //validate offset value is not too big
        if (instruction.offset > memory->maximum_object_size) {
            notify(std::invalid_argument("Offset value exceeds maximum object size."), request);
            break;
        }

        //validate data is not too big
        if (instruction.data.size() > memory->maximum_object_size) {
            notify(std::invalid_argument("Data is too large and exceeds maximum object size."), request);
            break;
        }

        //validate data length aligns with memory access alignment constraint
        if (memory->access_alignment_constraint % instruction.data_length != 0) {
            notify(std::invalid_argument("Data is not a multiple of the memory access alignment constraint."), request);
            break;
        }

        //verify data values with checksum
        //if (!data_is_correct(write_address, instruction.checksum)) {
        //  throw exception("Corrupt instruction data.");
        //};

        std::string* data = memory->read(request.base_id, instruction);
        data_buffer.push_back(data);
    }

    return data_buffer;

}
