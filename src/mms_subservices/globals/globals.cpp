//
// Created by ericj on 11.07.2023.
//

#include "array"

#include "mms/types/types.h"
#include "mms/types/Memory.h"
#include "mms/types/RamMemory.h"
#include "mms/types/FlashMemory.h"


FlashMemory STM32_FLASH(512, 32, true);
RamMemory STM32_RAM(512, 32, false);

std::array<Memory*, 2> memories {
        &STM32_FLASH,
        &STM32_RAM
};

bool abort_memory_dump = false;
