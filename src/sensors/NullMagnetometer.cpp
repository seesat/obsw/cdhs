#include "sensors/NullMagnetometer.h"

NullMagnetometer::NullMagnetometer(const uint8_t id) : Magnetometer(id) {
}

NullMagnetometer::~NullMagnetometer() {
}

void NullMagnetometer::init(int mode) {
    this->currMode = mode;
}

bool NullMagnetometer::reset() {
    return false;
}

MagnetometerMeasurement NullMagnetometer::readMeasurement() {
    return MagnetometerMeasurement(0, 0, 0, 0);
}
