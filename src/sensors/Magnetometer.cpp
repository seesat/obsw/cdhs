#include "sensors/Magnetometer.h"

Magnetometer::Magnetometer(const uint32_t id) : id(id)
{
}

Magnetometer::~Magnetometer()
{
}

uint32_t Magnetometer::getID() const
{
    return id;
}
