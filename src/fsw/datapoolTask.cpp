#include "rodos.h"
#include "topics.h"
#include "services.h"
#include "PusTmPacket.h"
#include "service20.h"
#include "datapool.h"
#include "datapoolConfig.h"

static Application receiverName("Service20Receiver");

static SyncFifo<PusTcPacket, 5> pusPacketFifo;
static Subscriber ioPacketReceiver(incomingTcPacket, pusPacketFifo, "pusPacketFifo");

static SyncFifo<SetParamTcData, 10> setRequestBuffer;
static Subscriber setRequestReceiverBuf(setParamTcData, setRequestBuffer, "setRequestSub");

static SyncFifo<GetParamTcData, 10> getRequestBuffer;
static Subscriber getRequestReceiverBuf(getParamTcData, getRequestBuffer, "getRequestSub");

Semaphore dataPoolLock;
Datapool datapool;



//Packet receiver for getting packets from the main packet pus and answering with telemetry
// This is forwarding all TCs that don't create a report to the dedicated receivers
// All TCs creating TM reports are handled here
class S20TcReceiver : public StaticThread<> {

    
    void run () {
    PusTcPacket packet;

    datapool.init();
        while (1) {
                pusPacketFifo.syncGet(packet);

            if (packet.secondaryHeader.serviceType == PUS_S20_ONBOARD_PARAM)
            {	
            if (packet.secondaryHeader.serviceSubtype == 1)
            {
                SetParamTcData setTcParam;
		RODOS::memcpy(&setTcParam, packet.data, packet.header.packetDataLength-5-1);

                for (int i = 0; i<setTcParam.n1; i++)
                {
                dataPoolLock.enter();

                if (datapool.getType(setTcParam.paramValue[i].paramId) == E_DP_TYPE_INTEGER)
                {
                datapool.setValue(setTcParam.paramValue[i].paramId, (int)ntohl(setTcParam.paramValue[i].value.iValue));
                }
                else if (datapool.getType(setTcParam.paramValue[i].paramId) == E_DP_TYPE_FLOAT)
                {
                    setTcParam.paramValue[i].value.iValue = ntohl(setTcParam.paramValue[i].value.iValue);
                    datapool.setValue(setTcParam.paramValue[i].paramId, setTcParam.paramValue[i].value.fValue);
                }			

                dataPoolLock.leave();
                }

            }
        if (packet.secondaryHeader.serviceSubtype == 2)
        {
            GetParamTcData getParamTcData;
            ParamValueReportData valueReport;
            PusTmPacket tmReportPacket;
            
	    RODOS::memcpy(&getParamTcData, packet.data, packet.header.packetDataLength-5-1);
            valueReport.n1 = getParamTcData.n1;
            for (int i = 0; i<getParamTcData.n1; i++)
            {
            valueReport.paramValue[i].paramId = getParamTcData.paramId[i];
            dataPoolLock.enter();

            if (datapool.getType(getParamTcData.paramId[i]) == E_DP_TYPE_INTEGER)
            {
                valueReport.paramValue[i].value.iValue = htonl(datapool.getValue(getParamTcData.paramId[i]));
            }
            else if (datapool.getType(getParamTcData.paramId[i]) == E_DP_TYPE_FLOAT)
            {
                valueReport.paramValue[i].value.fValue = datapool.getEngValue(getParamTcData.paramId[i]);
            }
            dataPoolLock.leave();
            }

            unsigned short length = 14+1+ sizeof(getParamTcData.n1) + getParamTcData.n1*sizeof(ParamValuePair);
            PusTmPacket_create(&tmReportPacket, 20, 3, length, 0, &valueReport, sizeof(getParamTcData.n1) + getParamTcData.n1*sizeof(ParamValuePair));

            outgoingTmPacket.publish(tmReportPacket);
        }
        }
        }
    }
} s20TcReceiver;


// Receiver for internal and external parameter get requests
class Service20SetReceiver : public StaticThread<> {

    
    void run () {
    SetParamTcData setParamTcData;
        while (1) {

            setRequestBuffer.syncGet(setParamTcData);
        for (int i = 0; i<setParamTcData.n1; i++)
        {
        dataPoolLock.enter();
        if(datapool.getType(setParamTcData.paramValue[i].paramId) == E_DP_TYPE_INTEGER)
        {
            datapool.setValue(setParamTcData.paramValue[i].paramId, setParamTcData.paramValue[i].value.iValue);
        }
        else if(datapool.getType(setParamTcData.paramValue[i].paramId) == E_DP_TYPE_FLOAT)
        {
            datapool.setValue(setParamTcData.paramValue[i].paramId, setParamTcData.paramValue[i].value.fValue);
        }
        dataPoolLock.leave();
        }
        

        }
    }
} s20SetRequestReceiverSync;


// Receiver for internal parameter get requests
class Service20GetReceiver : public StaticThread<> {

    
    void run () {
    GetParamTcData getParamTcData;
    ParamValueReportData valueReport;
    
        while (1) {
            getRequestBuffer.syncGet(getParamTcData);
        valueReport.n1 = getParamTcData.n1;
        for (int i = 0; i<getParamTcData.n1; i++)
        {
        valueReport.paramValue[i].paramId = getParamTcData.paramId[i];
        dataPoolLock.enter();
        if (datapool.getType(getParamTcData.paramId[i]) == E_DP_TYPE_INTEGER)
        {
            valueReport.paramValue[i].value.iValue = datapool.getValue(getParamTcData.paramId[i]);
        }
        else if (datapool.getType(getParamTcData.paramId[i]) == E_DP_TYPE_FLOAT)
        {
            valueReport.paramValue[i].value.fValue = datapool.getEngValue(getParamTcData.paramId[i]);
        }
        dataPoolLock.leave();
        }

        paramValueReportData.publish(valueReport);
        }
    }
} s20GetRequestReceiverSync;


/******************************/
