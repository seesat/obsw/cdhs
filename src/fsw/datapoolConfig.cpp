#include "datapoolConfig.h"


DatapoolConfigEntry datapoolConfig [] =
{
    {
	.id = E_PARAMID_MAIN_MODE,
	.name = "PARAM_MAIN_MODE",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_SUB_MODE,
	.name = "PARAM_SUB_MODE",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_MON2,
	.name = "PARAM_MON2",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_MON4,
	.name = "PARAM_MON4",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_MON5,
	.name = "PARAM_MON5",
	.type = E_DP_TYPE_FLOAT
    },
    {
	.id = E_PARAMID_TEST1,
	.name = "PARAM_TEST1",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_TEST2,
	.name = "PARAM_TEST2",
	.type = E_DP_TYPE_INTEGER
    },
    /* GNC PARAMETER*/
    {
	.id = E_PARAMID_DISTANCE,
	.name = "PARAM_DISTANCE",
	.type = E_DP_TYPE_FLOAT
    },
    {
	.id = E_PARAMID_GNC_MODE,
	.name = "PARAM_GNC_MODE",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_CMD_MOVE_FLAG,
	.name = "PARAM_CMD_MOVE_FLAG",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_CMD_MOVE_SPEED,
	.name = "PARAM_CMD_MOVE_SPEED",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_CMD_MOVE_DIR,
	.name = "PARAM_CMD_MOVE_DIR",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_CMD_MOVE_TURN,
	.name = "PARAM_CMD_MOVE_TURN",
	.type = E_DP_TYPE_INTEGER
    },
    /* Manipulator Values */
    {
	.id = E_PARAMID_CMD_ARM_FLAG,
	.name = "PARAM_CMD_ARM_FLAG",
	.type = E_DP_TYPE_INTEGER
    },
    {
        .id = E_PARAMID_CMD_ARM_ANGLE,
	.name = "PARAM_CMD_ARM_ANGLE",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_CMD_HAND_FLAG,
	.name = "PARAM_CMD_HAND_FLAG",
	.type = E_DP_TYPE_INTEGER
    },
    {
        .id = E_PARAMID_CMD_HAND_ANGLE,
	.name = "PARAM_CMD_HAND_ANGLE",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_CMD_WRIST_FLAG,
	.name = "PARAM_CMD_WRIST_FLAG",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_CMD_WRIST_ANGLE,
	.name = "PARAM_CMD_WRIST_ANGLE",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_CMD_CLAW_FLAG,
	.name = "PARAM_CMD_CLAW_FLAG",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_CMD_CLAW_ANGLE,
	.name = "PARAM_CMD_CLAW_ANGLE",
	.type = E_DP_TYPE_INTEGER
    },

    /* Parameters for Bus/GNSR Failure Scenario */
    {
	.id = E_PARAMID_FDIR_MODE,
	.name = "PARAM_FDIR_MODE",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_GNSR1_HEALTH,
	.name = "PARAM_GNSR1_HEALTH",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_GNSR1_PWR,
	.name = "PARAM_GNSR1_PWR",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_GNSR1_TM,
	.name = "PARAM_GNSR1_TM",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_GNSR2_HEALTH,
	.name = "PARAM_GNSR1_HEALTH",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_GNSR2_PWR,
	.name = "PARAM_GNSR2_PWR",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_GNSR2_TM,
	.name = "PARAM_GNSR2_TM",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_BUS1_ERRCNT,
	.name = "PARAM_BUS1_ERRCNT",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_BUS2_ERRCNT,
	.name = "PARAM_BUS2_ERRCNT",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_BUS1_RETRY,
	.name = "PARAM_BUS1_RETRY",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_BUS2_RETRY,
	.name = "PARAM_BUS2_RETRY",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_OBC_RECONF,
	.name = "PARAM_OBC_RECONF",
	.type = E_DP_TYPE_INTEGER
    },
    {
	.id = E_PARAMID_FDIR_CYCLE_COUNT,
	.name = "PARAM_FDIR_CYCLE_COUNT",
	.type = E_DP_TYPE_INTEGER
    },
	{
	.id = E_PARAMID_FDIR_AGENDA_ENABLED,
	.name = "PARAM_FDIR_AGENDA_ENABLED",
	.type = E_DP_TYPE_INTEGER
    },
};
    
