#include "rodos.h"
#include "topics.h"
#include "services.h"
#include "service20.h"
#include "service5.h"
#include "PusTmPacket.h"

#include "datapool.h"

Semaphore monitoringLock;

typedef struct monitoringEntry
{
    unsigned char paramId;
    unsigned char type;
    unsigned char validityParam;
    unsigned int mask;
    unsigned int validityExpected;
    unsigned char interval;
    unsigned char intervalCounter;
    unsigned char repetition;
    unsigned char repetitionCounter;
    unsigned char checkType;
    unsigned int expectedValue;
    unsigned char expectedEventId;
    ParamValue lowLimit;
    unsigned char lowLimitEventId;
    ParamValue upperLimit;
    unsigned char upperLimitEventId;
} MonitoringEntry;

#define DONT_CARE 0
#define CHECK_TYPE_EXPECTED 0
#define CHECK_TYPE_LIMITS 1

#define N_MONITORINGS 3

MonitoringEntry monitoringList[N_MONITORINGS] =
{
    {0x01, TYPE_UINT, 0x02, 0xFFFFFFFF, 1, 10, DONT_CARE, 1, DONT_CARE, CHECK_TYPE_EXPECTED, 0x00, 0xAB, {DONT_CARE}, DONT_CARE, {DONT_CARE}, DONT_CARE},
    {0x04, TYPE_UINT, 0x02, 0xFFFFFFFF, 1, 10, DONT_CARE, 1, DONT_CARE, CHECK_TYPE_LIMITS,  DONT_CARE, DONT_CARE, {.iValue = 0x01}, 0xCA, {.iValue = 0x04}, 0xCB},
    {0x05, TYPE_FLOAT, 0x02, 0xFFFFFFFF, 1, 10, DONT_CARE, 1, DONT_CARE, CHECK_TYPE_LIMITS, DONT_CARE, DONT_CARE, {.fValue = 3.0}, 0xCC, {.fValue = 5.0}, 0xCB},
};

static Application monitoringApp("CyclicMonitoring");

class Monitoring : public StaticThread<> {

    EventReportData eventReport;
    
    bool checkValidity(MonitoringEntry entry)
    {
	bool result;
	dataPoolLock.enter();
	unsigned int maskedValue = datapool.getValue(entry.validityParam) & entry.mask;
	dataPoolLock.leave();
	result = (maskedValue == entry.validityExpected);
	return result;
    }
    void run () {
        
	TIME_LOOP(2* SECONDS, 100*MILLISECONDS) {
	    for (int i = 0; i<N_MONITORINGS; i++)
	    {
		if(checkValidity(monitoringList[i]))
		{
		    monitoringList[i].intervalCounter++;
		    if (monitoringList[i].intervalCounter == monitoringList[i].interval)
		    {
			monitoringList[i].intervalCounter = 0;
			if(monitoringList[i].checkType == CHECK_TYPE_EXPECTED)
			{
			    dataPoolLock.enter();
			    unsigned int value = datapool.getValue(monitoringList[i].paramId);
			    dataPoolLock.leave();
			    if (value != monitoringList[i].expectedValue)
			    {
				// This counter wraps on purpose, so monitoring is "reenabled" in due time
				monitoringList[i].repetitionCounter++;
				if( monitoringList[i].repetition == monitoringList[i].repetitionCounter)
				{
				    EventReportData_create(&eventReport, 3,
							   monitoringList[i].expectedEventId,
							   monitoringList[i].paramId, 0, 0, 0, 0);
				    pusEventBus.publish(eventReport);
				}
			    }
			    else
			    {
				//Reenable monitoring when okay again
				monitoringList[i].repetitionCounter=0;
			    }
			}
			else if (monitoringList[i].checkType == CHECK_TYPE_LIMITS)
			{
			    if(monitoringList[i].type == TYPE_UINT)
			    {
				dataPoolLock.enter();
				unsigned int value = datapool.getValue(monitoringList[i].paramId);
				dataPoolLock.leave();

				if (value < monitoringList[i].lowLimit.iValue)
				{
				    // This counter wraps on purpose, so monitoring is "reenabled" in due time
				    monitoringList[i].repetitionCounter++;
				    if( monitoringList[i].repetition == monitoringList[i].repetitionCounter)
				    {
					EventReportData_create(&eventReport, 3,
							       monitoringList[i].lowLimitEventId,
							       monitoringList[i].paramId, 0, 0, 0, 0);
					pusEventBus.publish(eventReport);
				    }
				}
				else if (value > monitoringList[i].upperLimit.iValue)
				{
				    // This counter wraps on purpose, so monitoring is "reenabled" in due time
				    monitoringList[i].repetitionCounter++;
				    if( monitoringList[i].repetition == monitoringList[i].repetitionCounter)
				    {
					EventReportData_create(&eventReport, 3,
							       monitoringList[i].upperLimitEventId,
							       monitoringList[i].paramId, 0, 0, 0, 0);
					pusEventBus.publish(eventReport);
				    }
				}
				else
				{
				    //Reenable monitoring when okay again
				    monitoringList[i].repetitionCounter=0;
				}
			    }
			    else if(monitoringList[i].type == TYPE_FLOAT)
			    {
				dataPoolLock.enter();
				ParamValue value;
				value.fValue = datapool.getEngValue(monitoringList[i].paramId);
				dataPoolLock.leave();

				if (value.fValue < monitoringList[i].lowLimit.fValue)
				{
				    // This counter wraps on purpose, so monitoring is "reenabled" in due time
				    monitoringList[i].repetitionCounter++;
				    if( monitoringList[i].repetition == monitoringList[i].repetitionCounter)
				    {
					EventReportData_create(&eventReport, 3,
							       monitoringList[i].lowLimitEventId,
							       monitoringList[i].paramId, 0, 0, 0, 0);
					pusEventBus.publish(eventReport);
				    }
				}
				else if (value.fValue > monitoringList[i].upperLimit.fValue)
				{
				    // This counter wraps on purpose, so monitoring is "reenabled" in due time
				    monitoringList[i].repetitionCounter++;
				    if( monitoringList[i].repetition == monitoringList[i].repetitionCounter)
				    {
					EventReportData_create(&eventReport, 3,
							       monitoringList[i].upperLimitEventId,
							       monitoringList[i].paramId, 0, 0, 0, 0);
					pusEventBus.publish(eventReport);
				    }
				}
				else
				{
				    //Reenable monitoring when okay again
				    monitoringList[i].repetitionCounter = 0;
				}
			    }
			}
		    }
		}
		else
		{
		    monitoringList[i].repetitionCounter = 0;
		}
	    }
	}
    }
} monitoring;
