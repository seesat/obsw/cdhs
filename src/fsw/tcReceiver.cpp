#include "rodos.h"
#include "topics.h"


static Application  receiverName("UDPReceiver",1100);

static UDPIn in(9000);


class TcReceiver : public StaticThread<> {

public:
    TcReceiver() : StaticThread<>("SenderSimple") {}

    PusTcPacket packet;
    unsigned char userData[4096];
    
    void run () {
	TIME_LOOP(1*SECONDS, 20*MILLISECONDS) {
	    while(int len = in.get(userData, 4096)) {
		PusTcPacket packet;
		RODOS::memcpy(&packet, userData, len);
		// Correct byte order
		packet.header.packetDataLength = ntohs(packet.header.packetDataLength);
		packet.secondaryHeader.sourceId = ntohs(packet.secondaryHeader.sourceId);
		
		incomingTcPacket.publish(packet);
		
	    }
	}
    }
};

/******************************/

static TcReceiver tcReceiver;
