#include "rodos.h"
#include "topics.h"
#include "packet_definition.h"
#include "service20.h"

// Main packet busses
Topic<PusTcPacket> incomingTcPacket(-1, "incomingTcPacket");
Topic<PusTmPacket> outgoingTmPacket(-1, "outgoingTmPacket");

// Service 5
Topic<EventReportData> pusEventBus(-1, "pusEventBus");

// Service 20
Topic<GetParamTcData> getParamTcData(-1, "getParamTcData");
Topic<SetParamTcData> setParamTcData(-1, "setParamTcData");
Topic<ParamValueReportData> paramValueReportData(-1, "paramValueReportData");
