#include "rodos.h"
#include "datapool.h"
#include "datapoolConfig.h"



DatapoolParameter::DatapoolParameter ()
{
    this->id = 0;
    this->name = "unassigned";
    this->rawValue = 0;
    this->engValue = 0.0;
    this->type = E_DP_TYPE_INTEGER;
}
DatapoolParameter::DatapoolParameter (unsigned char id, const char * name, DpTypes type)
{
    this->id = id;
    this->name = name;
    this->rawValue = 0;
    this->engValue = 0.0;
    this->type = type;
}


void Datapool::init(){
    const unsigned int configSize = sizeof(datapoolConfig) / sizeof (DatapoolConfigEntry);

    for (unsigned int i = 0; i < NB_PARAMETERS; i++)
    {
	params[i].setId(datapoolConfig[i].id);
	params[i].setName(datapoolConfig[i].name);
	params[i].setType(datapoolConfig[i].type);
    }
}
