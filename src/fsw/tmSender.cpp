#include "rodos.h"
#include "topics.h"



static Application senderName("UDPSender");

static SyncFifo<PusTmPacket, 5> pusTmPacketFifo;
static Subscriber tmPacketSender(outgoingTmPacket, pusTmPacketFifo, "pusTmPacketFifo");


static UDPOut out(9999);

class Sender : public StaticThread<> {
    unsigned char outbuf[4096];
    PusTmPacket packet;
    
    void run () {
	int  cnt = 0;
	while(1) {
	    pusTmPacketFifo.syncGet(packet);
	    unsigned short length = ( packet.header.packetDataLength);
	    packet.header.packetDataLength = htons(packet.header.packetDataLength);
	    RODOS::memcpy(outbuf, &packet,length +7);
	    out.send(outbuf, length+7);
	}
    }
};

static Sender   sender;
