#include "rodos.h"
#include "topics.h"
#include "services.h"
#include "service5.h"
#include "PusTmPacket.h"
#include "service20.h"
#include "datapool.h"
#include "datapoolConfig.h"
extern "C" {
#include "clips.h"
}

static Application RuleBasedFdir("Fdir");

static Fifo<PusTcPacket, 5> pusPacketFifo;
static Subscriber ioPacketReceiver(incomingTcPacket, pusPacketFifo, "pusPacketFifo");

static Fifo<EventReportData, 10> eventReportBuffer;
static Subscriber eventReceiverBuf(pusEventBus, eventReportBuffer, "eventReportSub");


/* Gettimeofday function to force clips compatibility */
extern "C" {
int _gettimeofday( struct timeval *tv, void *tzvp )
{
    return 0;
}
}



void setDatapoolParam(  Environment *env,  UDFContext *udfc,  UDFValue *out)
{
    UDFValue theArg;
    SetParamTcData setParamData;
    setParamData.n1 = 1;
    
    UDFFirstArgument(udfc,INTEGER_BIT,&theArg);
    setParamData.paramValue[0].paramId = (unsigned char)theArg.integerValue->contents;

    UDFNextArgument(udfc, NUMBER_BITS, &theArg);
    if (theArg.header->type == INTEGER_TYPE)
    {
	setParamData.paramValue[0].value.iValue = theArg.integerValue->contents;
    }
    else
    {
	//TODO: This needs to be protected when type != float
	setParamData.paramValue[0].value.fValue = theArg.floatValue->contents;
    }

    setParamTcData.publish(setParamData);
    
    out->lexemeValue = CreateBoolean(env,1);
}

void reportEvent(  Environment *env,  UDFContext *udfc,  UDFValue *out)
{
    UDFValue theArg;
    unsigned int  eventId;
    unsigned int severity;
    unsigned char param1;
    unsigned char param2;
    unsigned char param3;
    unsigned char param4;
    unsigned char param5;
    EventReportData eventReport;
    // Retrieve the first argument.
    UDFFirstArgument(udfc,INTEGER_BIT,&theArg);
    eventId = theArg.integerValue->contents;

    UDFNextArgument(udfc, INTEGER_BIT, &theArg);
    severity = theArg.integerValue->contents;

    UDFNextArgument(udfc, INTEGER_BIT, &theArg);
    param1 = (unsigned char)theArg.integerValue->contents;
    UDFNextArgument(udfc, INTEGER_BIT, &theArg);
    param2 = (unsigned char)theArg.integerValue->contents;
    UDFNextArgument(udfc, INTEGER_BIT, &theArg);
    param3 = (unsigned char)theArg.integerValue->contents;
    UDFNextArgument(udfc, INTEGER_BIT, &theArg);
    param4 = (unsigned char)theArg.integerValue->contents;
    UDFNextArgument(udfc, INTEGER_BIT, &theArg);
    param5 = (unsigned char)theArg.integerValue->contents;

    EventReportData_create(&eventReport, severity, eventId,
			   param1, param2, param3, param4, param5);
    pusEventBus.publish(eventReport);
    out->lexemeValue = CreateBoolean(env,1);
}


void UserFunctions(Environment *env)
{
    AddUDF(env, "reportEvent", "b", 7, 7, NULL, reportEvent, "reportEvent", NULL);
    AddUDF(env, "setDatapoolParam", "b", 2, 2, "ld", setDatapoolParam, "setDatapoolParam", NULL);
}


class ClipsAdapter {
    Environment* env;
    EventReportData eventReport;
    bool fileLoadErr;
public:
    ClipsAdapter() {
	env = CreateEnvironment();
	UserFunctions(env);
	loadClipsFile("../fdir/fdirTemplates.clp");
	loadClipsFile("../fdir/fdirPerception.clp");
	loadClipsFile("../fdir/fdirRules.clp");
	loadClipsFile("../fdir/fdirActionRules.clp");
	Eval(env, "(reset)", NULL);
	Reset(env);
    }

    ~ClipsAdapter() {
	DestroyEnvironment(env);
    }

    void loadClipsFile(const char *fileName) {
	LoadError loadErr;
	loadErr = Load(env, fileName);
	if (loadErr != LE_NO_ERROR)
	{
	    fileLoadErr = true;
	}
    }
    

    void updateParameter(DatapoolParameter* parameter) {
	FactBuilder * theFB;
	CLIPSValue cv;

	theFB = CreateFactBuilder(env,"datapoolParam");
	FBPutSlotInteger(theFB,"id", parameter->getId());
	FBPutSlotSymbol(theFB, "name", parameter->getName());
    
	if (parameter->getType() == E_DP_TYPE_INTEGER)
	{
	    FBPutSlotInteger(theFB,"value", parameter->getRawValue());
	}
	else if (parameter->getType() == E_DP_TYPE_FLOAT)
	{
	    FBPutSlotFloat(theFB,"value", parameter->getEngValue());
	}

	Fact* result = FBAssert(theFB);
	if (result == NULL) {
	    PRINTF("error in assert");
	}
	FBDispose(theFB);
    }

    void assertEvents()
    {
	while(eventReportBuffer.get(eventReport))
	{
	    FactBuilder * theFB;

	    theFB = CreateFactBuilder(env,"pusEvent");
	    FBPutSlotInteger(theFB,"id", eventReport.eventId);
	    FBPutSlotInteger(theFB,"severity", eventReport.severity);
	    FBPutSlotInteger(theFB,"param1", eventReport.eventInfo[0]);
	    FBPutSlotInteger(theFB,"param2", eventReport.eventInfo[1]);
	    FBPutSlotInteger(theFB,"param3", eventReport.eventInfo[2]);
	    FBPutSlotInteger(theFB,"param4", eventReport.eventInfo[3]);
	    FBPutSlotInteger(theFB,"param5", eventReport.eventInfo[4]);

	    Fact* result = FBAssert(theFB);
	    if (result == NULL) {
		FactBuilderError error = FBError(env);
		PRINTF("error in assert %d\n", error);
	    }
	    FBDispose(theFB);
	}
            
    }

    void schedule() {
	long timeBeforeAssert, timeBeforePerception, timeBeforeExec, timeFinish;
        {
	    //PRIORITY_CEILER_IN_SCOPE();
	    timeBeforeAssert = NOW();
	    for (unsigned int i = 0; i < NB_PARAMETERS; i++)
	    {
		this->updateParameter(datapool.getParameter(datapoolConfig[i].id));
	    }

	    this->assertEvents();

	    timeBeforePerception = NOW();
	    Eval(env, "(focus MAIN PERCEPTION)", NULL);
	    Run(env,-1);
	    /* Generate Agenda event */
	    int agendaEnable = datapool.getValue(E_PARAMID_FDIR_AGENDA_ENABLED);
	    if(agendaEnable == 1)
		generateAgendaEvent();
	    timeBeforeExec = NOW();
	    Eval(env, "(focus FDIR ACTION)", NULL);
	    Run(env,-1);

	    timeFinish = NOW();

	    long durationAssert = timeBeforePerception - timeBeforeAssert;
	    long durationPerception = timeBeforeExec - timeBeforePerception;
	    long durationRules = timeFinish - timeBeforeExec;

	    long durationClips = durationPerception + durationRules ;

	    // PRINTF("FDIR %ld  %ld ( %ld - %ld )\n",
	    // 	   durationAssert, durationClips, durationPerception, durationRules);
	}
/* Increase the cycle count */
	int fdirCycleCount = datapool.getValue(E_PARAMID_FDIR_CYCLE_COUNT);
	fdirCycleCount++;
	datapool.setValue(E_PARAMID_FDIR_CYCLE_COUNT, fdirCycleCount);
    }

    void printFacts() {
	Eval(env,"(do-for-all-facts ((?f datapoolParam)) TRUE (ppfact ?f))",NULL);
	Eval(env,"(do-for-all-facts ((?f pusEvent)) TRUE (ppfact ?f))",NULL);
    }

    void generateAgendaEvent() {
        unsigned char parameters[5] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
        int cnt = 0;
        Activation * a = NULL;
        a = GetNextActivation(env, NULL);
        while ((a != NULL) && (cnt < 5))
        {
            const char * name = ActivationRuleName(a);
            parameters[cnt] = atoi(name);
            a = GetNextActivation(env, a);
            cnt++;
        }
        EventReportData eventReport;
        EventReportData_create(&eventReport, 3,
			       0xAF,
			       parameters[0], 
			       parameters[1], 
			       parameters[2], 
			       parameters[3], 
			       parameters[4]);
        pusEventBus.publish(eventReport);
    }

    void saveEnv()
    {
        PRINTF("FDIR : TC Save Facts\n");
        Eval(env, "(focus FDIR)", NULL);
        long result = BinarySaveFacts(env, "fdirEnvFdir.bin", LOCAL_SAVE);
        Eval(env, "(focus ACTION)", NULL);
        result += BinarySaveFacts(env, "fdirEnvAction.bin", LOCAL_SAVE);

    }
    void loadEnv()
    {
	long result = 0;
        this->resetEnv();
        PRINTF("FDIR : TC Load Facts\n");
        Eval(env, "(focus FDIR)", NULL);
        result += BinaryLoadFacts(env, "fdirEnvFdir.bin");
        Eval(env, "(focus ACTION)", NULL);
        result += BinaryLoadFacts(env, "fdirEnvAction.bin");
        
    }
    void resetEnv()
    {
        Reset(env);
    }
    
};
    

class FdirMainLoop : public StaticThread<> {

    void run() {
	ClipsAdapter* clipsAdapter = new ClipsAdapter();
	TIME_LOOP(2 * SECONDS, 250 * MILLISECONDS) {
	    PusTcPacket packet;
	    while(pusPacketFifo.get(packet))
	    {
		if (packet.secondaryHeader.serviceType == PUS_S150_FDIR_SERVICE)
		{	
		    if (packet.secondaryHeader.serviceSubtype == 1)
		    {
			clipsAdapter->saveEnv();
		    }
		    if (packet.secondaryHeader.serviceSubtype == 2)
		    {
			clipsAdapter->loadEnv();
		    }
		    if (packet.secondaryHeader.serviceSubtype == 3)
		    {
			clipsAdapter->resetEnv();
		    }
		}
	    }
	    clipsAdapter->schedule();
	}

	delete clipsAdapter;
    }

} FdirMainLoop;

