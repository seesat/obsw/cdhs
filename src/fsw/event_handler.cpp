#include "rodos.h"
#include "topics.h"
#include "services.h"
#include "service5.h"
#include "PusTmPacket.h"



static Application receiverName("Service5EventReportReceiver");

static SyncFifo<EventReportData, 10> eventReportBuffer;
static Subscriber eventReceiverBuf(pusEventBus, eventReportBuffer, "eventReportSub");

class S5EventReceiver : public StaticThread<> {

    
    void run () {
	EventReportData eventReport;
        
        while (1) {
            eventReportBuffer.syncGet(eventReport);

		{
		    PusTmPacket tmReportPacket;
		    
		    unsigned short length = 14+1+ sizeof(EventReportData)-sizeof(unsigned char);
		    PusTmPacket_create(&tmReportPacket, 5, eventReport.severity, length, 0, &eventReport.eventId, sizeof(EventReportData)-sizeof(unsigned char));

		    outgoingTmPacket.publish(tmReportPacket);
		}
        }
    }
} s5EventReportReceiver;
