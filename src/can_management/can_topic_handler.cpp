/**************************************************************************************************
  * @file    can_topic_handler.cpp
  * @author  Christian Ültzhöfer, Philipp Thümler
  * @version V0.0.0
  * @date    03-June-2023
  * @brief   This file contains all function implementations for the communication with the
  *          SeeSat flight software via topics and the publisher-subscriber-protocol
  ************************************************************************************************/

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
#include "can_management/can_topic_handler.h"

/**************************************************************************************************
 * Global variables
 *************************************************************************************************/
Topic<T_CanMsg> CanCmd(CAN_CMD_TOPIC_ID, "CanCmd");
Topic<T_CanMsg> CanAcq(CAN_ACQ_TOPIC_ID, "CanAcq");

/**************************************************************************************************
 * Class implementation
 *************************************************************************************************/

/* Public Member Function Implementation */

CanTopicHandler::CanTopicHandler() : Subscriber(CanCmd, "CanManagement Subscriber; Topic: CanCmd") {}

CanTopicHandler::~CanTopicHandler() {}

uint32_t CanTopicHandler::put([[maybe_unused]]const uint32_t topicId, [[maybe_unused]]const size_t len, void* data, [[maybe_unused]]const NetMsgInfo& netMsgInfo) {
  return (mCanMsgRxFifo.put(*(T_CanMsg*)data)) ? 1U : 0U;
}

void CanTopicHandler::responseToClient(const T_CanMsgScheduleEntry* pt_CanMsgScheduleEntry) {
  /* create T_CanMsg from T_CanMsgScheduleEntry */
  T_CanMsg canMsg;
  canMsg.u32_Uuid = pt_CanMsgScheduleEntry->u32_Uuid;
  std::memcpy(canMsg.u8_MsgData, pt_CanMsgScheduleEntry->u8_Data, pt_CanMsgScheduleEntry->CanProfileEntry_t.u8_Size);
  canMsg.u8_Size = pt_CanMsgScheduleEntry->CanProfileEntry_t.u8_Size;
  canMsg.u32_EntryId = pt_CanMsgScheduleEntry->CanProfileEntry_t.u32_EntryId;
  canMsg.u32_DeviceId = pt_CanMsgScheduleEntry->CanProfileEntry_t.u32_DeviceId;
  canMsg.MsgTransferType_t = pt_CanMsgScheduleEntry->CanProfileEntry_t.MsgTransferType_t;

  /* publish response on CAN Acquisition topic */
  CanAcq.publish(canMsg);
}
