/**************************************************************************************************
  * @file    can_management.cpp
  * @author  Christian Ültzhöfer, Philipp Thümler
  * @version V0.0.0
  * @date    03-June-2023
  * @brief   This file contains all function implementations for the CAN management for the
  *          SeeSat Central Data Handling System
  ************************************************************************************************/

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
#include "can_management/can_management.h"

/**************************************************************************************************
 * Public - Class implementation
 *************************************************************************************************/

/* Private Member Function Implementation */
void CanManagement::resetSlotCounter(void) {
  this->u32_mCurrentSlot = 0U;
}

void CanManagement::incrementSlotCounter(void) {
  this->u32_mCurrentSlot++;
}

bool CanManagement::isMsgInSlot(void) const {
  const uint32_t *pt_CurrentCanProfileEntryId = &(mCanProfile_t.at(u32_mCurrentSlot).u32_EntryId);

  return (mCanMsgSchedule_t.count(*pt_CurrentCanProfileEntryId) == 1U);
}

bool CanManagement::isCurrentSlotBeforeEntryIdSlot(const uint32_t &entryId) const {
  T_CanProfile::const_iterator canProfileIterator = mCanProfile_t.cbegin();
  std::advance(canProfileIterator, u32_mCurrentSlot);

  for(; canProfileIterator != mCanProfile_t.cend(); ++canProfileIterator) {
    if(canProfileIterator->u32_EntryId == entryId) {
      return true;
    }
  }
  return false;
}

bool CanManagement::isEntryInSchedule(const T_CanMsgScheduleEntry &scheduleEntry, const T_CanMsgSchedule &schedule) const {
  return schedule.count(scheduleEntry.CanProfileEntry_t.u32_EntryId) > 0;
}

bool CanManagement::isCanProfileEntryValid(const T_CanProfileEntry &canProfileEntry) const {
  for(uint32_t i = 0U; i < mCanProfile_t.size(); i++) {
    if(canProfileEntry.u32_EntryId == mCanProfile_t.at(i).u32_EntryId) {
      if(canProfileEntry.u32_DeviceId != mCanProfile_t.at(i).u32_DeviceId) {
        return false;
      }
      if(canProfileEntry.MsgTransferType_t == mCanProfile_t.at(i).MsgTransferType_t) {
        return false;
      }
      if(canProfileEntry.u8_Size == mCanProfile_t.at(i).u8_Size) {
        return false;
      }
      return true;
    }
  }
  return false;
}


void CanManagement::updateSchedules(void) {
  mCanMsgSchedule_t = mCanMsgScheduleNext_t;
  mCanMsgScheduleNext_t.clear();
}

void CanManagement::sendMsg(void) {
  if (isMsgInSlot()) {
      mCanStatus_t = CAN_STATUS_WRITE_PENDING;
      T_CanMsgScheduleEntry *pt_CanMsgScheduleEntry = &(mCanMsgSchedule_t.at(mCanProfile_t.at(u32_mCurrentSlot).u32_EntryId));
      bool b_Rtr {};

      switch (pt_CanMsgScheduleEntry->CanProfileEntry_t.MsgTransferType_t) {
        case CAN_COMMAND:
        case CAN_ACQUISITION:
            b_Rtr = (pt_CanMsgScheduleEntry->CanProfileEntry_t.MsgTransferType_t == CAN_ACQUISITION);

            if (mCanDriverCtrl.write(pt_CanMsgScheduleEntry->u8_Data, pt_CanMsgScheduleEntry->CanProfileEntry_t.u8_Size,
                                        pt_CanMsgScheduleEntry->CanProfileEntry_t.u32_DeviceId, b_Rtr)) {
                mCanStatus_t = b_Rtr ? CAN_STATUS_AWAIT_RECEIVING : CAN_STATUS_OK;
            } else {
                mCanStatus_t = CAN_STATUS_WRITE_ERROR;
                errorHandler();
            }
            break;
        default:
            mCanStatus_t = CAN_STATUS_UNKNOWN_MSG_TYPE_ERROR; // no command or acquisition
            errorHandler();
            break;
      }
  }
}

void CanManagement::readMsg(void) {
  mCanStatus_t = CAN_STATUS_READ_PENDING;
  T_CanMsgScheduleEntry* pt_CanMsgScheduleEntry = &(mCanMsgSchedule_t.at(mCanProfile_t.at(u32_mCurrentSlot).u32_EntryId));

  /* Temporary variables to store received can frame information for validity check */
  uint8_t u8_RecBuf[CAN_STD_DATA_LENGTH] {};
  uint32_t u32_CanId {};
  bool b_isExtId {};
  bool b_IsRtr {};
  int8_t s8_Dlc = mCanDriverCtrl.read(u8_RecBuf, &u32_CanId, &b_isExtId, &b_IsRtr);

  if ((s8_Dlc == pt_CanMsgScheduleEntry->CanProfileEntry_t.u8_Size) && (u32_CanId == pt_CanMsgScheduleEntry->CanProfileEntry_t.u32_DeviceId)
      && (b_isExtId == false) && (b_IsRtr == false)) {
    std::memcpy(pt_CanMsgScheduleEntry->u8_Data, u8_RecBuf, CAN_STD_DATA_LENGTH);
    mCanStatus_t = CAN_STATUS_OK;
    mCanTopicHandler.responseToClient(pt_CanMsgScheduleEntry);
  }
  else {
    mCanStatus_t = CAN_STATUS_READ_ERROR;
    errorHandler();
  }
}

bool CanManagement::addToSchedule(const T_CanMsg &canMsg) {
  T_CanProfileEntry tempProfileEntry = {
    canMsg.u32_EntryId,
    canMsg.u32_DeviceId,
    canMsg.MsgTransferType_t,
    canMsg.u8_Size
  };  // create CanProfile entry from CAN message

  if(!isCanProfileEntryValid(tempProfileEntry)) {
    mCanStatus_t = CAN_STATUS_INVALID_MSG;
    return false;
  }

  /* create schedule entry from CAN message */
  T_CanMsgScheduleEntry tempScheduleEntry;
  tempScheduleEntry.CanProfileEntry_t = tempProfileEntry;
  std::memcpy(tempScheduleEntry.u8_Data, canMsg.u8_MsgData, canMsg.u8_Size);
  std::fill(tempScheduleEntry.u8_Data + canMsg.u8_Size, tempScheduleEntry.u8_Data + CAN_STD_DATA_LENGTH, 0);
  tempScheduleEntry.u32_Uuid = canMsg.u32_Uuid;

  /* add schedule entry to schedule or next schedule */
  if(!isEntryInSchedule(tempScheduleEntry, mCanMsgSchedule_t) && isCurrentSlotBeforeEntryIdSlot(canMsg.u32_EntryId)) {
    /* add entry to current schedule */
    mCanMsgSchedule_t.insert(std::pair<uint32_t, T_CanMsgScheduleEntry>(tempScheduleEntry.CanProfileEntry_t.u32_EntryId, tempScheduleEntry));
    return true;
  } else {
    /* add entry to next schedule */
    return addToNextSchedule(tempScheduleEntry);
  }
  return false;
}

bool CanManagement::addToNextSchedule(const T_CanMsgScheduleEntry &scheduleEntry) {
  if(!isEntryInSchedule(scheduleEntry, mCanMsgScheduleNext_t)) {
    mCanMsgScheduleNext_t.insert(std::pair<uint32_t, T_CanMsgScheduleEntry>(scheduleEntry.CanProfileEntry_t.u32_EntryId, scheduleEntry));
    return true;
  }
  return false;
}

void CanManagement::errorHandler(void) {
  switch(getStatus()) {
    case CAN_STATUS_UNDEFINED_ERROR:
      break;
    case CAN_STATUS_WRITE_ERROR:
      break;
    case CAN_STATUS_READ_ERROR:
      break;
    case CAN_STATUS_ADD_TO_SCHEDULE_ERROR:
      break;
    case CAN_STATUS_RESPONSE_ERROR:
      break;
    default:
      break;
  }
  while(true) {}
}


/* Public Member Function Implementation */
CanManagement::CanManagement(const T_CanProfile &canProfile_t) : mCanProfile_t(canProfile_t), mCanDriverCtrl(), mCanTopicHandler() {
  u32_mCanProfileSize = mCanProfile_t.size();
  resetSlotCounter();
  mCanCurrentPhase_t = CAN_PHASE_1;
  mCanStatus_t = CAN_STATUS_OK;
}

CanManagement::~CanManagement() {}

T_CanStatus CanManagement::getStatus(void) const {
  return this->mCanStatus_t;
}

void CanManagement::manager(void) {
  if(getStatus() == CAN_STATUS_AWAIT_RECEIVING) {//Acq answer is expected
    switch(mCanCurrentPhase_t) {
      case CAN_PHASE_1://first function call in current time slot and expecting Acq answer -> Error (timeout)
        errorHandler();
        break;
      case CAN_PHASE_2://second function call in current time slot and expecting Acq answer
        readMsg();//read received message from RxFifo
        incrementSlotCounter();
        mCanCurrentPhase_t = CAN_PHASE_1;
        break;
      default:
        errorHandler();
        break;
    }
  } else if(getStatus() == CAN_STATUS_OK) {//no answer expected -> ready to write
      switch(mCanCurrentPhase_t) {
        case CAN_PHASE_1://first function call in the current time slot
          sendMsg();//if message for current slot is in schedule -> send CAN message
          mCanCurrentPhase_t = CAN_PHASE_2;
          break;
        case CAN_PHASE_2://second function call in the current time slot, but no acq answer expected
          incrementSlotCounter();//increment the slot counter without sending/receiving any messages
          mCanCurrentPhase_t = CAN_PHASE_1;
          break;
        default:
          errorHandler();
          break;
      }
  } else {
    errorHandler();//no Acq answer expected and status not OK -> Error
  }
  
  if(u32_mCurrentSlot >= u32_mCanProfileSize) {
    resetSlotCounter();
    updateSchedules();
  }
}

void CanManagement::scheduleManager(void) {
  T_CanMsg canMsg;
  while(mCanTopicHandler.mCanMsgRxFifo.get(canMsg) && mCanStatus_t == CAN_STATUS_OK) {
    if(!addToSchedule(canMsg)) {
      mCanStatus_t = CAN_STATUS_ADD_TO_SCHEDULE_ERROR;
    }
  }
}
