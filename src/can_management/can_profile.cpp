/**************************************************************************************************
  * @file    can_profile.cpp
  * @author  Christian Ültzhöfer, Philipp Thümler
  * @version V0.0.0
  * @date    03-June-2023
  * @brief   This file contains the CanProfile table, including all possible message types that
  *          can be sent on the CAN Bus by the CAN_MANAGEMENT class.
  ************************************************************************************************/

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
#include "can_management/can_profile.h"

/**************************************************************************************************
 * CanProfile
 *************************************************************************************************/
//T_CanProfile CanProfile_t = {
//  {
//    1U,                     //Entry Id
//    1U,                     //Device Id
//    CAN_COMMAND,            //Message transfer type
//    (uint8_t)(6)            //size [bytes]
//  },
//  {2U,  1U,   CAN_ACQUISITION, (uint8_t)(8)},
//  {4U,  3U,   CAN_COMMAND,     (uint8_t)(2)},
//  {3U,  3U,   CAN_ACQUISITION, (uint8_t)(8)},
//  {5U,  3U,   CAN_ACQUISITION, (uint8_t)(6)},
//  {6U,  4U,   CAN_COMMAND,     (uint8_t)(3)}
//};
