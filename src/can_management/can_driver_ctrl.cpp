/**************************************************************************************************
 * @file    can_driver_ctrl.cpp
 * @author  Christian Ültzhöfer, Philipp Thümler
 * @version V0.0.0
 * @date    04-May-2023
 * @brief   This file contains all function implementations for the communication with the
 *          RODOS HAL_CAN driver library
 *************************************************************************************************/

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
#include "can_management/can_driver_ctrl.h"

/**************************************************************************************************
 * Public - Class implementation
 *************************************************************************************************/
/* Public Member Function Implementation */
CanDriverCtrl::CanDriverCtrl() : mCanHandle(CANIDX, CAN_RX_PIN, CAN_TX_PIN) {
    mCanHandle.init(CAN_STD_BAUDRATE);
    mCanHandle.addIncomingFilter(0, 0x7ff, false, false); // Accept all filter for standard frame
}

CanDriverCtrl::~CanDriverCtrl() {}

bool CanDriverCtrl::write(const uint8_t* pt_Data, uint8_t &u8_Dlc, uint32_t &u32_CanId, bool b_Rtr) {
    return (mCanHandle.write(pt_Data, u8_Dlc, u32_CanId, false, b_Rtr) == 0);
}

int8_t CanDriverCtrl::read(uint8_t* pt_RecBuf, uint32_t* u32_CanId, bool* b_IsExtId, bool* b_Rtr) {
    return (mCanHandle.read(pt_RecBuf, u32_CanId, b_IsExtId, b_Rtr));
}
