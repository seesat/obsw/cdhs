#include "include/can_management/can_management.h"

T_CanProfile CanProfile_t = {
    {
        0U,                 //Entry ID
        0U,                 //Device ID
        CAN_COMMAND,        //Message transfer type (Cmd/Acq)
        8U                  //Size [byte]
    },
    {1U, 0U, CAN_ACQUISITION, 0U},
    {2U, 1U, CAN_COMMAND, 8U},
    {29U, 1U, CAN_ACQUISITION, 0U},
    {13U, 5U, CAN_COMMAND, 8U},
    {574U, 5U, CAN_ACQUISITION, 0U},
    {3U, 2U, CAN_COMMAND, 8U},
    {7U, 2U, CAN_ACQUISITION, 0U}
};

CAN_MANAGEMENT can_management(CanProfile_t);
