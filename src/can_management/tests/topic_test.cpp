#include "rodos.h"
#include "hal/hal_can.h"

#ifndef  STM32F4_CAN

#define APPLICATION_ID 1000 // Default
#define PUBLISHER_THREAD_PRIORITY DEFAULT_THREAD_PRIORITY

#define CANIDX CAN_IDX::CAN_IDX0
#define RXPIN GPIO_PIN::GPIO_024
#define TXPIN GPIO_PIN::GPIO_025

#define BAUDRATE 125'000

#endif /* STM32F4_CAN */

static Application can_test("STM32F4 CAN Test", APPLICATION_ID);

HAL_GPIO ledGreen ( GPIO_060 );
HAL_GPIO ledOrange ( GPIO_061 );
HAL_GPIO ledRed ( GPIO_062 );
HAL_GPIO ledBlue ( GPIO_063 );
Topic<uint32_t> CounterTopic(10, "Counter Topic");

class PublisherTestThread : public StaticThread<> {
  public:

    PublisherTestThread() : StaticThread<>("PublisherTestThread", PUBLISHER_THREAD_PRIORITY) {}

    void init() override {
      ledGreen.init(true);
      ledOrange.init(true);
    }

    void run() override {
    uint32_t u32_Counter = 0;
      TIME_LOOP(SECONDS, SECONDS) {
        CounterTopic.publish(u32_Counter);
        ledGreen.setPins(u32_Counter%2==0?1:0);
        ledOrange.setPins(u32_Counter%2==0?0:1);
        u32_Counter++;
      }
    }
};

class SubscriberTest : public Subscriber {
  public:
      HAL_CAN canHandle;
      uint8_t u8_Data[8] = {1, 2, 3, 4, 5, 6, 7, 8};
      uint8_t u8_Size = 8;
      uint16_t u16_CanId = 0;
      bool b_IsExtId = false;
      bool b_IsRtr = false;
    uint32_t u32_SendCounter = 0;

    SubscriberTest() : Subscriber(CounterTopic, "subscriber_test"), canHandle(CANIDX, RXPIN, TXPIN) {
      ledRed.init(true);
      ledBlue.init(true);
    }

    uint32_t put([[maybe_unused]]const uint32_t topicId, const size_t len, void* data, [[maybe_unused]]const NetMsgInfo& netMsgInfo) {
      canHandle.init(BAUDRATE);
      if(0 == canHandle.write(reinterpret_cast<uint8_t*>(data), (uint8_t)(len), u16_CanId, b_IsExtId, b_IsRtr)) {
        ledRed.setPins(u32_SendCounter%2==0?1:0);
        ledBlue.setPins(u32_SendCounter%2==0?0:1);
      }
      u32_SendCounter++;
      return 1;
    }
};

PublisherTestThread publisher_test;
SubscriberTest subscriber_test;