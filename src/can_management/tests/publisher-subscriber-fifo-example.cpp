#include "rodos.h"
#include "hal/hal_can.h"
#include <vector>

#ifndef  STM32F4_CAN

#define APPLICATION_ID 1000 // Default
#define PUBLISHER_THREAD_PRIORITY DEFAULT_THREAD_PRIORITY

#define CANIDX CAN_IDX::CAN_IDX0
#define RXPIN GPIO_PIN::GPIO_024
#define TXPIN GPIO_PIN::GPIO_025

#define BAUDRATE 125'000

#endif /* STM32F4_CAN */

using namespace std;

static Application can_test("STM32F4 publisher-subscriber-fifo test", APPLICATION_ID);

HAL_GPIO ledGreen ( GPIO_060 );
HAL_GPIO ledOrange ( GPIO_061 );
HAL_GPIO ledRed ( GPIO_062 );
HAL_GPIO ledBlue ( GPIO_063 );

typedef struct {
  uint32_t u32_uuid;
  uint8_t  u8_Data[8];
  uint32_t u32_Size;
} T_Topic;

Topic<T_Topic> TestTopic(10, "Test Topic");

class TEST_PUBLISHER_THREAD : public StaticThread<> {
  public:

    TEST_PUBLISHER_THREAD() : StaticThread<>("PUBLISHER_THREAD", PUBLISHER_THREAD_PRIORITY) {}

    void init() override {
      ledGreen.init(true);
      ledOrange.init(true);
    }

    void run() override {
      T_Topic TestTopic_t;
      TestTopic_t.u32_uuid = 1234;
      TestTopic_t.u8_Data[0] = 0;
      TestTopic_t.u32_Size = 8;
      TIME_LOOP(800*MILLISECONDS, 800*MILLISECONDS) {
        TestTopic.publish(TestTopic_t);
        TestTopic_t.u8_Data[0]++;
        ledGreen.setPins(TestTopic_t.u8_Data[0]%2);
        ledOrange.setPins(!TestTopic_t.u8_Data[0]%2);
      }
    }
};

class CAN_SENDING_THREAD : public StaticThread<> {
  friend TEST_SUBSCRIBER;
  private:
    vector<T_Topic> Schedule;
  public:
    HAL_CAN canHandle;
    uint16_t u16_CanId = 0;
    bool b_IsExtId = false;
    bool b_IsRtr = false;
    uint8_t u8_Data[8];
    uint8_t u8_Size = sizeof(u8_Data) / sizeof(uint8_t);

    CAN_SENDING_THREAD() : StaticThread<>("Sending Thread (CAN)", DEFAULT_THREAD_PRIORITY), canHandle(CANIDX, RXPIN, TXPIN) {}

    void init() override {
      ledRed.init(true);
      canHandle.init(BAUDRATE);
    }

    void run() override {
      TIME_LOOP(SECONDS, SECONDS) {
        if(Schedule.size() > 0) {
          if(0 == canHandle.write(reinterpret_cast<uint8_t*>(&u8_Data), u8_Size, u16_CanId, false, false)) {
            ledRed.setPins(u8_Data[0]%2);
          }
        }
      }
    }
};

class TEST_SUBSCRIBER : public Subscriber {
  private:
    Fifo<T_Topic, 10> TestFifo;

  public:

    TEST_SUBSCRIBER() : Subscriber(TestTopic, "SUBSCRIBER_COUNTER_TOPIC") {
      ledBlue.init(true);
    }

    uint32_t put([[maybe_unused]]const uint32_t topicId, [[maybe_unused]]const size_t len, void* data, [[maybe_unused]]const NetMsgInfo& netMsgInfo) override {
      if(TestFifo.put(*(T_Topic*)(data))) ledBlue.setPins(!ledBlue.readPins());
      return 1;
    }
};

TEST_PUBLISHER_THREAD publisher_test;
TEST_SUBSCRIBER subscriber_test;
CAN_SENDING_THREAD sending_thread;
