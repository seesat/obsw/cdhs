#include "rodos.h"
#include "hal/hal_can.h"

#ifndef  STM32F4_CAN

#define APPLICATION_ID 1000 // Default
#define SENDING_THREAD_PRIORITY DEFAULT_THREAD_PRIORITY
#define RECEIVING_THREAD_PRIORITY DEFAULT_THREAD_PRIORITY

#define SENDING_CANIDX CAN_IDX::CAN_IDX0
#define SENDING_RXPIN GPIO_PIN::GPIO_024
#define SENDING_TXPIN GPIO_PIN::GPIO_025

#define RECEIVING_CANIDX CAN_IDX::CAN_IDX1
#define RECEIVING_RXPIN GPIO_PIN::GPIO_028
#define RECEIVING_TXPIN GPIO_PIN::GPIO_029

#define BAUDRATE 125'000

#endif /* STM32F4_CAN */

// Threads can be grouped under a common application
static Application can_test("STM32F4 CAN Test", APPLICATION_ID);

HAL_GPIO ledGreen ( GPIO_060 );
HAL_GPIO ledOrange ( GPIO_061 );
HAL_GPIO ledRed ( GPIO_062 );
HAL_GPIO ledBlue ( GPIO_063 );

class CanSendingTestThread : public RODOS::StaticThread<> {
public:
	RODOS::HAL_CAN test_can { SENDING_CANIDX, SENDING_RXPIN, SENDING_TXPIN };

	CanSendingTestThread() : StaticThread<>("CAN Sending Test Thread", SENDING_THREAD_PRIORITY) {}

	void init() override {
		ledBlue.init( true );
		ledGreen.init( true );
		test_can.init( BAUDRATE );
	}

	void run() override {
		uint8_t u8_Data[8] = {1, 2, 3, 4, 5, 6, 7, 8};
		uint8_t testValueSize { 8 };
		uint16_t canID { 0 };
		bool extID { false };
		bool rtr { false };

		uint32_t cnt { 1 };

		TIME_LOOP( RODOS::SECONDS, RODOS::SECONDS ) {
			if( !test_can.write(u8_Data, testValueSize, canID, extID, rtr) ) {
				ledBlue.setPins( cnt );
				if( cnt ) cnt = 0;
				else cnt++;
				ledGreen.setPins( cnt );
			}
		}
	}
};


class CanReceivingTestThread : public RODOS::StaticThread<> {
public:
	RODOS::HAL_CAN test_can { RECEIVING_CANIDX, RECEIVING_RXPIN, RECEIVING_TXPIN };

	CanReceivingTestThread() : StaticThread<>("CAN Receiving Test Thread", RECEIVING_THREAD_PRIORITY) {}

	void init() override {
		uint32_t id { 0 };
		uint32_t idMask { 0x7ff };
		bool extID { false };
		bool rtr { false };

		ledRed.init( true );
		ledOrange.init( true );
		test_can.init( BAUDRATE );
		
		// Adding filters here causes both Threads to not working correctly
		//test_can.addIncomingFilter( id, idMask, extID, rtr );
	}

	void run() override {
		// Adding filters here seems to be ok
		test_can.addIncomingFilter( 0, 0x7ff, false, false );
	
		/* read variables */
		int64_t readValue {};
		uint32_t id {};
		bool isExtID {};
		bool rtr {};

		uint32_t cnt { 1 };
		
		int8_t dlc {};

		// Read is never successfull
		while(true) {
			//test_can.suspendUntilDataReady();
			//if(test_can.isDataReady()) {
				dlc = test_can.read( reinterpret_cast<uint8_t*>(&readValue), &id, &isExtID, &rtr );
				if( dlc >= 0 ) {
					ledRed.setPins( true );
					//ledRed.setPins( cnt );
					if(cnt) cnt = 0;
					else cnt++;
				}
				else if( dlc == -1 ) {
					ledOrange.setPins( true );
				}
			//}
		}
	}
};

CanSendingTestThread canSendingTestThread;
CanReceivingTestThread canReceivingTestThread;
