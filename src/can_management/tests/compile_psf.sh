#!/usr/bin/env bash

# Christian Ültzhöfer - April 2023
# Philipp Thümler - May 2023: Altered file for publisher-subscriber-fifo-example.cpp

# This script can be used to compile the publisher-subscriber-fifo-example.cpp file for the STM32F4 board
# Optional argument -n to compile RODOS library for STM32F4 new

pushd libs/rodos > /dev/null # Change to Rodos Repository

printf "  --------- Set Environment Variables --------------------\n"
source setenvs.sh > /dev/null # Set Environment Variables

popd > /dev/null

if [ "$1" == "-n" ] || [ ! -f "$RODOS_LIBS/discovery/librodos.a" ]; then
	printf "  --------- Compile RODOS library for STM32F4 ------------\n"
	rodos-lib.sh discovery > /dev/null # Compile RODOS library for STM32F4
else # Use existing compilation of RODOS STM32F4 library
	printf "  --------- Using existing RODOS STM32F4 library ---------\n"
fi

export CPPFLAGS="${CPPFLAGS} -Wl,--no-warn-rwx-segments"

printf "  --------- Compile user program -------------------------\n"
rodos-executable.sh discovery publisher-subscriber-fifo-example.cpp # Compile user program

printf "  --------- Compilation terminated -----------------------\n\n"
echo -e "Compiled Files: topic_test.cpp"

arm-none-eabi-objcopy -O binary tst boot.bin # Convert elf file to bin file

rm tst # Remove elf file
