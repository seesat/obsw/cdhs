/**************************************************************************************************
  * @file    can_application.cpp
  * @author  Christian Ültzhöfer, Philipp Thümler
  * @version V0.0.0
  * @date    03-June-2023
  * @brief   This file contains the application for communication via CAN-bus.
  ************************************************************************************************/

/**************************************************************************************************
 * Include Header Files
 *************************************************************************************************/
#include "can_management/can_management.h"
#include "can_management/can_profile.h"

/**************************************************************************************************
 * Public - Global variables
 *************************************************************************************************/

/**************************************************************************************************
 * Public - Thread implementation
 *************************************************************************************************/
//class CanManagementThreadExample;
//class CanTransmitThreadExample;
//class ParentThreadExample;
//CanManagement canManager(CanProfile_t);

static RODOS::Application can_management_application("STM32F4 can_management Application", CAN_STD_APPLICATION_ID); //führt zu linker errors

//class CanManagementThreadExample : public RODOS::StaticThread<> {
//  public:
//    ParentThreadExample* parentThread;

//    void init() override {}

//    void run() override {
//      for(;;) {
//        canManager.scheduleManager();
//        suspendCallerUntil(END_OF_TIME, parentThread);
//      }
//    }
//  //Get CanMsg from CanMsgRxFifo and sort them into the schedule
//};

///**
// * @brief periodically send messages on the CAN-Bus
// * 
// */
//class CanTransmitThreadExample : public RODOS::StaticThread<> {
//  public:
//    ParentThreadExample* parentThread;

//    void init() override {}

//    void run() override {
//      canManager.manager();
//      suspendCallerUntil(END_OF_TIME, parentThread);
//    }
//};

class ParentThreadExample : public RODOS::StaticThread<> {
  public:
    //CanManagementThreadExample* canManagementThread;
    //CanTransmitThreadExample* canTransmitThread;

    void init() override {

      T_CanProfile CanProfile_t = {   // definition for CanProfile table -> defining it outside of a function/thread leads to linker errors
        {
          1U,                     //Entry Id
          1U,                     //Device Id
          CAN_COMMAND,            //Message transfer type
          (uint8_t)(6)            //size [bytes]
        },
        {2U,  1U,   CAN_ACQUISITION, (uint8_t)(8)},
        {4U,  3U,   CAN_COMMAND,     (uint8_t)(2)},
        {3U,  3U,   CAN_ACQUISITION, (uint8_t)(8)},
        {5U,  3U,   CAN_ACQUISITION, (uint8_t)(6)},
        {6U,  4U,   CAN_COMMAND,     (uint8_t)(3)}
      };

      CanManagement canManager(CanProfile_t);
    }

    void run() override {
      for(;;) {
        //canTransmitThread->resume();
        //suspendCallerUntil(RODOS::NOW() + 75*RODOS::MILLISECONDS);
        //canManagementThread->resume();
        //suspendCallerUntil(RODOS::NOW() + 25*RODOS::MILLISECONDS);
        
      }
    }
};
